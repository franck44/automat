/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat

package auto
package tests

import org.scalatest._
import edge.LabDiEdge
import edge.Implicits._
import util.Partition
import util.Minimiser._

/**
 * Sanity checks for Partition.
 */
class PartitionTests extends FunSuite with Matchers {

    test( "Create an empty partition" ){

        val p = Partition[ Int, Int ]( Map() )
        val s = p.equivClassOf( 0 )
        s shouldBe None
    }

    test( "Create an non-empty partition and look some states" ){

        val p = Partition[ Int, Int ]( Map( 0 -> 1, 1 -> 2 ) )
        p.equivClassOf( 0 ) shouldBe Some( 1 )
        p.equivClassOf( 1 ) shouldBe Some( 2 )
        p.equivClassOf( 9 ) shouldBe None

    }

    test( "Create an empty partition compute allClasses" ){

        val p = Partition[ Int, Int ]( Map() )
        p.allClasses shouldBe Set()

    }

    test( "Create an non-empty partition1 compute allClasses" ){

        val p = Partition[ Int, Int ]( Map( 0 -> 1, 1 -> 2 ) )
        p.allClasses shouldBe Set( Set( 0 ), Set( 1 ) )

    }

    test( "Create an non-empty partition2 compute allClasses" ){

        val p = Partition[ Int, Int ]( Map( 0 -> 1, 1 -> 2, 2 -> 2 ) )
        p.allClasses shouldBe Set( Set( 0 ), Set( 1, 2 ) )

    }

    test( "Create an non-empty partition3 compute allClasses" ){

        val p = Partition[ Int, String ]( Map( 0 -> "a", 1 -> "b", 2 -> "a" ) )
        p.allClasses shouldBe Set( Set( 1 ), Set( 0, 2 ) )

    }

    test( "Create an empty partition compute size" ){

        val p = Partition[ Int, Int ]( Map() )
        p.size shouldBe 0

    }

    test( "Create an non-empty partition1 compute size" ){

        val p = Partition[ Int, Int ]( Map( 0 -> 1, 1 -> 2 ) )
        p.size shouldBe 2

    }

    test( "Create an non-empty partition2 compute size" ){

        val p = Partition[ Int, Int ]( Map( 0 -> 1, 1 -> 2, 2 -> 2 ) )
        p.size shouldBe 2

    }

    test( "Create an non-empty partition3 compute size" ){

        val p = Partition[ Int, String ]( Map( 0 -> "a", 1 -> "b", 2 -> "a" ) )
        p.size shouldBe 2

    }

    test( "Compare two equivalent empty partitions 1" ){

        val p1 = Partition[ Int, Int ]( Map() )
        val p2 = Partition[ Int, String ]( Map() )
        p1.equiv( p2 ) shouldBe true
        p2.equiv( p1 ) shouldBe true

    }

    test( "Compare two equivalent singleton partitions 1" ){

        val p1 = Partition[ Int, Int ]( Map( 0 -> 1 ) )
        val p2 = Partition[ Int, String ]( Map( 0 -> "a" ) )
        p1.equiv( p2 ) shouldBe true
        p2.equiv( p1 ) shouldBe true

    }

    test( "Compare two non equivalent  partitions " ){

        val p1 = Partition[ Int, Int ]( Map( 0 -> 1, 1 -> 1 ) )
        val p2 = Partition[ Int, String ]( Map( 0 -> "a", 1 -> "b" ) )
        p1.equiv( p2 ) shouldBe false
        p2.equiv( p1 ) shouldBe false

    }

    test( "Compare two non equivalent partitions of different sizes" ){

        val p1 = Partition[ Int, Int ]( Map( 0 -> 1, 1 -> 1 ) )
        val p2 = Partition[ Int, String ]( Map( 0 -> "a", 1 -> "b", 3 -> "d" ) )
        p1.equiv( p2 ) shouldBe false
        p2.equiv( p1 ) shouldBe false

    }

}
