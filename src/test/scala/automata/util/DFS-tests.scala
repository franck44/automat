/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat

package util

import org.scalatest._
import edge.LabDiEdge
import edge.Implicits._
import auto.NFA
import Traversal.DFS

class DFSTests extends FunSuite with Matchers {

    test( "DFS on an single state automaton" ) {
        //  first one has no transition
        val nfa1 = NFA[ Int, String ]( Set( 0 ), Set(), Set() )

        //  create a visitor of type  nfa1
        val v = DefaultDFSVisitor( nfa1 )
        //  create a DFS with the visitor
        val dfsReach = DFS( v, nfa1.outGoingEdges )
        val r1 = dfsReach( nfa1.getInit )
        assert( r1.discoveryTime == Map( Set( 0 ) → 0 ) )
        assert( r1.parent == Map() )

    }

    test( "DFS on a DFA with loop" ) {
        //  second one
        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ), ( 0 ~> 2 )( "a" ), ( 1 ~> 0 )( "b" ), ( 1 ~> 2 )( "a" ) ),
            Set( 2 ) )

        //  notice that DFS returns same type of visitor
        val dfsReach2 = DFS( DefaultDFSVisitor( nfa2 ), nfa2.outGoingEdges )
        val r = dfsReach2( nfa2.getInit )

        assert( r.discoveryTime ==
            Map( Set( 0 ) → 0, Set( 1, 2 ) → 1, Set( 2 ) → 2 ) )
        assert( r.parent ===
            Map( Set( 1, 2 ) → ( ( Set( 0 ), "a" ) ), Set( 2 ) → ( ( Set( 1, 2 ), "a" ) ) ) )
        // 3 reachable states in deterministic version
        assert( r.discoveryTime.size == 3 )
    }

    test( "DFS on a product" ) {
        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ), ( 0 ~> 2 )( "a" ), ( 1 ~> 0 )( "b" ), ( 1 ~> 2 )( "a" ) ),
            Set( 2 ) )

        val nfa1 = NFA[ Int, String ]( Set( 0 ), Set(), Set( 0 ) )

        val p1 = nfa1 + nfa2
        val dfsReachp1 = DFS( DefaultDFSVisitor( p1 ), p1.outGoingEdges )
        val r = dfsReachp1( p1.getInit )

        // 4 reachable states in deterministic version
        assert( r.discoveryTime.size == 4 )

    }

}
