/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat

package util

import org.scalatest._
import auto.NFA
import edge.LabDiEdge
import edge.Implicits._
import Traversal.DFS
import util.Determiniser.toDetNFA
import scala.util.{ Try, Success, Failure }
import org.bitbucket.franck44.dot.util.DotASTAttr._
import org.bitbucket.franck44.dot.util.FileParser.parseFile
import org.bitbucket.franck44.dot.DOTSyntax.DotSpec
import org.bitbucket.franck44.dot.DOTPrettyPrinter.format
import util.PathUtil.{ joinPath, dotTestPath }

class AutomataIntersectionTests extends FunSuite with Matchers {
    val path = dotTestPath

    test( "Intersetion/Union of two NFAs with no common first letter" ) {
        //  build an NFA. As we read strings we have to convert
        val nfa1 = {
            val tryNfa : Try[ DotSpec ] = parseFile( joinPath( path, "auto4.dot" ) )
            assert( tryNfa.isSuccess )
            NFA(
                getInitStates( tryNfa.get ),
                getEdges( tryNfa.get ), getAcceptStates( tryNfa.get ) )
        }

        //  build an NFA. As we read strings we have to convert
        val nfa2 = {
            val tryNfa : Try[ DotSpec ] = parseFile( joinPath( path, "auto5.dot" ) )
            assert( tryNfa.isSuccess )
            NFA(
                getInitStates( tryNfa.get ),
                getEdges( tryNfa.get ), getAcceptStates( tryNfa.get ) )
        }

        val d = nfa1 * nfa2

        assert( toDetNFA( d )._1 == NFA[ Int, String ]( Set( 0 ), Set(), Set() ) )

        val e = nfa1 + nfa2
        val dete : NFA[ Int, String ] = toDetNFA( e )._1

        val resForE : NFA[ Int, String ] = {
            val tryNfa : Try[ DotSpec ] = parseFile( joinPath( path, "res1.dot" ) )
            assert( tryNfa.isSuccess )
            //  map the String obtained by parsing to Int
            NFA(
                getInitStates( tryNfa.get ).map( _.toInt ),
                getEdges( tryNfa.get ) map {
                    case LabDiEdge( e, l ) ⇒ ( e.src.toInt ~> e.tgt.toInt )( l )
                }, getAcceptStates( tryNfa.get ).map( _.toInt ) )
        }

        assert( toDetNFA( e )._1 == resForE )
    }

    test( "Intersetion/Union of two NFAs" ) {
        //  build an NFA. As we read strings we have to convert
        val nfa1 = {
            val tryNfa : Try[ DotSpec ] = parseFile( joinPath( path, "auto4.dot" ) )
            assert( tryNfa.isSuccess )
            NFA(
                getInitStates( tryNfa.get ),
                getEdges( tryNfa.get ), getAcceptStates( tryNfa.get ) )
        }

        //  build an NFA. As we read strings we have to convert
        val nfa2 = {
            val tryNfa : Try[ DotSpec ] = parseFile( joinPath( path, "auto5.dot" ) )
            assert( tryNfa.isSuccess )
            NFA(
                getInitStates( tryNfa.get ),
                getEdges( tryNfa.get ), getAcceptStates( tryNfa.get ) )
        }

        val d = nfa1 * nfa2

        assert( toDetNFA( d )._1 == NFA[ Int, String ]( Set( 0 ), Set(), Set() ) )

        val e = nfa1 + nfa2

        val resForE : NFA[ Int, String ] = {
            val tryNfa : Try[ DotSpec ] = parseFile( joinPath( path, "res2.dot" ) )
            assert( tryNfa.isSuccess )
            //  map the String obtained by parsing to Int
            NFA(
                getInitStates( tryNfa.get ).map( _.toInt ),
                getEdges( tryNfa.get ) map {
                    case LabDiEdge( e, l ) ⇒ ( e.src.toInt ~> e.tgt.toInt )( l )
                }, getAcceptStates( tryNfa.get ).map( _.toInt ) )
        }

        assert( toDetNFA( e )._1 == resForE )
    }

    test( "Two NFAs and compute difference" ) {
        //  build an NFA. As we read strings we have to convert
        val nfa1 = {
            val tryNfa : Try[ DotSpec ] = parseFile( joinPath( path, "auto6.dot" ) )
            assert( tryNfa.isSuccess )
            NFA(
                getInitStates( tryNfa.get ),
                getEdges( tryNfa.get ), getAcceptStates( tryNfa.get ) )
        }

        //  build an NFA. As we read strings we have to convert
        val nfa2 = {
            val tryNfa : Try[ DotSpec ] = parseFile( joinPath( path, "auto7.dot" ) )
            assert( tryNfa.isSuccess )
            NFA(
                getInitStates( tryNfa.get ),
                getEdges( tryNfa.get ), getAcceptStates( tryNfa.get ) )
        }

        val d = nfa1 - nfa2
        val resFord : NFA[ Int, String ] = {
            val tryNfa : Try[ DotSpec ] = parseFile( joinPath( path, "res3.dot" ) )
            assert( tryNfa.isSuccess )
            //  map the String obtained by parsing to Int
            NFA(
                getInitStates( tryNfa.get ).map( _.toInt ),
                getEdges( tryNfa.get ) map {
                    case LabDiEdge( e, l ) ⇒ ( e.src.toInt ~> e.tgt.toInt )( l )
                }, getAcceptStates( tryNfa.get ).map( _.toInt ) )
        }

        assert( toDetNFA( d )._1 == resFord )
    }

}
