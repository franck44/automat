/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat
package util
package tests

import org.scalatest._
import edge.LabDiEdge
import edge.Implicits._
import auto.NFA
import Traversal.DFS
import graph.Dominator.{ tarjanImDom, domFrontier }
import graph.RootedDiGraph
import org.bitbucket.franck44.dot.DOTPrettyPrinter.show
import DotConverter._
import org.bitbucket.franck44.dot.DOTSyntax.DotSpec
import org.bitbucket.franck44.dot.util.DotASTAttr._
import org.bitbucket.franck44.dot.DOTSyntax.{ Attribute, StringLit, Ident }

class DominatorTests extends FunSuite with Matchers {

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    //  logger
    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    def prettyPrint[ S ]( root : S, s : Map[ S, S ] ) : DotSpec = {
        val a = new NFA[ S, Option[ Nothing ] ](
            Set( root ),
            ( s map {
                case ( src, tgt ) ⇒ ( src ~> tgt )( None : Option[ Nothing ] )
            } ).toSet,
            Set() )

        val aDot : DotSpec =
            toDot(
                a,
                //  map for node labels
                //  this is the label that will be displayed in Dot
                nodeProp = {
                    x : S ⇒
                        List(
                            // Attribute("shape", Ident("doublecircle")),
                            Attribute( "label", StringLit( x.toString ) ) )
                },
                //  map for node identifiers
                //  this is the node ID e.g. edges will be output
                //  as nodeIDsrc -> nodeIDtgt [some edge attributes]
                nodeDotName = {
                    x : S ⇒ "\"" + x.toString + "\""
                },
                labelDotName = {
                    x : Option[ Nothing ] ⇒ ""
                },
                graphProp = {
                    () ⇒ List( Attribute( "rankdir", Ident( "TB" ) ) )
                },
                graphDirective = {
                    () ⇒ List( "rank = sink ; \"" + root.toString + "\"" )
                } )
        aDot
    }

    test( "NFA with no loop, compute post dominator tree" ) {

        val nfa1 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ), ( 0 ~> 2 )( "a" ), ( 1 ~> 2 )( "b" ), ( 2 ~> 3 )( "c" ) ),
            Set( 2 ) )

        val nfa1Dot : DotSpec = toDot( nfa1 )
        //  dump automaton in a file
        logger.info( s"nfaDom1.dot\n: ${show( nfa1Dot )})" )

        //  for post dominators we use the dominator algorithm from a
        //  in the reverse graph
        val postDomTree = tarjanImDom( RootedDiGraph( 3, nfa1.reversed ) )
        val domDot : DotSpec = prettyPrint( 3, postDomTree )
        logger.info( s"dominator1.dot\n: ${show( domDot )}" )
        val postDomFront = domFrontier( RootedDiGraph( 3, nfa1 ).reversed )
        assert( postDomFront === Map( 2 → Set(), 1 → Set( 0 ), 0 → Set(), 3 → Set() ) )
    }

    test( "NFA with no loops, if-else-like, compute post dominator tree" ) {
        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ), ( 0 ~> 2 )( "b" ), ( 2 ~> 3 )( "b" ), ( 1 ~> 3 )( "a" ) ),
            Set( 3 ) )

        val nfa2Dot : DotSpec = toDot( nfa2 )
        //  dump automaton in a file
        logger.info( s"nfaDom2.dot\n: ${show( nfa2Dot )})" )

        //  for post dominators we use the dominator algorithm from a
        //  in the reverse graph
        val postDomTree = tarjanImDom( RootedDiGraph( 3, nfa2 ).reversed )
        val domDot : DotSpec = prettyPrint( 3, postDomTree )
        logger.info( s"dominator2.dot\n: ${show( domDot )}" )
        val postDomFront = domFrontier( RootedDiGraph( 3, nfa2 ).reversed )
        assert( postDomFront === Map( 2 → Set( 0 ), 1 → Set( 0 ), 3 → Set(), 0 → Set() ) )
    }

    test( "Example from Tarjan's paper" ) {
        val nfa3 = NFA[ String, String ](
            Set(),
            Set(
                ( "I" ~> "K" )( "" ),
                ( "K" ~> "I" )( "" ),
                ( "J" ~> "G" )( "" ),
                ( "I" ~> "F" )( "" ),
                ( "I" ~> "G" )( "" ),
                ( "I" ~> "J" )( "" ),
                ( "F" ~> "C" )( "" ),
                ( "G" ~> "C" )( "" ),
                ( "R" ~> "K" )( "" ),
                ( "K" ~> "H" )( "" ),
                ( "E" ~> "H" )( "" ),
                ( "H" ~> "E" )( "" ),
                ( "H" ~> "L" )( "" ),
                ( "L" ~> "D" )( "" ),
                ( "A" ~> "B" )( "" ),
                ( "A" ~> "R" )( "" ),
                ( "D" ~> "A" )( "" ),
                ( "D" ~> "B" )( "" ),
                ( "E" ~> "B" )( "" ),
                ( "C" ~> "R" )( "" ),
                ( "B" ~> "R" )( "" ) ),
            Set( "R" ) )

        val nfa3Dot : DotSpec = toDot( nfa3 )
        //  dump automaton in a file
        logger.info( s"nfaDom3.dot\n: ${show( nfa3Dot )})" )

        //  for post dominators we use the dominator algorithm from a
        //  in the reverse graph
        val postDomTree = tarjanImDom( RootedDiGraph( "R", nfa3 ).reversed )
        val domDot : DotSpec = prettyPrint( "R", postDomTree )
        logger.info( s"dominator3.dot\n: ${show( domDot )}" )
        val postDomFront = domFrontier( RootedDiGraph( "R", nfa3 ).reversed )
        assert(
            postDomFront ===
                Map( "E" → Set( "H" ), "J" → Set( "I" ),
                    "F" → Set( "I" ), "A" → Set( "D" ),
                    "I" → Set( "K" ), "G" → Set( "I" ),
                    "L" → Set( "H" ), "B" → Set( "E", "A", "D" ),
                    "C" → Set( "I" ), "H" → Set( "E", "K" ),
                    "K" → Set( "I" ), "R" → Set(), "D" → Set( "H" ) ) )
    }

    test( "Linear NFA with no loop, compute post dominator tree" ) {
        val nfa4 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ), ( 1 ~> 2 )( "a" ), ( 2 ~> 3 )( "b" ), ( 3 ~> 4 )( "a" ) ),
            Set( 4 ) )

        val nfa4Dot : DotSpec = toDot( nfa4 )
        //  dump automaton in a file
        logger.info( s"nfaDom4.dot\n: ${show( nfa4Dot )})" )

        //  for post dominators we use the dominator algorithm from a
        //  in the reverse graph
        val postDomTree = tarjanImDom( RootedDiGraph( 4, nfa4 ).reversed )
        val domDot : DotSpec = prettyPrint( 4, postDomTree )
        logger.info( s"dominator4.dot\n: ${show( domDot )}" )
        val postDomFront = domFrontier( RootedDiGraph( 4, nfa4 ).reversed )
        assert( postDomFront === Map( 0 → Set(), 1 → Set(), 2 → Set(), 3 → Set(), 4 → Set() ) )

    }
}

class DominanceFrontierTests extends FunSuite with Matchers {

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    //  logger
    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    def prettyPrint[ S ]( root : S, s : Map[ S, S ] ) : DotSpec = {
        val a = new NFA[ S, Option[ Nothing ] ](
            Set( root ),
            ( s map {
                case ( src, tgt ) ⇒ ( src ~> tgt )( None : Option[ Nothing ] )
            } ).toSet,
            Set() )

        val aDot : DotSpec =
            toDot(
                a,
                //  map for node labels
                //  this is the label that will be displayed in Dot
                nodeProp = {
                    x : S ⇒
                        List(
                            // Attribute("shape", Ident("doublecircle")),
                            Attribute( "label", StringLit( x.toString ) ) )
                },
                //  map for node identifiers
                //  this is the node ID e.g. edges will be output
                //  as nodeIDsrc -> nodeIDtgt [some edge attributes]
                nodeDotName = {
                    x : S ⇒ "\"" + x.toString + "\""
                },
                labelDotName = {
                    x : Option[ Nothing ] ⇒ ""
                },
                graphProp = {
                    () ⇒ List( Attribute( "rankdir", Ident( "TB" ) ) )
                },
                graphDirective = {
                    () ⇒ List( "rank = sink ; \"" + root.toString + "\"" )
                } )
        aDot
    }

    test( "NFA with no loop, compute post dominance frontier" ) {

        val nfa1 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ), ( 0 ~> 2 )( "a" ), ( 1 ~> 2 )( "b" ), ( 2 ~> 3 )( "c" ) ),
            Set( 2 ) )

        val nfa1Dot : DotSpec = toDot( nfa1 )
        //  dump automaton in a file
        logger.info( s"nfaDom1.dot\n: ${show( nfa1Dot )})" )

        //  for post dominators we use the dominator algorithm from a
        //  in the reverse graph
        val postDomTree = tarjanImDom( RootedDiGraph( 3, nfa1.reversed ) )
        val domDot : DotSpec = prettyPrint( 3, postDomTree )
        logger.info( s"dominanceFront1.dot\n: ${show( domDot )}" )
        val postDomFront = domFrontier( RootedDiGraph( 3, nfa1 ).reversed )
        assert( postDomFront === Map( 2 → Set(), 1 → Set( 0 ), 0 → Set(), 3 → Set() ) )
    }

    test( "NFA with no loops, if-else-like, compute post dominance frontier" ) {
        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ), ( 0 ~> 2 )( "b" ), ( 2 ~> 3 )( "b" ), ( 1 ~> 3 )( "a" ) ),
            Set( 3 ) )

        val nfa2Dot : DotSpec = toDot( nfa2 )
        //  dump automaton in a file
        logger.info( s"nfaDom2.dot\n: ${show( nfa2Dot )})" )

        //  for post dominators we use the dominator algorithm from a
        //  in the reverse graph
        val postDomTree = tarjanImDom( RootedDiGraph( 3, nfa2 ).reversed )
        val domDot : DotSpec = prettyPrint( 3, postDomTree )
        logger.info( s"dominator2.dot\n: ${show( domDot )}" )
        val postDomFront = domFrontier( RootedDiGraph( 3, nfa2 ).reversed )
        assert( postDomFront === Map( 2 → Set( 0 ), 1 → Set( 0 ), 3 → Set(), 0 → Set() ) )
    }

    test( "Example from Tarjan's paper - compute post dominance frontier" ) {
        val nfa3 = NFA[ String, String ](
            Set(),
            Set(
                ( "I" ~> "K" )( "" ),
                ( "K" ~> "I" )( "" ),
                ( "J" ~> "G" )( "" ),
                ( "I" ~> "F" )( "" ),
                ( "I" ~> "G" )( "" ),
                ( "I" ~> "J" )( "" ),
                ( "F" ~> "C" )( "" ),
                ( "G" ~> "C" )( "" ),
                ( "R" ~> "K" )( "" ),
                ( "K" ~> "H" )( "" ),
                ( "E" ~> "H" )( "" ),
                ( "H" ~> "E" )( "" ),
                ( "H" ~> "L" )( "" ),
                ( "L" ~> "D" )( "" ),
                ( "A" ~> "B" )( "" ),
                ( "A" ~> "R" )( "" ),
                ( "D" ~> "A" )( "" ),
                ( "D" ~> "B" )( "" ),
                ( "E" ~> "B" )( "" ),
                ( "C" ~> "R" )( "" ),
                ( "B" ~> "R" )( "" ) ),
            Set( "R" ) )

        val nfa3Dot : DotSpec = toDot( nfa3 )
        //  dump automaton in a file
        logger.info( s"nfaDom3.dot\n: ${show( nfa3Dot )})" )

        //  for post dominators we use the dominator algorithm from a
        //  in the reverse graph
        val postDomTree = tarjanImDom( RootedDiGraph( "R", nfa3 ).reversed )
        val domDot : DotSpec = prettyPrint( "R", postDomTree )
        logger.info( s"dominator3.dot\n: ${show( domDot )}" )
        val postDomFront = domFrontier( RootedDiGraph( "R", nfa3 ).reversed )
        assert(
            postDomFront ===
                Map( "E" → Set( "H" ), "J" → Set( "I" ),
                    "F" → Set( "I" ), "A" → Set( "D" ),
                    "I" → Set( "K" ), "G" → Set( "I" ),
                    "L" → Set( "H" ), "B" → Set( "E", "A", "D" ),
                    "C" → Set( "I" ), "H" → Set( "E", "K" ),
                    "K" → Set( "I" ), "R" → Set(), "D" → Set( "H" ) ) )
    }

    test( "Linear NFA with no loop, compute post dominance frontier" ) {
        val nfa4 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ), ( 1 ~> 2 )( "a" ), ( 2 ~> 3 )( "b" ), ( 3 ~> 4 )( "a" ) ),
            Set( 4 ) )

        val nfa4Dot : DotSpec = toDot( nfa4 )
        //  dump automaton in a file
        logger.info( s"nfaDom4.dot\n: ${show( nfa4Dot )})" )

        //  for post dominators we use the dominator algorithm from a
        //  in the reverse graph
        val postDomTree = tarjanImDom( RootedDiGraph( 4, nfa4 ).reversed )
        val domDot : DotSpec = prettyPrint( 4, postDomTree )
        logger.info( s"dominator4.dot\n: ${show( domDot )}" )
        val postDomFront = domFrontier( RootedDiGraph( 4, nfa4 ).reversed )
        assert( postDomFront === Map( 0 → Set(), 1 → Set(), 2 → Set(), 3 → Set(), 4 → Set() ) )

    }

    test( "Linear NFA with no loop, dead location (unreachable from sink), compute post dominance frontier" ) {
        val nfa5 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ),
                ( 1 ~> 2 )( "a" ),
                ( 2 ~> 3 )( "a" ),
                ( 3 ~> 4 )( "a" ),
                ( 2 ~> 5 )( "a" ) ),
            Set( 4 ) )

        val nfa5Dot : DotSpec = toDot( nfa5 )
        //  dump automaton in a file
        logger.info( s"nfaDom5.dot\n: ${show( nfa5Dot )})" )

        //  for post dominators we use the dominator algorithm from a
        //  in the reverse graph
        val postDomTree = tarjanImDom( RootedDiGraph( 4, nfa5 ).reversed )
        val domDot : DotSpec = prettyPrint( 4, postDomTree )
        logger.info( s"dominator5.dot\n: ${show( domDot )}" )
        val postDomFront = domFrontier( RootedDiGraph( 4, nfa5 ).reversed )
        assert( postDomFront === Map(
            0 → Set(),
            1 → Set(),
            2 → Set(),
            3 → Set(),
            4 → Set() ) )

    }

    test( "Linear NFA with no loop, no dead location, compute post dominance frontier" ) {
        val nfa6 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ),
                ( 1 ~> 2 )( "a" ),
                ( 2 ~> 3 )( "a" ),
                ( 3 ~> 4 )( "a" ),
                ( 2 ~> 5 )( "a" ),
                ( 5 ~> 4 )( "a" ) ),
            Set( 4 ) )

        val nfa6Dot : DotSpec = toDot( nfa6 )
        //  dump automaton in a file
        logger.info( s"nfaDom6.dot\n: ${show( nfa6Dot )}" )

        //  for post dominators we use the dominator algorithm from a
        //  in the reverse graph
        val postDomTree = tarjanImDom( RootedDiGraph( 4, nfa6 ).reversed )
        val domDot : DotSpec = prettyPrint( 4, postDomTree )
        logger.info( s"dominator6.dot\n: ${show( domDot )}" )
        val postDomFront = domFrontier( RootedDiGraph( 4, nfa6 ).reversed )
        assert( postDomFront === Map(
            0 → Set(),
            5 → Set( 2 ),
            1 → Set(),
            2 → Set(),
            3 → Set( 2 ),
            4 → Set() ) )

    }
}
