/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat
package util
package tests

import org.bitbucket.franck44.dot.DOTSyntax.DotSpec
import edge.LabDiEdge
import edge.Implicits._
import auto.NFA
import org.scalatest._
import org.bitbucket.franck44.dot.DOTPrettyPrinter.show
import DotConverter._
import org.bitbucket.franck44.dot.util.FileParser.parseString
import scala.util.{ Try, Success, Failure }
import org.bitbucket.franck44.dot.util.DotASTAttr._
import org.bitbucket.franck44.dot.DOTSyntax.{ Attribute, StringLit, Ident }
import Determiniser.toDetNFA

/**
 *  test for conversion from NFA toDot format
 */
class NFAToDotTests extends FunSuite with Matchers {

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    //  logger
    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    test( "Convert NFA to DOT, read the file and compare to initial" ) {
        //   as we generate a dot file and the type of node
        //   read from DOT is String, we create an automaton with
        //   nodes of type strings
        val nfa1 = NFA(
            init = Set( "0" ),
            Set(
                ( "0" ~> "1" )( "a" ), ( "0" ~> "2" )( "a" ), ( "1" ~> "0" )( "b" ), ( "1" ~> "2" )( "a" ) ),
            Set( "2" ) )

        val nfa1Dot : DotSpec = toDot( nfa1 )
        //  print automaton in a file
        logger.info( s"nfa1.dot\n ${show( nfa1Dot )}" )

        val nfa1DotString = show( nfa1Dot )

        // now parse file and check it is the same as the automaton we started
        // with
        val tryNfa : Try[ DotSpec ] = parseString( nfa1DotString )

        assert( tryNfa.isSuccess )
        //  build an NFA. As we read strings we have to convert
        val readInnfa1 = NFA(
            getInitStates( tryNfa.get ),
            getEdges( tryNfa.get ), getAcceptStates( tryNfa.get ) )
        assert( nfa1 == readInnfa1 )
    }

    //  @tony: example with node attributes
    test( "Output an NFA with node attributes" ) {
        val nfa2 = NFA(
            init = Set( "0" ),
            Set(
                ( "0" ~> "1" )( "a" ), ( "0" ~> "2" )( "a" ), ( "1" ~> "0" )( "b" ), ( "1" ~> "2" )( "a" ) ),
            Set( "2" ), name = "NFA2" )

        val nfa2Dot : DotSpec =
            toDot(
                nfa2,
                //  map for node labels
                //  this is the label that will be displayed in Dot
                nodeProp = {
                    x : String ⇒
                        if ( !nfa2.accepting.contains( x ) )
                            List( Attribute( "label", StringLit( "node" + x ) ) )
                        else
                            List(
                                Attribute( "shape", Ident( "doublecircle" ) ),
                                Attribute( "label", StringLit( "node" + x ) ) )
                },
                //  map for node identifiers
                //  this is the node ID e.g. edges will be output
                //  as nodeIDsrc -> nodeIDtgt [some edge attributes]
                labelDotName = {
                    x : String ⇒ "Node_" + x
                },
                graphDirective = {
                    () ⇒ List( "rank = sink ; 0 ", "rank = source ; 2" )
                } )

        logger.info( s"nfa2.dot\n ${show( nfa2Dot )}" )

    }

    test( "Determinise an NFA and print out extra information" ) {
        //  second one
        val nfa3 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ), ( 0 ~> 2 )( "a" ), ( 1 ~> 0 )( "b" ), ( 1 ~> 2 )( "a" ) ),
            Set( 2 ),
            name = "nfa3" )

        //  get a pair, det NFA and a Map from states (Int) to String
        val ( det, nodeInfo ) = toDetNFA(
            nfa3,
            { x : Set[ Int ] ⇒ "{" + x.mkString( "," ) + "}" } )

        val nfa2Dot : DotSpec =
            toDot(
                det,
                //  map for node labels
                //  this is the label that will be displayed in Dot
                nodeProp = {
                    x : Int ⇒
                        if ( !det.accepting( x ) )
                            List(
                                Attribute( "xxlabel", StringLit( "node" + x ) ),
                                Attribute( "height", StringLit( "0.4" ) ),
                                Attribute( "fixedsize", StringLit( "true" ) ),
                                Attribute( "label", StringLit( nodeInfo( x ) ) ) )
                        else
                            List(
                                Attribute( "shape", Ident( "rectangle" ) ),
                                Attribute( "label", StringLit( nodeInfo( x ) ) ),
                                Attribute( "height", StringLit( "0.4" ) ),
                                Attribute( "fixedsize", StringLit( "true" ) ),
                                Attribute( "xxlabel", StringLit( "node" + x ) ) )
                },
                //  map for node identifiers
                //  this is the node ID e.g. edges will be output
                //  as nodeIDsrc -> nodeIDtgt [some edge attributes]
                labelDotName = {
                    x : String ⇒ " " + x + " "
                },
                graphProp = {
                    () ⇒
                        List()
                },
                graphDirective = {
                    () ⇒
                        List(
                            "rank = sink ; 2 ",
                            "rank = source ; 0" )
                } )

        logger.info( s"nfa3.dot\n ${show( nfa2Dot )}" )

    }

    test( "Convert a transition-less NFA to DOT, check that states are still present" ) {
        val nfa = NFA( Set( "0" ), Set.empty[ LabDiEdge[ String, Int ] ], Set( "0" ) )

        val nfaDot : DotSpec = toDot( nfa )
        //  print automaton in a file
        logger.info( s"nfa.dot\n ${show( nfaDot )}" )

        val nfaDotString = show( nfaDot )

        // now parse file and check it is the same as the automaton we started
        // with
        val tryNfa : Try[ DotSpec ] = parseString( nfaDotString )

        assert( tryNfa.isSuccess )
        //  build an NFA. As we read strings we have to convert
        val readInnfa1 = NFA(
            getInitStates( tryNfa.get ),
            getEdges( tryNfa.get ), getAcceptStates( tryNfa.get ) )
        assert( nfa == readInnfa1 )
    }
}
