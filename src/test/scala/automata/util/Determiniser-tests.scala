/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat
package util
package tests

import org.scalatest._
import edge.LabDiEdge
import edge.Implicits._
import auto.NFA
import Traversal.DFS
import Determiniser.toDetNFA
import org.bitbucket.franck44.dot.DOTPrettyPrinter.show
import DotConverter._
import org.bitbucket.franck44.dot.DOTSyntax.DotSpec

class DeterminiserTests extends FunSuite with Matchers {

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    //  logger
    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    test( "Determinise an NFA with no transition which is a DFA, check equality" ) {
        //  first one has no transition
        val nfa1 = NFA[ Int, String ]( Set( 0 ), Set(), Set() )
        val det = toDetNFA( nfa1 )._1
        assert( nfa1 == det )
    }

    test( "Determinise an NFA and check result" ) {
        //  second one
        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ), ( 0 ~> 2 )( "a" ), ( 1 ~> 0 )( "b" ), ( 1 ~> 2 )( "a" ) ),
            Set( 2 ) )
        val det = toDetNFA( nfa2 )._1

        val result = NFA(
            Set( 0 ),
            Set(
                ( 1 ~> 2 )( "a" ), ( 1 ~> 0 )( "b" ), ( 0 ~> 1 )( "a" ) ),
            Set( 1, 2 ) )

        assert( result == det )
    }

    test( "Automata with acceptsNone states" ) {
        //  second one
        val nfa1 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ), ( 1 ~> 2 )( "b" ), ( 2 ~> 3 )( "c" ), ( 2 ~> 1 )( "a" ) ),
            Set( 2 ),
            Set(),
            Set( 3 ),
            name = "" )

        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set( ( 0 ~> 1 )( "a" ), ( 1 ~> 2 )( "c" ) ),
            Set( 2 ),
            Set( 2 ) )
        val r = toDetNFA( nfa1 + nfa2 )._1
        val rDot : DotSpec = toDot( r )
        //  print automaton in a file
        logger.info( s"r.dot\n ${show( rDot )}" )
    }

    test( "Automata with Map as states" ) {
        //  An automaton to determinise. It is already deterninitic so should give an
        //  isomorphic one
        //  states are Maps and labels are String

        val s0 = Map ( 0 → "0" )
        val s1 = Map( 0 → "0", 1 → "1" )
        val s2 = Map( 0 → "20", 1 → "10", 10 → "s" )
        // val s3 = Map( 1 → "20", 3 → "10", 10 → "s" )

        val nfa1 = NFA[ Map[ Int, String ], String ](
            Set( s0 ),
            Set(
                ( s0 ~> s1 )( "a" ),
                ( s1 ~> s2 )( "b" ),
                // ( s2 ~> s3 )( "c" ),
                ( s2 ~> s1 )( "a" ) ),
            Set( s2 ),
            Set(),
            Set(),
            name = "Automaton with Map states" )

        val r = toDetNFA( nfa1 )._1
        val rDot : DotSpec = toDot( r )
        //  print automaton in a file
        logger.info( s"mapStates.dot\n ${show( rDot )}" )
    }

    test( "Automata with ListMap as states" ) {
        //  An automaton to determinise. It is already deterninitic so should give an
        //  isomorphic one
        //  states are Maps and labels are String
        import scala.collection.mutable.ListMap

        val s0 = ListMap ( 0 → "0" )
        val s1 = ListMap( 0 → "0", 1 → "1" )
        val s2 = ListMap( 0 → "20", 1 → "10", 10 → "s" )
        // val s3 = Map( 1 → "20", 3 → "10", 10 → "s" )

        val nfa1 = NFA[ ListMap[ Int, String ], String ](
            Set( s0 ),
            Set(
                ( s0 ~> s1 )( "a" ),
                ( s1 ~> s2 )( "b" ),
                // ( s2 ~> s3 )( "c" ),
                ( s2 ~> s1 )( "a" ) ),
            Set( s2 ),
            Set(),
            Set(),
            name = "Automaton with Map states" )

        val r = toDetNFA( nfa1 )._1
        val rDot : DotSpec = toDot( r )
        //  print automaton in a file
        logger.info( s"listMapStates.dot\n ${show( rDot )}" )
    }

    test( "Automata with List as states" ) {
        //  An automaton to determinise. It is already deterninitic so should give an
        //  isomorphic one
        //  states are Maps and labels are String

        val s0 = Map ( 0 → "0" ).toList
        val s1 = Map( 0 → "0", 1 → "1" ).toList
        val s2 = Map( 0 → "20", 1 → "10", 10 → "s" ).toList
        // val s3 = Map( 1 → "20", 3 → "10", 10 → "s" )

        val nfa1 = NFA[ List[ ( Int, String ) ], String ](
            Set( s0 ),
            Set(
                ( s0 ~> s1 )( "a" ),
                ( s1 ~> s2 )( "b" ),
                // ( s2 ~> s3 )( "c" ),
                ( s2 ~> s1 )( "a" ) ),
            Set( s2 ),
            Set(),
            Set() )

        val r = toDetNFA( nfa1 )._1
        val rDot : DotSpec = toDot( r )
        //  print automaton in a file
        logger.info( s"mapToListStates.dot\n ${show( rDot )}" )
    }

    test( "Automata with acceptsAll states and outgoing edges thereof" ) {
        //  second one
        intercept[ java.lang.Exception ]{
            val nfa7 = NFA[ Int, String ](
                Set( 0 ),
                Set(
                    ( 0 ~> 1 )( "a" ), ( 1 ~> 2 )( "b" ), ( 2 ~> 3 )( "c" ), ( 2 ~> 1 )( "a" ) ),
                Set( 2 ),
                Set( 2 ),
                Set(),
                name = "Accepts all in 2" )
        }
    }

    test( "Automata with acceptsAll states" ) {
        //  second one
        val nfa7 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ), ( 1 ~> 2 )( "b" )
            // , ( 2 ~> 3 )( "c" ), ( 2 ~> 1 )( "a" )
            ),
            Set( 2 ),
            Set( 2 ),
            Set(),
            name = "Accepts all in 2" )

        val r = toDetNFA( nfa7 )._1
        val rDot : DotSpec = toDot( r )
        //  print automaton in a file
        logger.info( s"nfa7.dot\n ${show( rDot )}" )
    }

}
