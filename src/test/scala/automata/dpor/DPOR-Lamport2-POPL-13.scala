/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat
package dpor
package tests

import org.scalatest._

import auto.{ NFA, DetAuto, FreeProduct }
import edge.LabDiEdge
import edge.Implicits._
import util.Traversal.DFS
import dpor.Traversal.DFSDPOR
import util.Determiniser.toDetNFA
import util.Determiniser.DeterminiserVisitor
import util.DotConverter._
import scala.util.{ Try, Success, Failure }
import org.bitbucket.franck44.dot.DOTSyntax.{ DotSpec, Attribute, Ident }
import org.bitbucket.franck44.dot.DOTPrettyPrinter.show

/**
 *  Helpers to create actions, extract processes of an action
 * and read/written variables.
 */
object ConcurrentTrace {

    /** Round percentage */
    def myRound( x : Float ) : Float = ( x * 1000 ).toFloat.round.toFloat / 10

    def showDFSvsDFSDPORToDot( a : NFA[ Int, ( Int, Action ) ], v : DeterminiserVisitor[ Set[ Int ], ( Int, Action ), String ], title : String ) : DotSpec =
        toDot(
            a.copy( name = s"$title ${v.edges.size}/${a.edges.size}, ratio = ${myRound( v.edges.size.toFloat / a.edges.size )}%" ),
            nodeProp = { s : Int ⇒
                if ( v.index.isDefinedAt( Set( s ) ) )
                    List(
                        Attribute( "style", Ident( "filled" ) ),
                        Attribute( "fillcolor", Ident( "orange" ) ),
                        Attribute( "color", Ident( "orange" ) ),
                        Attribute( "fontcolor", Ident( "black" ) ) )
                else List(
                    Attribute( "style", Ident( "dashed" ) ),
                    Attribute( "fillcolor", Ident( "grey" ) ),
                    Attribute( "color", Ident( "grey" ) ),
                    Attribute ( "fontcolor", Ident( "grey" ) ) )
            },
            edgeProp = { e : LabDiEdge[ Int, ( Int, Action ) ] ⇒
                val exploredEdgesStyle = List(
                    Attribute( "color", Ident( "orange" ) ),
                    Attribute( "fontcolor", Ident( "orange" ) ) )
                val unexploredEdgesStyle = List(
                    Attribute( "style", Ident( "dashed" ) ),
                    Attribute( "color", Ident( "grey" ) ),
                    Attribute( "fontcolor", Ident( "grey" ) ) )

                if ( v.index.isDefinedAt( Set( e.src ) ) && v.index.isDefinedAt( Set( e.tgt ) ) )
                    if ( v.edges.contains( ( v.index( Set( e.src ) ) ~> v.index( Set( e.tgt ) ) )( e.lab ) ) )
                        exploredEdgesStyle
                    else
                        unexploredEdgesStyle
                else
                    unexploredEdgesStyle

            } )

    case class Action(
        name : String,
        read : Set[ String ] = Set(),
        write : Set[ String ] = Set() )

    /** The process id of an action. */
    def proc : ( ( Int, Action ) ) ⇒ Int = { x ⇒ x._1 }

    /**
     *  Two actions in same process are dependent, and otherwise
     *  if one is a Write on a shared variable.
     *  @param  l           A sequence of actions.
     *  @param  (x1,x2)     two indeices in the trace
     *  @return             Whether actions at x1 and x2 are dependent in the trace `l`.
     */
    def dep : Seq[ ( Int, Action ) ] ⇒ ( Int, Int ) ⇒ Boolean = {
        l ⇒
            {
                ( x1, x2 ) ⇒
                    ( proc( l( x1 ) ) == proc( l( x2 ) ) ) ||
                        ( ( l( x1 )._2, l( x2 )._2 ) match {
                            case ( s1, s2 ) if ( s1.read & s2.write ).nonEmpty ⇒ true
                            case ( s1, s2 ) if ( s1.write & s2.read ).nonEmpty ⇒ true
                            case ( s1, s2 ) if ( s1.write & s2.write ).nonEmpty ⇒ true
                            case _ ⇒ false
                        } )
            }
    }
}

/**
 * Tests for Dynamic partial order reduction Lamport Bakery
 */
class DFSDPORLamportBakeryTests extends FunSuite with Matchers {
    import ConcurrentTrace._
    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    //  logger
    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    ignore( "Example 1 [Farzan et al., POPL 2013]" ) {

        logger.debug( "Example 1  [Farzan et al., POPL 2013]" )
        val threadA = NFA[ Int, Action ](
            init = Set( 0 ),
            transitions = Set(
                ( 0 ~> 1 )( Action( "e1 := true", Set(), Set( "e1" ) ) ),
                ( 1 ~> 2 ) ( Action( "tmp1 := n2", Set( "n2" ), Set( "tmp1" ) ) ),
                ( 2 ~> 3 ) ( Action( "n1 := tmp1 + 1", Set( "tmp1" ), Set( "n1" ) ) ),
                ( 3 ~> 4 ) ( Action( "e1 := false", Set(), Set( "e1" ) ) ),
                ( 4 ~> 5 ) ( Action( "[not(e2)]", Set( "e2" ), Set() ) ),
                ( 5 ~> 6 ) ( Action( "n2 = 0 or n2 >= n1", Set( "n1", "n2" ), Set() ) ),
                ( 6 ~> 7 ) ( Action( "n1 := 0", Set(), Set( "n1" ) ) ) ),
            accepting = Set ( 7 ),
            name = "threadA" )

        val threadB = NFA[ Int, Action ](
            init = Set( 0 ),
            transitions = Set(
                ( 0 ~> 1 )( Action( "e2 := true", Set(), Set( "e2" ) ) ),
                ( 1 ~> 2 ) ( Action( "tmp2 := n1", Set( "n1" ), Set( "tmp2" ) ) ),
                ( 2 ~> 3 ) ( Action( "n2 := tmp2 + 1", Set( "tmp2" ), Set( "n2" ) ) ),
                ( 3 ~> 4 ) ( Action( "e2 := false", Set(), Set( "e2" ) ) ),
                ( 4 ~> 5 ) ( Action( "[not(e1)]", Set( "e1" ), Set() ) ),
                ( 5 ~> 6 ) ( Action( "n1 = 0 or n1 > n2", Set( "n1", "n2" ), Set() ) ),
                ( 6 ~> 7 ) ( Action( "n2 := 0", Set(), Set( "n2" ) ) ) ),
            accepting = Set ( 7 ),
            name = "threadB" )

        val prod = new FreeProduct( Seq( threadA, threadB ) )

        //  detProd is an NFA[Int, (Int,Action)]
        val ( detProd, info ) = toDetNFA( prod )
        logger.debug( "Initial automaton" )
        logger.debug( show( toDot( detProd ) ) )

        //  Compute the determinised version with a visitor and collect states and edges
        val DFSdeterminiser = DFSDPOR(
            DeterminiserVisitor[ Set[ Int ], ( Int, Action ), String ](
                Map(),
                List(),
                { _ ⇒ "" },
                Map() ),
            detProd.outGoingEdges,
            proc,
            MzEquiv( dep ) )
        val v = DFSdeterminiser( detProd.getInit )

        // create an NFA using the visitor collected states and edges in the DFSDPOR
        // val detReduced =
        //     new NFA(
        //         init = Set( v.index( detProd.getInit ) ),
        //         transitions = v.edges.toSet,
        //         accepting = Set(),
        //         name = "DFSDPOR Example 1" )

        //  Expected search tree for DFSDPOR
        // val expectedResult = NFA[ Int, ( Int, Action ) ](
        //     init = Set( 0 ),
        //     transitions = Set(
        //         ( 7 ~> 8 )( ( 1, Read( "x" ) ) ),
        //         ( 3 ~> 4 )( ( 1, Read( "x" ) ) ),
        //         ( 2 ~> 3 )( ( 1, Read( "y" ) ) ),
        //         ( 6 ~> 5 )( ( 1, Read( "x" ) ) ),
        //         ( 3 ~> 6 )( ( 0, Write( "x" ) ) ),
        //         ( 4 ~> 5 )( ( 0, Write( "x" ) ) ),
        //         ( 7 ~> 10 )( ( 0, Write( "x" ) ) ),
        //         ( 9 ~> 5 )( ( 2, Read( "x" ) ) ),
        //         ( 10 ~> 6 )( ( 2, Read( "x" ) ) ),
        //         ( 1 ~> 7 )( ( 1, Read( "y" ) ) ),
        //         ( 0 ~> 1 )( ( 2, Read( "z" ) ) ),
        //         ( 1 ~> 2 )( ( 2, Read( "x" ) ) ),
        //         ( 8 ~> 9 )( ( 0, Write( "x" ) ) ) ),
        //     accepting = Set(),
        //     name = "DFSDPOR Example 1" )
        // logger.debug( "DFSDPOR tree for Example 1  [Farzan et al., POPL 2013]" )
        // logger.debug( show( toDot( detReduced ) ) )
        // detReduced shouldBe expectedResult

        // logger.debug( "DFS/DFSDPOR tree for Example 1  [Farzan et al., POPL 2013]" )
        // logger.debug(
        //     show ( ConcurrentTrace.showDFSvsDFSDPORToDot( detProd, v, "Example 1  [Farzan et al., POPL 2013]" ) ) )
    }
}

