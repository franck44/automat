/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat
package dpor
package tests

import org.scalatest._

import auto.{ NFA, DetAuto, FreeProduct }
import edge.LabDiEdge
import edge.Implicits._
import util.Traversal.DFS
import dpor.Traversal.{ DFSDPOR, DFSDPORDiff, DFSDPORDiff2 }
import util.Determiniser.toDetNFA
import util.Determiniser.DeterminiserVisitor
import util.DotConverter._
import scala.util.{ Try, Success, Failure }
import org.bitbucket.franck44.dot.DOTSyntax.{ DotSpec, Attribute, Ident }
import org.bitbucket.franck44.dot.DOTPrettyPrinter.show
import DPORHelper._

/**
 * Tests for Dynamic partial order reduction on difference of automata.
 *
 */
class DFSDPORDiffAutoTests extends FunSuite with Matchers {
    import SimpleTrace._
    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    //  logger
    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    test( "Mz difference example 1" ) {

        logger.debug( "Mz difference example 1" )
        val p = NFA[ Int, ( Int, Action ) ](
            init = Set( 0 ),
            transitions = Set(
                ( 0 ~> 1 )( ( 0, Write( "x" ) ) ) ),
            accepting = Set ( 1 ),
            name = "p" )

        val q = NFA[ Int, ( Int, Action ) ](
            init = Set( 0 ),
            transitions = Set(
                ( 0 ~> 1 )( ( 0, Write( "x" ) ) ) ),
            accepting = Set ( 1 ),
            name = "q" )

        val DFSdeterminiser = DFSDPORDiff(
            DeterminiserVisitor[ ( ( Set[ Int ], Set[ Int ] ), Seq[ ( Int, Action ) ] ), ( Int, Action ), String ](
                Map(),
                List(),
                { _ ⇒ "" },
                Map() ),
            ( p + q ).outGoingEdges,
            ( p * q ).outGoingEdges,
            proc,
            MzEquiv( dep ) )
        val v = DFSdeterminiser( ( p - q ).getInit )

        // create an NFA using the visitor collected states and edges in the DFSDPOR
        val detReduced =
            new NFA(
                init = Set( v.index( ( ( p + q ).getInit, List[ ( Int, Action ) ]() ) ) ),
                transitions = v.edges.toSet,
                accepting = v.index.collect( { case ( ( k, _ ), v ) if ( p - q ).isFinal( k ) ⇒ v } ).toSet,
                name = "Mz difference example 1" )

        //  Expected search tree for DFSDPOR, accepts empty language
        val expectedResult = NFA[ Int, ( Int, Action ) ](
            init = Set( 0 ),
            transitions = Set(
                ( 0 ~> 1 )( ( 0, Write( "x" ) ) ) ),
            accepting = Set(),
            name = "DFSDPOR Example 1" )

        detReduced shouldBe expectedResult

        logger.debug( "DFS/DFSDPOR search tree for Mz difference Example 1" )
        logger.debug(
            show ( toDot( detReduced.copy( name = "DFS/DFSDPOR search tree for Mz difference Example 1" ) ) ) )
    }

    test( "Mz difference example 2" ) {

        logger.debug( "Mz difference example 2" )
        val p = NFA[ Int, ( Int, Action ) ](
            init = Set( 0 ),
            transitions = Set(
                ( 0 ~> 1 )( ( 0, Write( "x" ) ) ),
                ( 0 ~> 2 )( ( 1, Read( "y" ) ) ),
                ( 1 ~> 3 )( ( 1, Read( "y" ) ) ),
                ( 2 ~> 3 )( ( 0, Write( "x" ) ) ),
                ( 3 ~> 4 )( ( 0, Read( "z" ) ) ) ),
            accepting = Set ( 4 ),
            name = "p" )

        val q = NFA[ Int, ( Int, Action ) ](
            init = Set( 0 ),
            transitions = Set(
                ( 0 ~> 1 )( ( 1, Read( "y" ) ) ),
                ( 0 ~> 2 )( ( 0, Write( "x" ) ) ),
                ( 1 ~> 3 )( ( 0, Write( "x" ) ) ),
                ( 2 ~> 3 )( ( 1, Read( "y" ) ) ) ),
            accepting = Set ( 3 ),
            name = "q" )

        // logger.info( s"${( p -- q ).outGoingEdges( ( p -- q ).getInit )}" )
        logger.info( s"${( p - q ).outGoingEdges( ( p - q ).getInit )}" )
        val DFSdeterminiser = DFSDPORDiff(
            DeterminiserVisitor[ ( ( Set[ Int ], Set[ Int ] ), Seq[ ( Int, Action ) ] ), ( Int, Action ), String ](
                Map(),
                List(),
                { _ ⇒ "" },
                Map() ),
            ( p + q ).outGoingEdges,
            ( p * q ).outGoingEdges,
            proc,
            MzEquiv( dep ) )
        val v = DFSdeterminiser( ( p + q ).getInit )

        // create an NFA using the visitor collected states and edges in the DFSDPOR
        val detReduced =
            new NFA(
                init = Set( v.index( ( ( p + q ).getInit, List() ) ) ),
                transitions = v.edges.toSet,
                accepting = v.index.collect( { case ( ( k, _ ), v ) if ( p - q ).isFinal( k ) ⇒ v } ).toSet,
                name = "Mz difference example 2" )

        //  Expected search tree for DFSDPOR, accepts empty language
        val expectedResult = NFA[ Int, ( Int, Action ) ](
            init = Set( 0 ),
            transitions = Set(
                ( 0 ~> 1 )( ( 0, Write( "x" ) ) ) ),
            accepting = Set(),
            name = "DFSDPOR Example 2" )

        // detReduced shouldBe expectedResult

        logger.debug( "DFS/DFSDPOR search tree for Mz difference Example 2" )
        logger.debug(
            show ( toDot( detReduced.copy( name = "DFS/DFSDPOR diff search tree for Mz difference Example 2" ) ) ) )
    }
}
