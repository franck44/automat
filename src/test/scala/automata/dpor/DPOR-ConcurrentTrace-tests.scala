/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat
package dpor
package tests

import org.scalatest._

/**
 *  Helpers to create actions and extract processes of an action.
 */
object SimpleTrace {

    trait Action {
        def name : String
    }

    case class Read( name : String ) extends Action
    case class Write( name : String ) extends Action

    /** The process id of an action. */
    def proc : ( ( Int, Action ) ) ⇒ Int = { x ⇒ x._1 }

    /**
     *  Two actions in same process are dependent, and otherwise
     *  if one is a Write on a shared variable.
     */
    def dep : Seq[ ( Int, Action ) ] ⇒ ( Int, Int ) ⇒ Boolean = {
        l ⇒
            {
                ( x1, x2 ) ⇒
                    ( proc( l( x1 ) ) == proc( l( x2 ) ) ) ||
                        ( ( l( x1 )._2, l( x2 )._2 ) match {
                            case ( Write( l ), Read( k ) ) if l == k ⇒ true
                            case ( Read( l ), Write( k ) ) if l == k ⇒ true
                            case _                                   ⇒ false
                        } )
            }
    }
}

/**
 * Tests for Dynamic partial order reduction operations (POPL'14)
 */
class MzTraceTests extends FunSuite with Matchers {

    import SimpleTrace._

    override def suiteName = "Check happens-before and DPOR operators "

    test( "empty trace" ) {
        val ct1 = MzTrace[ ( Int, Action ) ]( Seq(), proc )
        val bt1 = MzAnalyser( ct1, MzEquiv( dep ) )

        bt1.happensBefore shouldBe empty
        assertThrows[ IllegalArgumentException ] { bt1.<( 2 ) }
        assertThrows[ IllegalArgumentException ] { bt1.initial( 0 ) }
        assertThrows[ IllegalArgumentException ] { bt1.initialForceLast( 1 ) }
    }

    test( "a single process with 1 action (0,write_x)" ) {
        val t1 = Seq( ( 0, Write( "x" ) ) )
        val ct1 = MzTrace[ ( Int, Action ) ]( t1, proc )
        val bt1 = MzAnalyser( ct1, MzEquiv( dep ) )

        bt1.happensBefore shouldBe Map( 0 → List() )
        bt1.<( 0 ) shouldBe empty
        bt1.initial( 0 ) shouldBe empty
        //  0 <.  Last element not satisfied
        assertThrows[ IllegalArgumentException ] { bt1.initialForceLast( 0 ) }
    }

    test( "single process with 2 dependent actions (0,read_x)(0,read_y)" ) {
        val t1 = Seq( ( 0, Read( "x" ) ), ( 0, Read( "y" ) ) )
        val ct1 = MzTrace[ ( Int, Action ) ]( t1, proc )
        val bt1 = MzAnalyser( ct1, MzEquiv( dep ) )

        bt1.happensBefore shouldBe Map( 0 → List(), 1 → List( 0 ) )
        bt1.<( 0 ) shouldBe empty
        bt1.<( 1 ) shouldBe empty
        bt1.initial( 0 ) shouldBe empty
        bt1.initial( 1 ) shouldBe empty
        //  0 <.  Last element not satisfied
        assertThrows[ IllegalArgumentException ] { bt1.initialForceLast( 0 ) }
        //  Cannot ask for addToLast using the last index
        assertThrows[ IllegalArgumentException ] { bt1.initialForceLast( 1 ) }
    }

    test( "single process with 2 dependent actions (0,read_x)(0,write_x)" ) {
        val t1 = Seq( ( 0, Read( "x" ) ), ( 0, Write( "x" ) ) )
        val ct1 = MzTrace[ ( Int, Action ) ]( t1, proc )
        val bt1 = MzAnalyser( ct1, MzEquiv( dep ) )

        bt1.happensBefore shouldBe Map( 0 → List(), 1 → List( 0 ) )
        bt1.<( 0 ) shouldBe empty
        bt1.<( 1 ) shouldBe empty
        bt1.initial( 0 ) shouldBe empty
        bt1.initial( 1 ) shouldBe empty

        //  0 <.  Last element not satisfied
        assertThrows[ IllegalArgumentException ] { bt1.initialForceLast( 0 ) }
        //  Cannot ask for addToLast using the last index
        assertThrows[ IllegalArgumentException ] { bt1.initialForceLast( 1 ) }
    }

    test( "2 processes with independent actions (0,write_x)(1,read_y)" ) {
        val t1 = Seq( ( 0, Write( "x" ) ), ( 1, Read( "y" ) ) )
        val ct1 = MzTrace[ ( Int, Action ) ]( t1, proc )
        val bt1 = MzAnalyser( ct1, MzEquiv( dep ) )

        bt1.happensBefore shouldBe Map( 0 → List(), 1 → List() )
        bt1.<( 0 ) shouldBe empty
        bt1.<( 1 ) shouldBe empty
        bt1.initial( 0 ) shouldBe List( 1 )
        //  0 <.  Last element not satisfied
        assertThrows[ IllegalArgumentException ] { bt1.initialForceLast( 0 ) }
    }

    test( "2 processes with dependent actions (0,write_x)(1,read_x)" ) {
        val t1 = Seq( ( 0, Write( "x" ) ), ( 1, Read( "x" ) ) )
        val ct1 = MzTrace[ ( Int, Action ) ]( t1, proc )
        val bt1 = MzAnalyser( ct1, MzEquiv( dep ) )

        bt1.happensBefore shouldBe Map( 0 → List(), 1 → List( 0 ) )
        bt1.<( 0 ) shouldBe empty
        bt1.<( 1 ) shouldBe List( 0 )
        bt1.initial( 0 ) shouldBe List()
        bt1.initialForceLast( 0 ) shouldBe List( 1 )
    }

    test( "2 processes with dependent actions (0,write_x)(1,read_y)(1,read_x)" ) {
        val t1 = Seq( ( 0, Write( "x" ) ), ( 1, Read( "y" ) ), ( 1, Read( "x" ) ) )
        val ct1 = MzTrace[ ( Int, Action ) ]( t1, proc )
        val bt1 = MzAnalyser( ct1, MzEquiv( dep ) )

        bt1.happensBefore shouldBe Map( 0 → List(), 1 → List(), 2 → List( 1, 0 ) )
        bt1.<( 0 ) shouldBe empty
        bt1.<( 1 ) shouldBe List()
        bt1.<( 2 ) shouldBe List( 0 )
        bt1.initial( 0 ) shouldBe List( 1 )
        bt1.initialForceLast( 0 ) shouldBe List( 1 )
    }

    test( "2 processes with dependent actions (0,write_x)(0,read_y)(1,read_x)" ) {
        val t1 = Seq( ( 0, Write( "x" ) ), ( 0, Read( "y" ) ), ( 1, Read( "x" ) ) )
        val ct1 = MzTrace[ ( Int, Action ) ]( t1, proc )
        val bt1 = MzAnalyser( ct1, MzEquiv( dep ) )

        bt1.happensBefore shouldBe Map( 0 → List(), 1 → List( 0 ), 2 → List( 0 ) )
        bt1.<( 0 ) shouldBe empty
        bt1.<( 1 ) shouldBe List()
        bt1.<( 2 ) shouldBe List( 0 )
        bt1.initial( 0 ) shouldBe List()
        bt1.initialForceLast( 0 ) shouldBe List( 2 )
    }

    test( "3 processes with dependent actions (0,write_x)(2,read_y)(1,read_z)(2,read_x)(1,read_x)" ) {
        val t1 = Seq(
            ( 0, Write( "x" ) ),
            ( 2, Read( "y" ) ),
            ( 1, Read( "z" ) ),
            ( 2, Read( "x" ) ),
            ( 1, Read( "x" ) ) )
        val ct1 = MzTrace[ ( Int, Action ) ]( t1, proc )
        val bt1 = MzAnalyser( ct1, MzEquiv( dep ) )

        bt1.happensBefore shouldBe Map(
            0 → List(),
            1 → List(),
            2 → List(),
            3 → List( 1, 0 ),
            4 → List( 2, 0 ) )
        bt1.<( 0 ) shouldBe empty
        bt1.<( 1 ) shouldBe List()
        bt1.<( 2 ) shouldBe List()
        bt1.<( 3 ) shouldBe List( 0 )
        bt1.<( 4 ) shouldBe List( 0 )
        bt1.initial( 0 ) shouldBe List( 1, 2 )
        bt1.initialForceLast( 0 ) shouldBe List( 1, 2 )
    }

    test( "Update sleep = (1,read_y) set after <>.(0,write_x)" ) {
        val t1 = Seq[ ( Int, Action ) ]( ( 0, Write( "x" ) ) )
        val ct1 = MzTrace[ ( Int, Action ) ]( t1, proc )
        val sleep = Set[ ( Int, Action ) ](
            ( 1, Read( "y" ) ) )
        val bt1 = MzAnalyser( ct1, MzEquiv( dep ) )
        bt1.indepOfLastIn ( sleep ) shouldBe sleep
    }

    test( "Update sleep = (1,write_x) set after <>.(0,write_x)" ) {
        val t1 = Seq[ ( Int, Action ) ]( ( 0, Read( "x" ) ) )
        val ct1 = MzTrace[ ( Int, Action ) ]( t1, proc )
        val sleep = Set[ ( Int, Action ) ](
            ( 1, Write( "x" ) ) )
        val bt1 = MzAnalyser( ct1, MzEquiv( dep ) )
        bt1.indepOfLastIn ( sleep ) shouldBe empty
    }

    test( "Update sleep = (0,read_z)(1,write_x)(2,read_y) set after <>(0,read_x)" ) {
        val t1 = Seq[ ( Int, Action ) ]( ( 0, Read( "x" ) ) )
        val ct1 = MzTrace[ ( Int, Action ) ]( t1, proc )
        val sleep = Set[ ( Int, Action ) ](
            ( 0, Read( "z" ) ),
            ( 1, Write( "x" ) ),
            ( 2, Read( "y" ) ) )
        val bt1 = MzAnalyser( ct1, MzEquiv( dep ) )
        bt1.indepOfLastIn ( sleep ) shouldBe Set( ( 2, Read( "y" ) ) )
    }

    test( "Update sleep = (0,read_z)(1,write_x)(2,read_y)  set after <>.(2, read_z)" ) {
        val t1 = Seq[ ( Int, Action ) ]( ( 2, Read( "z" ) ) )
        val ct1 = MzTrace[ ( Int, Action ) ]( t1, proc )
        val sleep = Set[ ( Int, Action ) ](
            ( 0, Read( "z" ) ),
            ( 1, Write( "x" ) ) )
        val bt1 = MzAnalyser( ct1, MzEquiv( dep ) )
        bt1.indepOfLastIn ( sleep ) shouldBe sleep
    }

}
