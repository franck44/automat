/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat

import org.scalatest._
import edge.LabDiEdge
import edge.Implicits._
import auto.{ NFA, Union }

class UnionTests extends FunSuite with Matchers {
    //  first one has no transition
    val nfa1 = NFA[ Int, String ]( Set( 0 ), Set(), Set() )
    //  second one
    val nfa2 = NFA[ Int, String ](
        Set( 0 ),
        Set(
            ( 0 ~> 1 )( "a" ), ( 0 ~> 2 )( "a" ), ( 1 ~> 0 )( "b" ), ( 1 ~> 2 )( "a" ) ),
        Set( 2 ) )

    //  third one
    val nfa3 = NFA[ Int, String ](
        Set( 0 ),
        Set(
            ( 0 ~> 2 )( "a" ), ( 0 ~> 1 )( "b" ), ( 1 ~> 0 )( "b" ), ( 1 ~> 2 )( "a" ) ),
        Set( 2 ) )

    val p1 = Union( nfa1, nfa2 );
    val initState = p1.getInit

    test( "Check final states for product with one empty automaton" ) {
        import p1.{ succ, isFinal }

        assert( !isFinal( initState ) )
        //  succ after a
        val s3 = succ( initState, List( "a", "b", "a" ) )
        assert( isFinal( s3 ) )
        // val s4 = succ(s3,"d")
        // assert(isDummy(s4))
    }

    val p2 = Union( nfa2, nfa3 )
    val initState2 = p2.getInit

    test( "Check final states for 2 automata" ) {
        import p2.{ succ, isFinal }

        //  succ after a.a should not be final state (no final in SyncProd)
        val s3 = succ( succ( initState, "b" ), "b" )
        //
        assert( !isFinal( s3 ) )
        val s4 = succ( s3, List( "a" ) )
        assert( isFinal( s4 ) )
        // assert(!isFinal(succ(initState,"b")))
    }

    test( "Use operators \\/ to create union" ) {
        val p = nfa1 + nfa2
        assert( p == Union( nfa1, nfa2 ) )
        //  notice that nfa1 | nfa2 is not equal to nfa2 | nfa1
        assert( nfa1 + nfa2 != nfa2 + nfa1 )
    }

}
