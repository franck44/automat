/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat

package util
package tests

import org.scalatest._
import edge.LabDiEdge
import edge.Implicits._
import util.Minimiser._
import auto.NFA

import org.scalatest.prop.TableDrivenPropertyChecks

/**
 * Minimisation tests.
 */
class NFAMinimTests extends FunSuite with TableDrivenPropertyChecks with Matchers with PrivateMethodTester {

    /**
     * splitClass tests
     * Given a class of states S, a set of transitions T, a partition P,
     * split S into S1, S2, ... , Sk such that any two states in Si:
     *  1. have the same set of enabled labels
     *  2. for each lable a, they lead to equivalent state.
     * 
     */

    //  format: OFF
    val splitClassTests = Table[    Set[Int], 
                                     Set[LabDiEdge[Int, String]], 
                                     Map[Int, Int], 
                                     Set[Set[Int]] ](
    ("Class to split",      "Transitions",          "Partition",    "Result"),
    (Set(),             Set( ( 1 ~> 1 )( "a" ) ),   Map(),          Set() ),
    (Set( 0 ),          Set( ( 0 ~> 1 )( "a" ) ),   Map( 0 -> 2 ),  Set( Set( 0 ))),
    (Set( 0, 1, 2 ),    Set( ( 0 ~> 1 )( "a" ), 
                             ( 2 ~> 1 )( "a" ) ),   Map( 1 -> 1 ),  Set( 
                                                                        Set( 0, 2 ), 
                                                                        Set( 1 ))),
    (Set( 0, 1, 2 ),    Set( ),                     Map( 1 -> 1 ),  Set( 
                                                                        Set( 0, 1, 2 ))),

    (Set( 0, 1, 2 ),    Set( ( 0 ~> 1 )( "a" ),
                             ( 2 ~> 1 )( "b" )),    Map( 1 -> 1),   Set( Set( 0 ), 
                                                                         Set( 2 ), 
                                                                         Set( 1 ))),
    (Set( 0, 1, 2 ),    Set( ( 0 ~> 1 )( "a" ), 
                             ( 2 ~> 1 )( "b" ), 
                             ( 1 ~> 0 )( "b" ) ),   Map( 0 -> 2,
                                                         1 -> 2, 
                                                         2 -> 0 ), Set( Set( 0 ), 
                                                                        Set( 1, 2 )))
    )
    //  format: ON

    val splitClassMethod = PrivateMethod[ Set[ Set[ Int ] ] ]( 'splitClass )

    for ( ( c, t, p, result ) ← splitClassTests ) {
        test( s"SplitClass $c, transitions $t, partition $p -- should yield $result" ) {
            val grouped = t.groupBy( _.src ).mapValues( _.map { e ⇒ ( e.lab, e.tgt ) } )
            ( Minimiser invokePrivate splitClassMethod( c, grouped, Partition( p ) ) ) shouldBe result
        }
    }

    /**
     * splitPartition tests
     * Given a partition p i.e. a set of classes, and some transitions,
     * split each class in p according to splitClass.
     *
     */

    //  format: OFF
    val splitPartitionTests = Table[Map[Int, Int], 
                                     Set[LabDiEdge[Int, String]], 
                                     Map[Int,Int] ](
    ("Initial partition",      "Transitions",               "Split Partition"),
    (Map(),                     Set( ( 1 ~> 1 )( "a" ) ),   Map() ),
    (Map( 0 -> 1),              Set(),                      Map(0 -> 1)),
    (Map( 0 -> 2,
          1 -> 2, 
          2 -> 0 ),             Set( ( 0 ~> 1 )( "a" ), 
                                     ( 2 ~> 1 )( "b" ), 
                                     ( 1 ~> 0 )( "b" ) ),   Map(0 -> 0, 1 -> 1, 2 -> 2)),   
    (Map( 0 -> 2,
          1 -> 2, 
          2 -> 2 ),             Set( ( 0 ~> 1 )( "a" ), 
                                     ( 2 ~> 1 )( "a" ), 
                                     ( 1 ~> 0 )( "b" ) ),   Map(0 -> 2, 1 -> 0, 2 -> 2))
    )
    //  format: ON

    val splitPartitionMethod = PrivateMethod[ Partition[ Int, Int ] ]( 'splitPartition )

    for ( ( p, t, result ) ← splitPartitionTests ) {
        test( s"splitPartition $p, transitions $t -- should yield $result" ) {
            val grouped = t.groupBy( _.src ).mapValues( _.map { e ⇒ ( e.lab, e.tgt ) } )
            ( Minimiser invokePrivate
                splitPartitionMethod( grouped, Partition( p ) ) ).allClasses shouldBe Partition( result ).allClasses
        }
    }


     /**
     * defaultPartition tests
     */
     //  format: OFF
    val defaultPartitionTests = Table[NFA[Int,String], Partition[Int,Int] ](
    (   "NFA",                                      "Default Partition"),
    (   NFA(Set(), Set(), Set() ),                  Partition(Map())),
    (   NFA(Set(0), Set(), Set() ),                 Partition(Map(0 -> 3))),
    (   NFA(Set(0), Set(), Set(0) ),                Partition(Map(0 -> 1))),
    //  sinkAccept priority over accept
    (   NFA(Set(0), Set(), Set(0), Set(0) ),        Partition(Map(0 -> 0))),
    //  sinkReject
    (   NFA(Set(0), Set(), Set(), Set(), Set(0) ),  Partition(Map(0 -> 2))),
    //  2 accepting, erything else should be no property
    (   NFA(Set(1), Set(
                        (0 ~> 1)("A"), 
                        (1 ~> 3)("b")
                    ),
                    Set(2)),                        Partition(Map(2 -> 1, 1 -> 3, 3 -> 3, 0 -> 3))),
    //  sinkReject has priority over not property
    (   NFA(Set(1), Set(
                    (0 ~> 1)("A"), 
                    (1 ~> 3)("b")
                ),
                Set(2), Set(), Set(3)),             Partition(Map(2 -> 1, 1 -> 3, 3 -> 2, 0 -> 3)))

    
    )
    //  format: ON

    val defaultPartitionMethod = PrivateMethod[ Partition[ Int, Int ] ]( 'defaultPartition )

    for ( ( nfa, result ) ← defaultPartitionTests ) {
        test( s"defaultPartition for $nfa, -- should yield $result" ) {
            ( Minimiser invokePrivate
                defaultPartitionMethod( nfa ) ) shouldBe result
        }
    }

    /**
     * Minimisation tests.
     * getLanguageEquivStates and minimiseDet, minimiseNonDet
     *
     */
    test( "Minimisation of empty automaton should be the empty automaton" ) {

        val detAuto1 = NFA[ Int, String ](
            Set(),
            Set(),
            Set() )

        val aut = minimiseDet( detAuto1 )()

        getLanguageEquivStates( detAuto1 )() shouldBe Partition( Map() )

        aut shouldBe NFA[ Int, String ](
            Set(),
            Set(),
            Set() )
    }

    test( "Minimisation of automaton with no transitions but with an initial state" ) {

        val detAuto1 = NFA[ Int, String ](
            Set( 0 ),
            Set(),
            Set() )

        getLanguageEquivStates( detAuto1 )() shouldBe Partition( Map( 0 -> 0 ) )

        minimiseDet( detAuto1 )() shouldBe NFA[ Int, String ](
            Set( 0 ),
            Set(),
            Set() )
    }

    test( "Minimisation of automaton with no transitions but with an initial/accepting state" ) {

        val detAuto1 = NFA[ Int, String ](
            Set( 0 ),
            Set(),
            Set( 0 ) )

        getLanguageEquivStates( detAuto1 )() shouldBe Partition( Map( 0 -> 0 ) )

        minimiseDet( detAuto1 )() shouldBe NFA[ Int, String ](
            Set( 0 ),
            Set(),
            Set( 0 ) )
    }

    test( "Minimisation of automaton with no transitions but with an initial and an accepting state" ) {

        val detAuto1 = NFA[ Int, String ](
            Set( 0 ),
            Set(),
            Set( 1 ) )

        getLanguageEquivStates( detAuto1 )() shouldBe Partition( Map( 1 -> 0, 0 -> 1 ) )

        minimiseDet( detAuto1 )() shouldBe NFA[ Int, String ](
            Set( 1 ),
            Set(),
            Set( 0 ) )
    }

    test( "Minimisation of automaton with one init state, no final state" ) {
        val detAuto1 = NFA[ Int, Int ](
            Set( 1 ),
            Set( ( 1 ~> 1 )( 0 ) ),
            Set() )

        val aut = minimiseDet( detAuto1 )()

        getLanguageEquivStates( detAuto1 )() shouldBe Partition( Map( 1 -> 0 ) )

        aut shouldBe NFA[ Int, Int ](
            Set( 0 ),
            Set( ( 0 ~> 0 )( 0 ) ),
            Set() )
    }

    test( "Minimisation example 1" ) {

        /*
            0 - a -> 1 -|
                     ^  a with 0 initial and {0,1} final
                    |__|
        */
        val detAuto1 = NFA[ Int, String ](
            Set( 0 ),
            Set( ( 0 ~> 1 )( "a" ), ( 1 ~> 1 )( "a" ) ),
            Set( 0, 1 ) )

        val aut = minimiseDet( detAuto1 )()

        aut shouldBe NFA(
            Set( 0 ),
            Set( ( 0 ~> 0 )( "a" ) ),
            Set( 0 ) )
    }

    test( "Minimisation example 2 -- no sinkAccept nor sinkReject states" ) {

        val detAuto1 = NFA[ Int, Int ](
            Set( 0 ),
            Set(
                ( 0 ~> 0 )( 1 ),
                ( 0 ~> 1 )( 0 ),
                ( 1 ~> 1 )( 1 ),
                ( 1 ~> 0 )( 0 ),
                ( 2 ~> 0 )( 1 ),
                ( 2 ~> 1 )( 0 ) ),
            Set( 1 ) )

        val aut = minimiseDet( detAuto1 )()

        aut shouldBe NFA(
            Set( 1 ),
            Set(
                ( 0 ~> 0 )( 1 ),
                ( 0 ~> 1 )( 0 ),
                ( 1 ~> 1 )( 1 ),
                ( 1 ~> 0 )( 0 ) ),
            Set( 0 ) )
    }

    test( "Minimisation example 3 -- no sinkAccept nor sinkReject states" ) {

        val detAuto1 = NFA[ Int, Int ](
            Set( 0 ),
            Set(
                ( 0 ~> 0 )( 1 ),
                ( 0 ~> 1 )( 0 ),
                ( 1 ~> 1 )( 1 ),
                ( 1 ~> 0 )( 0 ),
                ( 1 ~> 2 )( 0 ),
                ( 2 ~> 0 )( 1 ),
                ( 2 ~> 1 )( 0 ) ),
            Set( 1 ) )

        val aut = minimiseDet( detAuto1 )()

        aut shouldBe NFA(
            Set( 1 ),
            Set(
                ( 0 ~> 0 )( 1 ),
                ( 0 ~> 1 )( 0 ),
                ( 1 ~> 1 )( 1 ),
                ( 1 ~> 0 )( 0 ) ),
            Set( 0 ) )
    }

    test( "Minimisation example 4 --  sinkAccept no sinkReject states" ) {

        val detAuto1 = NFA[ Int, Int ](
            Set( 0 ),
            Set(
                ( 0 ~> 0 )( 1 ),
                ( 0 ~> 1 )( 0 ) ),
            Set( 1 ),
            Set( 1 ) )

        val aut = minimiseDet( detAuto1 )()

        aut shouldBe NFA(
            Set( 0 ),
            Set(
                ( 0 ~> 0 )( 1 ),
                ( 0 ~> 1 )( 0 ) ),
            Set( 1 ),
            Set ( 1 ) )
    }

    test( "Minimisation example 5 --  initial partition given" ) {

        val detAuto1 = NFA[ Int, String ](
            Set( 1 ),
            Set(
                ( 1 ~> 1 )( "a" ),
                ( 1 ~> 2 )( "b" ),
                ( 1 ~> 0 )( "c" ),
                ( 2 ~> 0 )( "c" ) ),
            Set( 0 ) )

        val aut = minimiseDet( detAuto1 )( Partition( Map( 0 -> 0, 1 -> 1, 2 -> 2 ) ) )

        aut shouldBe NFA(
            Set( 1 ),
            Set(
                ( 1 ~> 1 )( "a" ),
                ( 1 ~> 0 )( "b" ),
                ( 1 ~> 2 )( "c" ),
                ( 0 ~> 2 )( "c" ) ),
            Set( 2 ) )
    }

    test( "Minimisation example 6 NonDet --  should merge 0 and 1" ) {

        val detAuto1 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 0 )( "a" ),
                ( 0 ~> 1 )( "a" ) ),
            Set( 0, 1 ) )

        val aut = minimiseNonDet( detAuto1 )

        aut shouldBe NFA(
            Set( 0 ),
            Set(
                ( 0 ~> 0 )( "a" ) ),
            Set ( 0 ) )
    }

    test( "Minimisation example 7 --  icanniot minimise if initialParition does not map all states" ) {

        val detAuto1 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 1 ~> 1 )( "a" ),
                ( 1 ~> 2 )( "b" ),
                ( 1 ~> 0 )( "c" ),
                ( 2 ~> 0 )( "c" ) ),
            Set( 0, 1 ) )

        an[ IllegalArgumentException ] should be thrownBy minimiseDet( detAuto1 )( Partition( Map( 0 -> 0, 1 -> 1 ) ) )

    }

}
