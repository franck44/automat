/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat

package util

import org.scalatest._
import edge.LabDiEdge
import edge.Implicits._
import auto.NFA
import Traversal.DFS
import Determiniser.toDetNFA
import org.bitbucket.franck44.dot.DOTPrettyPrinter.format
import DotConverter._
import org.bitbucket.franck44.dot.DOTSyntax.DotSpec

class AcceptsAllSimpleTests extends FunSuite with Matchers {

    test( "Try to create an NFA with non-emtpy sinkAccept and outgoing transitions from sinkAccept - should fail" ) {
        //  first one has no transition
        intercept[ java.lang.Exception ] {
            val nfa1 = NFA[ Int, String ](
                Set( 0 ),
                Set(
                    ( 0 ~> 1 )( "a" ),
                    ( 1 ~> 2 )( "b" ) ),
                Set( 1 ),
                Set( 1 ) )
        }
    }

    test( "Try to create an NFA with non-emtpy sinkReject and outgoing transitions from sinkReject - should fail" ) {
        //  first one has no transition
        intercept[ java.lang.Exception ] {
            val nfa1 = NFA[ Int, String ](
                Set( 0 ),
                Set(
                    ( 0 ~> 1 )( "a" ),
                    ( 1 ~> 2 )( "b" ) ),
                Set( 1 ),
                Set(),
                Set( 1 ) )
        }
    }

    test( "Try to create an NFA with non-emtpy sinkReject that intersects accepting - should fail" ) {
        //  first one has no transition
        intercept[ java.lang.Exception ] {
            val nfa1 = NFA[ Int, String ](
                Set( 0 ),
                Set(
                    ( 0 ~> 1 )( "a" ),
                    ( 1 ~> 2 )( "b" ) ),
                Set( 1 ),
                Set(),
                Set( 1 ) )
        }
    }

    test( "Try to create an NFA with non-emtpy sinkAccept not included in accepting - should fail" ) {
        //  first one has no transition
        intercept[ java.lang.Exception ] {
            val nfa1 = NFA[ Int, String ](
                Set( 0 ),
                Set(
                    ( 0 ~> 1 )( "a" ),
                    ( 1 ~> 2 )( "b" ) ),
                Set( 1 ),
                Set( 2 ),
                Set() )
        }
    }

    test( "One automaton a1, other a2 non-deterministic has sinkReject and is non-deterministic, check a1 - det(a2)" ) {

        val nfa1 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ),
                ( 1 ~> 2 )( "a" ),
                ( 2 ~> 2 )( "b" ) ),
            Set ( 1, 2 ) )

        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ),
                ( 1 ~> 2 )( "a" ),
                ( 1 ~> 3 )( "a" ),
                ( 3 ~> 3 )( "b" ) ),
            Set ( 2 ),
            Set( 2 ),
            Set() )
        import Determiniser.toDetNFA

        val p = nfa1 - toDetNFA( nfa2 )._1
        import p._
        acceptsNone( succ( getInit, Seq( "a", "a" ) ) ) shouldBe true

        //  now determinise p, no edges should go out of succ(init, a.a)
        val pDet = toDetNFA( p )._1
        pDet.outGoingEdges( pDet.succ( pDet.getInit, Seq( "a", "a" ) ) ) shouldEqual List()

    }

    ignore( "One automaton a1, other a2 non-deterministic has sinkReject but should not be reachable in diff" ) {

        val nfa1 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ) ),
            Set ( 1 ) )

        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ),
                ( 1 ~> 2 )( "b" ) ),
            Set (),
            Set(),
            Set( 2 ) )
        import Determiniser.toDetNFA
        import org.bitbucket.franck44.dot.DOTPrettyPrinter.show

        val p = nfa1 - nfa2

        import p._
        acceptsNone( succ( getInit, Seq( "a", "a" ) ) ) shouldBe true

        // //  now determinise p, no edges should go out of succ(init, a.a)
        val pDet = toDetNFA( p )._1
        pDet.outGoingEdges( pDet.succ( pDet.getInit, Seq( "a", "a" ) ) ) shouldEqual List()

    }

}
