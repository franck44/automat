/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat

package util

package tests

import org.scalatest._
import edge.LabDiEdge
import edge.Implicits._
import util.Projecter
import auto.NFA

class HideLabelsTests extends FunSuite with Matchers {

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    //  logger
    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    test( "ex1: Hide labels and detrminise a DetAuto" ) {
        val nfa1 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ),
                ( 1 ~> 2 )( "eps" ),
                ( 1 ~> 0 )( "eps" ),
                ( 1 ~> 4 )( "c" ),
                ( 2 ~> 3 )( "b" ) ),
            Set( 3 ) )

        //  get initial state
        val i = nfa1.getInitStar( Set( "a" ) )
        i shouldBe Set ( Set( 0 ), Set( 1 ) )
        // println( s1 )
        nfa1.succStar( Set( Set( 1 ) ), Set( "eps" ) ) shouldBe Set( Set( 1 ), Set( 2, 0 ) )

        nfa1.succStar( Set( Set( 4 ) ), Set( "eps" ) ) shouldBe Set( Set( 4 ) )

        nfa1.succStar( Set( Set( 1, 2 ) ), Set( "eps" ) ) shouldBe Set( Set( 1, 2 ), Set( 0, 2 ) )

        val ( r, m ) = Projecter.toProjectedNFA( nfa1, Set( "eps" ) )
        import util.DotConverter._
        import org.bitbucket.franck44.dot.DOTPrettyPrinter.show

        logger.info( show( toDot( r.copy( name = "ex1" ) ) ) )
        logger.info( s"Map for ex1: $m" )

    }

    test( "ex2: Hide labels and determinise a DetAuto" ) {
        val nfa1 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ),
                ( 1 ~> 0 )( "eps" ) ),
            Set( 1 ) )

        //  get initial state
        val i = nfa1.getInitStar( Set( "eps" ) )
        i shouldBe Set ( Set( 0 ) )
        // println( s1 )
        nfa1.succStar( Set( Set( 1 ) ), Set( "eps" ) ) shouldBe Set( Set( 0 ), Set( 1 ) )

        nfa1.succStar( Set( Set( 1 ) ), Set( "eps" ) ) shouldBe Set( Set( 0 ), Set ( 1 ) )

        nfa1.succStar( Set( Set( 1, 2 ) ), Set( "eps" ) ) shouldBe Set( Set( 1, 2 ), Set( 0 ) )

        // println( s )
        // assert( s === Set() )
        // val s1 = nfa1.succ( s, "d" )
        // assert( s1 === Set() )
        // assert( !nfa1.isFinal( s1 ) )
        // val s2 = nfa1.succ( nfa1.succ( i, "a" ), "a" )
        // assert( nfa1.isFinal( s2 ) )

        val ( r, m ) = Projecter.toProjectedNFA( nfa1, Set( "eps" ) )
        import util.DotConverter._
        import org.bitbucket.franck44.dot.DOTPrettyPrinter.show

        logger.info( show( toDot( r.copy( name = "ex2" ) ) ) )
        logger.info( s"Map for ex2: $m" )

    }

    test( "ex3: Hide labels and determinise a DetAuto" ) {
        val nfa1 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ),
                ( 0 ~> 0 )( "eps" ) ),
            Set( 1 ) )

        // println( s )
        // assert( s === Set() )
        // val s1 = nfa1.succ( s, "d" )
        // assert( s1 === Set() )
        // assert( !nfa1.isFinal( s1 ) )
        // val s2 = nfa1.succ( nfa1.succ( i, "a" ), "a" )
        // assert( nfa1.isFinal( s2 ) )

        val ( r, m ) = Projecter.toProjectedNFA( nfa1, Set( "eps" ) )
        import util.DotConverter._
        import org.bitbucket.franck44.dot.DOTPrettyPrinter.show

        logger.info( show( toDot( r.copy( name = "ex3" ) ) ) )
        logger.info( s"Map for ex3: $m" )

    }

}
