/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat

package auto

import org.scalatest._
import edge.LabDiEdge
import edge.Implicits._

class DiffPriorityTests extends FunSuite with Matchers {

    test( "Check order of enabled labels in A -- B, enabled in A empty, enabled in B empty " ) {

        //  first one
        val nfa1 = NFA[ Int, String ](
            Set( 0 ),
            Set(),
            Set() )

        //  second one
        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set(),
            Set( 2 ) )

        val p = DiffWithPriority( nfa1, nfa2 )
        import p._

        //  nfa1.enabled does not intersect nfa2.enabled
        enabledIn( getInit ).toList shouldBe List()
    }

    test( "Check order of enabled labels in A -- B, enabled in A non empty, enabled in B empty " ) {

        //  first one
        val nfa1 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ),
                ( 1 ~> 2 )( "a" ),
                ( 1 ~> 2 )( "b" ),
                ( 1 ~> 2 )( "c" ) ),
            Set() )

        //  second one
        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set(),
            Set( 2 ) )

        val p = DiffWithPriority( nfa1, nfa2 )
        import p._

        //  nfa1.enabled does not intersect nfa2.enabled
        enabledIn( getInit ).toList shouldBe List( "a" )

        //  advance nfa1 to state 1, all enabled from q should be there
        enabledIn( succ( getInit, "a" ) ).toSet shouldBe Set( "a", "b", "c" )
    }

    test( "Check order of enabled labels in A -- B, enabled in A empty, enabled in B non empty " ) {

        //  first one
        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ),
                ( 1 ~> 2 )( "a" ),
                ( 1 ~> 2 )( "b" ),
                ( 1 ~> 2 )( "c" ) ),
            Set() )

        //  second one
        val nfa1 = NFA[ Int, String ](
            Set( 0 ),
            Set(),
            Set( 2 ) )

        val p = DiffWithPriority( nfa1, nfa2 )
        import p._

        //  nfa1.enabled does not intersect nfa2.enabled
        enabledIn( getInit ).toList shouldBe List()

    }

    test( "Check order of enabled labels in A -- B, enabled in A is s1, enabled in B is s2, s1 & s2 is empty " ) {

        //  first one
        val nfa1 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ),
                ( 1 ~> 2 )( "a" ),
                ( 1 ~> 2 )( "c" ) ),
            Set(),
            Set() )

        //  second one
        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set( ( 0 ~> 1 )( "b" ) ),
            Set( 2 ) )

        val p = DiffWithPriority( nfa1, nfa2 )
        import p._

        //  nfa1.enabled does not intersect nfa2.enabled
        enabledIn( getInit ).toList shouldBe List( "a" )

        //  advance nfa1 to state 1, all enabled from q should be there
        enabledIn( succ( getInit, "a" ) ).toSet shouldBe Set( "a", "c" )

    }

    test( "Check order of enabled labels in A -- B, enabled in A is s1, enabled in B is s2, s1 in s2  " ) {

        //  first one
        val nfa1 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ),
                ( 0 ~> 1 )( "b" ),
                ( 1 ~> 2 )( "a" ),
                ( 1 ~> 2 )( "c" ) ),
            Set(),
            Set() )

        //  second one
        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "b" ),
                ( 1 ~> 2 )( "a" ),
                ( 1 ~> 2 )( "c" ),
                ( 1 ~> 2 )( "d" ) ),
            Set( 2 ) )

        val p = DiffWithPriority( nfa1, nfa2 )
        import p._

        //  nfa1.enabled does not intersect nfa2.enabled
        enabledIn( getInit ).toList shouldBe List( "b", "a" )

        //  advance nfa1 to state 1, all enabled from q should be there
        enabledIn( succ( getInit, "a" ) ).toList shouldBe List( "c", "a" )

    }

    test( "Check order of enabled labels in A -- B, enabled in A is s1, enabled in B is s2, s2 in s1  " ) {

        //  first one
        val nfa1 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ),
                ( 0 ~> 1 )( "b" ),
                ( 1 ~> 2 )( "a" ),
                ( 1 ~> 2 )( "c" ),
                ( 1 ~> 2 )( "b" ) ),
            Set(),
            Set() )

        //  second one
        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "b" ),
                ( 1 ~> 2 )( "a" ),
                ( 1 ~> 2 )( "c" ) ),
            Set( 2 ) )

        val p = DiffWithPriority( nfa1, nfa2 )
        import p._

        //  nfa1.enabled does not intersect nfa2.enabled
        enabledIn( getInit ).toList shouldBe List( "b", "a" )

        //  advance nfa1 to state 1, all enabled from q should be there
        enabledIn( succ( getInit, "a" ) ).toList shouldBe List( "c", "b", "a" )

    }

}
