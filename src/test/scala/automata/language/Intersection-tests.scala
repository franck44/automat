/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat

package lang

import util.Traversal.DFS
import edge.LabDiEdge
import edge.Implicits._
import auto.NFA
import scala.language.existentials

import org.scalatest._

class InterLangTests extends FunSuite with Matchers {

    test( "Intersection of two languages, one empty" ) {
        //  one automaton, empty
        val nfa1 = NFA[ Int, String ]( Set(), Set(), Set() )
        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set( ( 1 ~> 2 )( "a" ), ( 0 ~> 1 )( "b" ) ),
            Set( 2 ) )

        val l = Lang( nfa1 ) /\ Lang( nfa2 )

        assert( l.isEmpty )

    }

    test( "Intersection of two languages, both non empty" ) {
        //  one automaton, empty
        val nfa1 = NFA[ Int, String ](
            Set( 0 ),
            Set( ( 0 ~> 1 )( "a" ), ( 1 ~> 0 )( "b" ), ( 1 ~> 1 )( "a" ) ),
            Set( 1 ) )

        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set( ( 1 ~> 2 )( "b" ), ( 0 ~> 1 )( "a" ), ( 1 ~> 2 )( "a" ) ),
            Set( 2 ) )

        val l = Lang( nfa1 ) /\ Lang( nfa2 )

        assert( !l.isEmpty )

        val e = l.getAcceptedTrace

        assert( l accepts List( "a", "a" ) )
        assert( !( l accepts List( "a", "b" ) ) )
    }

    test( "Intersection of two automata - empty string not accepted" ) {
        //  one automaton, accepts empty string
        val nfa1 = NFA[ Int, String ]( Set( 0 ), Set(), Set( 0 ) )
        //  another one does not accept empty string
        val nfa2 = NFA[ Int, String ]( Set( 0 ), Set(), Set() )

        val l = Lang( nfa1 ) /\ Lang( nfa2 )

        assert( l.isEmpty )

        assert( Lang( nfa1 ) accepts List() )
        assert( !( Lang( nfa2 ) accepts List() ) )
        assert( !( l accepts List() ) )
    }

}
