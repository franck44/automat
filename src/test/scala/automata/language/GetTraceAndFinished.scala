/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat

package lang

import util.Traversal.DFS
import edge.LabDiEdge
import edge.Implicits._
import auto.NFA
import scala.language.existentials
import util.Determiniser.toDetNFA

import org.scalatest._

class GetTraceAndFinishedTests extends FunSuite with Matchers {

    test( "One automaton has sinkAccept, other has sinkReject, take the product and getAcceptedTrace" ) {

        val nfa1 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ),
                ( 1 ~> 2 )( "c" ),
                ( 2 ~> 2 )( "c" ),
                ( 0 ~> 3 )( "b" ),
                ( 3 ~> 4 )( "d" ) ),
            Set ( 2 ) )

        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ),
                ( 1 ~> 2 )( "c" ),
                ( 2 ~> 2 )( "c" ),
                ( 0 ~> 3 )( "b" )
            // ( 3 ~> 4 )( "d" )
            ),
            Set ( 2 ) )

        val e = Lang( nfa1 ).getAcceptedTraceWithFinished
        e shouldEqual ( (
            Some( List( "a", "c" ) ),
            Set( Set( 3 ), Set( 4 ) ) ) )

        val nfa3 = nfa2.copy( sinkReject = e._2.flatten )
        val e2 = Lang( nfa3 ).getAcceptedTraceWithFinished
        e2 shouldEqual ( (
            Some( List( "a", "c" ) ),
            Set( Set( 3 ) ) ) )

    }

    ignore( "One automaton has sinkAccept, other has sinkReject, take the difference and getAcceptedTrace" ) {

        val nfa1 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ),
                ( 0 ~> 2 )( "a" ),
                ( 2 ~> 2 )( "a" ) ),
            Set ( 1 ),
            Set( 1 ) )

        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ) ),
            Set (),
            Set(),
            Set( 1 ) )

        val e = ( Lang( nfa1 ) \ Lang( nfa2 ) ).getAcceptedTrace
        e should not be empty

    }

    ignore( "One automaton has sinkAccept, other non-deterministic has sinkAccep, take the difference and getAcceptedTrace" ) {

        val nfa1 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ),
                ( 1 ~> 2 )( "a" ),
                ( 2 ~> 2 )( "b" ) ),
            Set ( 1, 2 ) )

        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ),
                ( 1 ~> 2 )( "a" ),
                ( 1 ~> 3 )( "a" ),
                ( 3 ~> 3 )( "b" ) ),
            Set ( 2 ),
            Set( 2 ),
            Set() )

        val l = ( Lang( nfa1 ) \ Lang( toDetNFA( nfa2 )._1 ) )

        //  format: OFF
        l.isEmpty                            shouldBe false
        l accepts List()                     shouldBe false
        l accepts List( "a" )                shouldBe true
        l accepts List( "a", "a" )           shouldBe false

        l accepts List( "a", "a", "b" )      shouldBe false
        l accepts List( "a", "b", "a", "c" ) shouldBe false
        //  format: ON
    }

}
