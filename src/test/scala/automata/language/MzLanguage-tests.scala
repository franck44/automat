/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat
package lang
package tests

import org.scalatest._

import auto.{ NFA, DetAuto, FreeProduct }
import edge.LabDiEdge
import edge.Implicits._
import util.Traversal.DFS
import dpor.Traversal.DFSDPOR
import dpor.MzEquiv
import util.Determiniser.toDetNFA
import util.Determiniser.DeterminiserVisitor
import util.DotConverter._
import scala.util.{ Try, Success, Failure }
import org.bitbucket.franck44.dot.DOTSyntax.{ DotSpec, Attribute, Ident }
import org.bitbucket.franck44.dot.DOTPrettyPrinter.show

object DPORHelper {
    import dpor.tests.SimpleTrace._

    /** Round percentage */
    def myRound( x : Float ) : Float = ( x * 1000 ).toFloat.round.toFloat / 10

    def showDFSvsDFSDPORToDot( a : NFA[ Int, ( Int, Action ) ], v : DeterminiserVisitor[ Set[ Int ], ( Int, Action ), String ], title : String ) : DotSpec =
        toDot(
            a.copy( name = s"$title ${v.edges.size}/${a.edges.size}, ratio = ${myRound( v.edges.size.toFloat / a.edges.size )}%" ),
            nodeProp = { s : Int ⇒
                if ( v.index.isDefinedAt( Set( s ) ) )
                    List(
                        Attribute( "style", Ident( "filled" ) ),
                        Attribute( "fillcolor", Ident( "orange" ) ),
                        Attribute( "color", Ident( "orange" ) ),
                        Attribute( "fontcolor", Ident( "black" ) ) )
                else List(
                    Attribute( "style", Ident( "dashed" ) ),
                    Attribute( "fillcolor", Ident( "grey" ) ),
                    Attribute( "color", Ident( "grey" ) ),
                    Attribute ( "fontcolor", Ident( "grey" ) ) )
            },
            edgeProp = { e : LabDiEdge[ Int, ( Int, Action ) ] ⇒
                val exploredEdgesStyle = List(
                    Attribute( "color", Ident( "orange" ) ),
                    Attribute( "fontcolor", Ident( "orange" ) ) )
                val unexploredEdgesStyle = List(
                    Attribute( "style", Ident( "dashed" ) ),
                    Attribute( "color", Ident( "grey" ) ),
                    Attribute( "fontcolor", Ident( "grey" ) ) )

                if ( v.index.isDefinedAt( Set( e.src ) ) && v.index.isDefinedAt( Set( e.tgt ) ) )
                    if ( v.edges.contains( ( v.index( Set( e.src ) ) ~> v.index( Set( e.tgt ) ) )( e.lab ) ) )
                        exploredEdgesStyle
                    else
                        unexploredEdgesStyle
                else
                    unexploredEdgesStyle

            } )
}
/**
 * Tests for Dynamic partial order reduction (from POPL'14)
 */
class MzLangTests extends FunSuite with Matchers {
    import dpor.tests.SimpleTrace._
    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    //  logger
    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    test( "Example 1 [Abdulla et al., 2014]" ) {

        logger.debug( "Example 1  [Abdulla et al., 2014]" )
        val p = NFA[ Int, Action ](
            init = Set( 0 ),
            transitions = Set(
                ( 0 ~> 1 )( Write( "x" ) ) ),
            accepting = Set ( 1 ),
            name = "p" )

        val q = NFA[ Int, Action ](
            init = Set( 0 ),
            transitions = Set(
                ( 0 ~> 1 )( Read( "y" ) ),
                ( 1 ~> 2 )( Read( "x" ) ) ),
            accepting = Set ( 2 ),
            name = "q" )

        val r = NFA[ Int, Action ](
            init = Set( 0 ),
            transitions = Set(
                ( 0 ~> 1 )( Read( "z" ) ),
                ( 1 ~> 2 )( Read( "x" ) ) ),
            accepting = Set ( 2 ),
            name = "r" )

        val prod = new FreeProduct( Seq( p, q, r ) )

        //  detProd is an NFA[Int, (Int,Action)]
        val ( detProd, info ) = toDetNFA( prod )
        logger.debug( "Initial automaton" )
        logger.debug( show( toDot( detProd ) ) )

        val dfsResult = MzLang( detProd, MzEquiv( dep ), proc ).dfs()
        val dfsdporResult = MzLang( detProd, MzEquiv( dep ), proc ).dfsdpor()

        //  Expected search tree for DFSDPOR
        val expectedResult = MzLang(
            NFA[ Int, ( Int, Action ) ](
                init = Set( 0 ),
                transitions = Set(
                    ( 7 ~> 8 )( ( 1, Read( "x" ) ) ),
                    ( 3 ~> 4 )( ( 1, Read( "x" ) ) ),
                    ( 2 ~> 3 )( ( 1, Read( "y" ) ) ),
                    ( 6 ~> 5 )( ( 1, Read( "x" ) ) ),
                    ( 3 ~> 6 )( ( 0, Write( "x" ) ) ),
                    ( 4 ~> 5 )( ( 0, Write( "x" ) ) ),
                    ( 7 ~> 10 )( ( 0, Write( "x" ) ) ),
                    ( 9 ~> 5 )( ( 2, Read( "x" ) ) ),
                    ( 10 ~> 6 )( ( 2, Read( "x" ) ) ),
                    ( 1 ~> 7 )( ( 1, Read( "y" ) ) ),
                    ( 0 ~> 1 )( ( 2, Read( "z" ) ) ),
                    ( 1 ~> 2 )( ( 2, Read( "x" ) ) ),
                    ( 8 ~> 9 )( ( 0, Write( "x" ) ) ) ),
                accepting = Set(),
                name = "DFSDPOR Example 1" ),
            MzEquiv( dep ), proc )

        logger.debug( "MzLang/DFS tree for Example 1  [Abdulla et al., 2014]" )
        logger.debug( show( toDot( dfsResult ) ) )
        logger.debug( "MzLang/DFSDPOR tree for Example 1  [Abdulla et al., 2014]" )
        logger.debug( show( toDot( dfsdporResult ) ) )

        ( expectedResult \ MzLang( dfsdporResult, MzEquiv( dep ), proc ) ).isEmpty shouldBe true

        // logger.debug( "DFS/DFSDPOR tree for Example 1  [Abdulla et al., 2014]" )
        // logger.debug(
        //     show ( DPORHelper.showDFSvsDFSDPORToDot( detProd, v, "Example 1  [Abdulla et al., 2014]" ) )
        // )
    }
}
//     test( "Example 2 [Abdulla et al., 2014]" ) {
//
//         logger.debug( "Example 2  [Abdulla et al., 2014]" )
//         val p = NFA[ Int, Action ](
//             init = Set( 0 ),
//             transitions = Set(
//                 ( 0 ~> 1 )( Write( "x" ) )
//             ),
//             accepting = Set ( 1 ),
//             name = "p"
//         )
//
//         val q = NFA[ Int, Action ](
//             init = Set( 0 ),
//             transitions = Set(
//                 ( 0 ~> 1 )( Write( "y" ) )
//             ),
//             accepting = Set ( 1 ),
//             name = "q"
//         )
//
//         val r = NFA[ Int, Action ](
//             init = Set( 0 ),
//             transitions = Set(
//                 ( 0 ~> 1 )( Read( "y" ) ),
//                 ( 1 ~> 2 )( Write( "m" ) ),
//                 ( 2 ~> 3 )( Read( "m" ) ),
//                 ( 3 ~> 4 )( Write( "z" ) )
//             ),
//             accepting = Set ( 4 ),
//             name = "r"
//         )
//         val s = NFA[ Int, Action ](
//             init = Set( 0 ),
//             transitions = Set(
//                 ( 0 ~> 1 )( Read( "z" ) ),
//                 ( 1 ~> 2 )( Write( "n" ) ),
//                 ( 2 ~> 3 )( Read( "n" ) ),
//                 ( 3 ~> 4 )( Write( "l" ) )
//             ),
//             accepting = Set ( 4 ),
//             name = "r"
//         )
//
//         val prod = new FreeProduct( Seq( p, q, r, s ) )
//
//         //  detProd is an NFA[Int, (Int,Action)]
//         val ( detProd, info ) = toDetNFA( prod )
//         logger.debug( "Initial automaton" )
//         logger.debug( show( toDot( detProd ) ) )
//
//         //  Compute the determinised version with a visitor and collect states and edges
//         val DFSdeterminiser = DFSDPOR(
//             DeterminiserVisitor[ Set[ Int ], ( Int, Action ) ](
//                 Map(),
//                 List(),
//                 { _ ⇒ "" },
//                 Map()
//             ),
//             detProd.outGoingEdges,
//             proc,
//             MzEquiv( dep )
//         )
//         val v = DFSdeterminiser( detProd.getInit )
//
//         // create an NFA using the visitor collected states and edges in the DFSDPOR
//         val detReduced =
//             new NFA(
//                 init = Set( v.index( detProd.getInit ) ),
//                 transitions = v.edges.toSet,
//                 accepting = Set(),
//                 name = "DFSDPOR Example 2"
//             )
//
//         //  Expected search tree for DFSDPOR
//         val expectedResult = NFA[ Int, ( Int, Action ) ](
//             init = Set( 0 ),
//             transitions = Set(
//                 ( 12 ~> 6 )( ( 2, Write( "z" ) ) ),
//                 ( 1 ~> 2 )( ( 2, Write( "m" ) ) ),
//                 ( 15 ~> 11 )( ( 2, Read( "m" ) ) ),
//                 ( 5 ~> 6 )( ( 3, Read( "z" ) ) ),
//                 ( 3 ~> 11 )( ( 1, Write( "y" ) ) ),
//                 ( 2 ~> 3 )( ( 2, Read( "m" ) ) ),
//                 ( 4 ~> 5 )( ( 1, Write( "y" ) ) ),
//                 ( 8 ~> 9 )( ( 3, Write( "l" ) ) ),
//                 ( 0 ~> 1 )( ( 2, Read( "y" ) ) ),
//                 ( 14 ~> 15 )( ( 2, Write( "m" ) ) ),
//                 ( 0 ~> 13 )( ( 1, Write( "y" ) ) ),
//                 ( 3 ~> 4 )( ( 2, Write( "z" ) ) ),
//                 ( 7 ~> 8 )( ( 3, Read( "n" ) ) ),
//                 ( 9 ~> 10 )( ( 0, Write( "x" ) ) ),
//                 ( 11 ~> 12 )( ( 3, Read( "z" ) ) ),
//                 ( 13 ~> 14 )( ( 2, Read( "y" ) ) ),
//                 ( 6 ~> 7 )( ( 3, Write( "n" ) ) )
//             ),
//             accepting = Set(),
//             name = "DFSDPOR Example 2"
//         )
//         logger.debug( "DFSDPOR tree for Example 2  [Abdulla et al., 2014]" )
//         logger.debug( show( toDot( detReduced ) ) )
//         detReduced shouldBe expectedResult
//
//         logger.debug( "DFS/DFSDPOR tree for Example 2  [Abdulla et al., 2014]" )
//         logger.debug(
//             show ( DPORHelper.showDFSvsDFSDPORToDot( detProd, v, "Example 2  [Abdulla et al., 2014]" ) )
//         )
//     }
// }
//
// /**
//  * Simple Tests for Dynamic partial order reduction
//  */
// class DPORSimpleTests extends FunSuite with Matchers {
//
//     // test( "LLVM Example with more intermediate steps" ) {
//     //
//     //     val t1 = NFA[ Int, String ](
//     //         Set( 0 ),
//     //         Set(
//     //             ( 0 ~> 1 )( "a = 5" ),
//     //             ( 1 ~> 2 )( "skip" ),
//     //             ( 2 ~> 3 )( "while (0)" ),
//     //             ( 3 ~> 2 )( "if 0" ),
//     //             ( 3 ~> 4 )( "if !0" )
//     //         ),
//     //         Set ()
//     //     )
//     //
//     //     val t2 = NFA[ Int, String ](
//     //         Set( 0 ),
//     //         Set(
//     //             ( 0 ~> 1 )( "a > 5" ),
//     //             ( 1 ~> 2 )( "skip" ),
//     //             ( 2 ~> 3 )( "skip" ),
//     //             ( 3 ~> 4 )( "!(a > 5)" )
//     //         ),
//     //         Set ( 4 )
//     //     )
//     //
//     //     val prod = new FreeProduct( Seq( t1, t2 ) )
//     //     val detProd = toDetNFA( prod )._1
//     //
//     //     File( tmpPath ( "NFA-LLVM2.dot" ) ).writeAll( show( toDot( detProd, graphProp = namer( "NFA-LLVM (full)" ) ) ) )
//     //
//     //     val dpor = DPOR(
//     //         prod,
//     //         { x : ( Int, String ) ⇒ x._1 },
//     //         { x : Seq[ ( Int, String ) ] ⇒
//     //             {
//     //                 ( e1 : Int, e2 : Int ) ⇒
//     //                     {
//     //                         val s = x( e1 )._2.contains( "a" ) && x( e2 )._2.contains( "a" )
//     //                         !s
//     //                     }
//     //             }
//     //         }
//     //     )
//     //
//     //     dpor.getTrace()
//     //     val resultGraph = dpor.getExploredGraph
//     //     val detReduced = toDetNFA( resultGraph )._1
//     //
//     //     import org.bitbucket.franck44.dot.DOTSyntax.{ Attribute, StringLit, Ident }
//     //     File( tmpPath ( "reducedNFA-LLVM2.dot" ) ).writeAll( show( toDot( detReduced, graphProp = namer( "Reduced NFA-LLVM (full)" ) ) ) )
//     // }
//
//     import SimpleTrace._
//     import com.typesafe.scalalogging.Logger
//     import org.slf4j.LoggerFactory
//
//     //  logger
//     private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )
//
//     test( "Simple DPOR ex1: One process with a loop" ) {
//
//         logger.debug( "Simple DPOR ex1: One process with a loop" )
//         val p = NFA[ Int, Action ](
//             init = Set( 0 ),
//             transitions = Set(
//                 ( 0 ~> 1 )( Write( "x" ) ),
//                 ( 0 ~> 0 )( Read( "x" ) ),
//                 ( 1 ~> 2 )( Read( "y" ) )
//             ),
//             accepting = Set ( 2 ),
//             name = "p"
//         )
//
//         val prod = new FreeProduct( Seq( p ) )
//
//         //  detProd is an NFA[Int, (Int,Action)]
//         val ( detProd, _ ) = toDetNFA( prod )
//         logger.debug( "Initial automaton" )
//         logger.debug( show( toDot( detProd ) ) )
//
//         //  Compute the determinised version with a visitor and collect states and edges
//         val DFSdeterminiser = DFSDPOR(
//             DeterminiserVisitor[ Set[ Int ], ( Int, Action ) ](
//                 Map(),
//                 List(),
//                 { _ ⇒ "" },
//                 Map()
//             ),
//             detProd.outGoingEdges,
//             proc,
//             MzEquiv( dep )
//         )
//         val v = DFSdeterminiser( detProd.getInit )
//
//         // create an NFA using the visitor collected states and edges in the DFSDPOR
//         val detReduced =
//             new NFA(
//                 init = Set( v.index( detProd.getInit ) ),
//                 transitions = v.edges.toSet,
//                 accepting = Set(),
//                 name = "DFSDPOR Simple DPOR Ex 1"
//             )
//
//         //  Expected search tree for DFSDPOR
//         val expectedResult = NFA[ Int, ( Int, Action ) ](
//             init = Set( 0 ),
//             transitions = Set(
//                 ( 1 ~> 2 ) ( ( 0, Read( "y" ) ) ),
//                 ( 0 ~> 1 ) ( ( 0, Write( "x" ) ) ),
//                 ( 0 ~> 0 ) ( ( 0, Read( "x" ) ) )
//             ),
//             accepting = Set(),
//             name = "DFSDPOR Simple DPOR Ex 1"
//         )
//         logger.debug( "DFSDPOR tree for Simple DPOR Ex 1" )
//         logger.debug( show( toDot( detReduced ) ) )
//         detReduced shouldBe expectedResult
//
//         logger.debug( "DFSDPOR/DFS for Simple DPOR Ex 1" )
//         logger.debug(
//             show ( DPORHelper.showDFSvsDFSDPORToDot( detProd, v, "Simple DPOR Ex 1" ) )
//         )
//
//     }
//
//     test( "Simple DPOR ex2: Two one-action processes with independent actions" ) {
//
//         logger.debug( "Simple DPOR ex2: Two one-action processes with independent actions" )
//
//         val p = NFA[ Int, Action ](
//             init = Set( 0 ),
//             transitions = Set(
//                 ( 0 ~> 1 )( Read( "x" ) )
//             ),
//             accepting = Set ( 1 ),
//             name = "p"
//         )
//
//         val q = NFA[ Int, Action ](
//             init = Set( 0 ),
//             transitions = Set(
//                 ( 0 ~> 1 )( Read( "y" ) )
//             ),
//             accepting = Set ( 1 ),
//             name = "q"
//         )
//
//         val prod = new FreeProduct( Seq( p, q ) )
//
//         //  detProd is an NFA[Int, (Int,Action)]
//         val ( detProd, _ ) = toDetNFA( prod )
//         logger.debug( "Initial automaton" )
//         logger.debug( show( toDot( detProd ) ) )
//
//         //  Compute the determinised version with a visitor and collect states and edges
//         val DFSdeterminiser = DFSDPOR(
//             DeterminiserVisitor[ Set[ Int ], ( Int, Action ) ](
//                 Map(),
//                 List(),
//                 { _ ⇒ "" },
//                 Map()
//             ),
//             detProd.outGoingEdges,
//             proc,
//             MzEquiv( dep )
//         )
//         val v = DFSdeterminiser( detProd.getInit )
//
//         // create an NFA using the visitor collected states and edges in the DFSDPOR
//         val detReduced =
//             new NFA(
//                 init = Set( v.index( detProd.getInit ) ),
//                 transitions = v.edges.toSet,
//                 accepting = Set(),
//                 name = "DFSDPOR Simple DPOR Ex 2"
//             )
//
//         //  Expected search tree for DFSDPOR
//         val expectedResult = NFA[ Int, ( Int, Action ) ](
//             init = Set( 0 ),
//             transitions = Set(
//                 ( 1 ~> 2 )( ( 0, Read( "x" ) ) ),
//                 ( 0 ~> 1 )( ( 1, Read( "y" ) ) )
//             ),
//             accepting = Set(),
//             name = "DFSDPOR Simple DPOR Ex 2"
//         )
//         logger.debug( "DFSDPOR tree for Simple DPOR Ex 2" )
//         logger.debug( show( toDot( detReduced ) ) )
//         detReduced shouldBe expectedResult
//
//         logger.debug( "DFSDPOR/DFS for Simple DPOR Ex 2" )
//         logger.debug(
//             show ( DPORHelper.showDFSvsDFSDPORToDot( detProd, v, "Simple DPOR Ex 2" ) )
//         )
//     }
//
//     test( "Simple DPOR ex3: Two processes dependent actions" ) {
//
//         logger.debug( "Simple DPOR ex3: Two processes dependent actions" )
//
//         val p = NFA[ Int, Action ](
//             init = Set( 0 ),
//             transitions = Set(
//                 ( 0 ~> 1 )( Write( "x" ) ),
//                 ( 1 ~> 2 )( Read( "y" ) )
//             ),
//             accepting = Set ( 2 ),
//             name = "p"
//         )
//
//         val q = NFA[ Int, Action ](
//             init = Set( 0 ),
//             transitions = Set(
//                 ( 0 ~> 1 )( Read( "x" ) )
//             ),
//             accepting = Set ( 1 ),
//             name = "q"
//         )
//
//         val prod = new FreeProduct( Seq( p, q ) )
//
//         //  detProd is an NFA[Int, (Int,Action)]
//         val ( detProd, _ ) = toDetNFA( prod )
//         logger.debug( "Initial automaton" )
//         logger.debug( show( toDot( detProd ) ) )
//
//         //  Compute the determinised version with a visitor and collect states and edges
//         val DFSdeterminiser = DFSDPOR(
//             DeterminiserVisitor[ Set[ Int ], ( Int, Action ) ](
//                 Map(),
//                 List(),
//                 { _ ⇒ "" },
//                 Map()
//             ),
//             detProd.outGoingEdges,
//             proc,
//             MzEquiv( dep )
//         )
//         val v = DFSdeterminiser( detProd.getInit )
//
//         // create an NFA using the visitor collected states and edges in the DFSDPOR
//         val detReduced =
//             new NFA(
//                 init = Set( v.index( detProd.getInit ) ),
//                 transitions = v.edges.toSet,
//                 accepting = Set(),
//                 name = "DFSDPOR Simple DPOR Ex 3"
//             )
//
//         //  Expected search tree for DFSDPOR
//         val expectedResult = NFA[ Int, ( Int, Action ) ](
//             init = Set( 0 ),
//             transitions = Set(
//                 ( 0 ~> 4 )( ( 0, Write( "x" ) ) ),
//                 ( 4 ~> 2 )( ( 1, Read( "x" ) ) ),
//                 ( 1 ~> 2 )( ( 0, Write( "x" ) ) ),
//                 ( 0 ~> 1 )( ( 1, Read( "x" ) ) ),
//                 ( 2 ~> 3 )( ( 0, Read( "y" ) ) )
//             ),
//             accepting = Set(),
//             name = "DFSDPOR Simple DPOR Ex 3"
//         )
//         logger.debug( "DFSDPOR tree for Simple DPOR Ex 3" )
//         logger.debug( show( toDot( detReduced ) ) )
//         detReduced shouldBe expectedResult
//
//         logger.debug( "DFSDPOR/DFS for Simple DPOR Ex 3" )
//         logger.debug(
//             show ( DPORHelper.showDFSvsDFSDPORToDot( detProd, v, "Simple DPOR Ex 3" ) )
//         )
//     }
//
//     test( "Simple DPOR ex4: Three processes with dependent actions" ) {
//
//         logger.debug( "Simple DPOR ex4: Three processes with dependent actions" )
//
//         val p = NFA[ Int, Action ](
//             init = Set( 0 ),
//             transitions = Set(
//                 ( 0 ~> 1 )( Write( "x" ) ),
//                 ( 0 ~> 2 )( Read( "y" ) )
//             ),
//             accepting = Set ( 1, 2 ),
//             name = "p"
//         )
//
//         val q = NFA[ Int, Action ](
//             init = Set( 0 ),
//             transitions = Set(
//                 ( 0 ~> 1 )( Read( "y" ) ),
//                 ( 1 ~> 2 )( Read( "x" ) )
//             ),
//             accepting = Set ( 2 ),
//             name = "q"
//         )
//
//         val r = NFA[ Int, Action ](
//             init = Set( 0 ),
//             transitions = Set(
//                 ( 0 ~> 1 )( Read( "z" ) ),
//                 ( 1 ~> 2 )( Read( "x" ) ),
//                 ( 0 ~> 3 )( Read( "w" ) ),
//                 ( 3 ~> 4 )( Read( "x" ) )
//             ),
//             accepting = Set ( 2, 4 ),
//             name = "r"
//         )
//
//         val prod = new FreeProduct( Seq( p, q, r ) )
//
//         val ( detProd, info ) = toDetNFA( prod )
//         logger.debug( "Initial automaton" )
//         logger.debug( show( toDot( detProd ) ) )
//
//         //  compute the determinised version with a visitor and collect states and edges
//         val DFSdeterminiser = DFSDPOR(
//             DeterminiserVisitor[ Set[ Int ], ( Int, Action ) ](
//                 Map(),
//                 List(),
//                 { _ ⇒ "" },
//                 Map()
//             ),
//             detProd.outGoingEdges,
//             proc,
//             MzEquiv( dep )
//         )
//         val v = DFSdeterminiser( detProd.getInit )
//
//         // create an NFA using the visitor collected states and edges in the DFSDPOR
//         val detReduced =
//             new NFA(
//                 init = Set( v.index( detProd.getInit ) ),
//                 transitions = v.edges.toSet,
//                 accepting = Set(),
//                 name = "DFSDPOR Simple DPOR Ex 4"
//             )
//
//         //  Expected search tree for DFSDPOR
//         val expectedResult = NFA[ Int, ( Int, Action ) ](
//             Set( 0 ),
//             Set(
//                 ( 3 ~> 4 )( ( 1, Read( "x" ) ) ),
//                 ( 0 ~> 13 )( ( 2, Read( "w" ) ) ),
//                 ( 2 ~> 3 )( ( 1, Read( "y" ) ) ),
//                 ( 4 ~> 6 )( ( 0, Write( "x" ) ) ),
//                 ( 16 ~> 17 )( ( 0, Read( "y" ) ) ),
//                 ( 7 ~> 6 )( ( 1, Read( "x" ) ) ),
//                 ( 1 ~> 8 )( ( 1, Read( "y" ) ) ),
//                 ( 9 ~> 10 )( ( 0, Read( "y" ) ) ),
//                 ( 4 ~> 5 )( ( 0, Read( "y" ) ) ),
//                 ( 11 ~> 6 )( ( 2, Read( "x" ) ) ),
//                 ( 24 ~> 19 )( ( 2, Read( "x" ) ) ),
//                 ( 21 ~> 22 )( ( 0, Read( "y" ) ) ),
//                 ( 14 ~> 15 )( ( 1, Read( "y" ) ) ),
//                 ( 20 ~> 21 )( ( 1, Read( "x" ) ) ),
//                 ( 13 ~> 20 )( ( 1, Read( "y" ) ) ),
//                 ( 12 ~> 7 )( ( 2, Read( "x" ) ) ),
//                 ( 15 ~> 16 )( ( 1, Read( "x" ) ) ),
//                 ( 13 ~> 14 )( ( 2, Read( "x" ) ) ),
//                 ( 16 ~> 18 )( ( 0, Write( "x" ) ) ),
//                 ( 19 ~> 18 )( ( 1, Read( "x" ) ) ),
//                 ( 3 ~> 7 )( ( 0, Write( "x" ) ) ),
//                 ( 20 ~> 24 )( ( 0, Write( "x" ) ) ),
//                 ( 0 ~> 1 )( ( 2, Read( "z" ) ) ),
//                 ( 15 ~> 19 )( ( 0, Write( "x" ) ) ),
//                 ( 8 ~> 12 )( ( 0, Write( "x" ) ) ),
//                 ( 23 ~> 18 )( ( 2, Read( "x" ) ) ),
//                 ( 1 ~> 2 )( ( 2, Read( "x" ) ) ),
//                 ( 21 ~> 23 )( ( 0, Write( "x" ) ) ),
//                 ( 9 ~> 11 )( ( 0, Write( "x" ) ) ),
//                 ( 8 ~> 9 )( ( 1, Read( "x" ) ) )
//             ),
//             accepting = Set(),
//             name = "DFSDPOR Simple DPOR Ex 4"
//         )
//         logger.debug( "DFSDPOR tree for Simple DPOR Ex 4" )
//         logger.debug( show( toDot( detReduced ) ) )
//         detReduced shouldBe expectedResult
//
//         logger.debug( "DFSDPOR/DFS for Simple DPOR Ex 4" )
//         logger.debug(
//             show ( DPORHelper.showDFSvsDFSDPORToDot( detProd, v, "Simple DPOR Ex 4" ) )
//         )
//     }
// }
