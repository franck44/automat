/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat

package lang

import util.Traversal.DFS
import edge.LabDiEdge
import edge.Implicits._
import auto.NFA
import scala.language.existentials
import util.Determiniser.toDetNFA

import org.scalatest._

class LangAcceptsAllTests extends FunSuite with Matchers {

    test( "One automaton single accepting/sinkAccept state - should accept everything" ) {
        //  automaton 1, one state acceptsAll
        val nfa1 = NFA[ Int, String ]( Set( 0 ), Set(), Set( 0 ), Set( 0 ) )

        //  should accept any string
        //  format: OFF
        Lang( nfa1 ).isEmpty                            shouldBe false
        Lang( nfa1 ) accepts List()                     shouldBe true
        Lang( nfa1 ) accepts List( "a" )                shouldBe true
        Lang( nfa1 ) accepts List( "a", "b" )           shouldBe true
        Lang( nfa1 ) accepts List( "a", "b", "a" )      shouldBe true
        Lang( nfa1 ) accepts List( "a", "b", "a", "c" ) shouldBe true
        //  format: ON
    }

    test( "One automaton single accepting/sinkAccept state - difference with another - should not accepts anything" ) {
        //  automaton 1, one state acceptsAll
        val nfa1 = NFA[ Int, String ]( Set( 0 ), Set(), Set( 0 ), Set( 0 ) )

        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set( ( 1 ~> 2 )( "a" ), ( 0 ~> 1 )( "b" ) ),
            Set( 2 ) )

        val l = Lang( nfa2 ) \ Lang( nfa1 )

        //  should accept any string
        //  format: OFF
        l.isEmpty shouldBe true
        l accepts List()                     shouldBe false
        l accepts List( "a" )                shouldBe false
        l accepts List( "a", "b" )           shouldBe false
        l accepts List( "a", "b", "a" )      shouldBe false
        l accepts List( "a", "b", "a", "c" ) shouldBe false
        //  format: ON
    }

    test( "One automaton single accepting/sinkAccept state - intersection with another with a single sinkReject - should not accepts anything" ) {
        //  automaton 1, one state sinkReject
        val nfa1 = NFA[ Int, String ]( Set( 0 ), Set(), Set(), Set(), Set( 0 ) )

        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set( ( 1 ~> 2 )( "a" ), ( 0 ~> 1 )( "b" ) ),
            Set( 2 ) )

        val l = Lang( nfa2 ) /\ Lang( nfa1 )

        //  should accept any string
        //  format: OFF
        l.isEmpty shouldBe true
        l accepts List()                     shouldBe false
        l accepts List( "a" )                shouldBe false
        l accepts List( "a", "b" )           shouldBe false
        l accepts List( "a", "b", "a" )      shouldBe false
        l accepts List( "a", "b", "a", "c" ) shouldBe false
        //  format: ON
    }

    test( "Automaton has sinkAccept, determinise" ) {
        //  automaton 1, one state sinkReject
        import org.bitbucket.franck44.dot.DOTSyntax.DotSpec
        import util.DotConverter._
        import org.bitbucket.franck44.dot.DOTPrettyPrinter.{ show }

        val nfa1 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ),
                ( 0 ~> 2 )( "a" ),
                ( 2 ~> 2 )( "a" ) ),
            Set ( 1 ),
            Set( 1 ) )

        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ) ),
            Set ( 1 ),
            Set( 1 ) )

        val nfa1det = toDetNFA( nfa1 )._1

        nfa1det shouldEqual nfa2
    }

    test( "One automaton has sinkAccept, other has sinkReject, take the product and getAcceptedTrace" ) {

        val nfa1 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ),
                ( 0 ~> 2 )( "a" ),
                ( 2 ~> 2 )( "a" ) ),
            Set ( 1 ),
            Set( 1 ) )

        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ) ),
            Set (),
            Set(),
            Set( 1 ) )

        val e = ( Lang( nfa1 ) /\ Lang( nfa2 ) ).getAcceptedTrace
        e shouldEqual None

    }

    test( "One automaton has sinkAccept, other has sinkReject, take the difference and getAcceptedTrace" ) {

        val nfa1 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ),
                ( 0 ~> 2 )( "a" ),
                ( 2 ~> 2 )( "a" ) ),
            Set ( 1 ),
            Set( 1 ) )

        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ) ),
            Set (),
            Set(),
            Set( 1 ) )

        val e = ( Lang( nfa1 ) \ Lang( nfa2 ) ).getAcceptedTrace
        e should not be empty

    }

    test( "One automaton has sinkAccept, other non-deterministic has sinkAccep, take the difference and getAcceptedTrace" ) {

        val nfa1 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ),
                ( 1 ~> 2 )( "a" ),
                ( 2 ~> 2 )( "b" ) ),
            Set ( 1, 2 ) )

        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ),
                ( 1 ~> 2 )( "a" ),
                ( 1 ~> 3 )( "a" ),
                ( 3 ~> 3 )( "b" ) ),
            Set ( 2 ),
            Set( 2 ),
            Set() )

        val l = ( Lang( nfa1 ) \ Lang( toDetNFA( nfa2 )._1 ) )

        //  format: OFF
        l.isEmpty                            shouldBe false
        l accepts List()                     shouldBe false
        l accepts List( "a" )                shouldBe true
        l accepts List( "a", "a" )           shouldBe false

        l accepts List( "a", "a", "b" )      shouldBe false
        l accepts List( "a", "b", "a", "c" ) shouldBe false
        //  format: ON
    }

}
