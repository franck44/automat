/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.dot

/**
 * Main object that contains the function to run first
 */

// import org.bitbucket.inkytonik.kiama.util.{ CompilerWithConfig, Config }
import org.bitbucket.inkytonik.kiama.util.{
    CompilerBase,
    Config
}

import DOTSyntax.{ ASTNode, DotSpec }

class DOTParserConfig( args : Seq[ String ] ) extends Config( args ) {
    lazy val sourcePrint = opt[ Boolean ]( "srcprint", short = 's',
        descr = "Print the source code tree" )
    lazy val sourcePrettyPrint = opt[ Boolean ]( "srcprettyprint", short = 'p',
        descr = "Pretty-print the source code" )
}

class Driver extends CompilerBase[ ASTNode, DotSpec, DOTParserConfig ] {
    import DOTPrettyPrinter.{ any, pretty }
    import java.io.Reader
    import org.bitbucket.inkytonik.kiama.output.PrettyPrinterTypes.Document
    import org.bitbucket.inkytonik.kiama.util.{ Config, Emitter, OutputEmitter, Source }
    import org.bitbucket.inkytonik.kiama.util.Messaging.Messages

    val name = "dot"

    override def createConfig( args : Seq[ String ] ) : DOTParserConfig =
        new DOTParserConfig( args )

    val parser = null
    val parsers = null

    override def makeast( source : Source, config : DOTParserConfig ) : Either[ DotSpec, Messages ] = {
        val p = new DOT( source, positions )
        val pr = p.pDotSpec( 0 )
        if ( pr.hasValue )
            Left( p.value( pr ).asInstanceOf[ DotSpec ] )
        else
            Right( Vector( p.errorToMessage( pr.parseError ) ) )
    }

    def process( source : Source, ast : DotSpec, config : DOTParserConfig ) {
        if ( config.sourcePrint() )
            config.output().emit( format( ast ).layout )
        if ( config.sourcePrettyPrint() )
            config.output().emitln( pretty( any( ast ) ).layout )
    }

    def format( ast : DotSpec ) : Document =
        DOTPrettyPrinter.format( ast, 5 )

}

object Main extends Driver
