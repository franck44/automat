/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44

/**
 * This is the dccumentation for package [[automat]].
 *
 * Provides traits and classes for manipulating and building
 * automata.
 *
 * ==Overview and Usage==
 *
 * Usage is:
 *
 * {{{
 * scala> import au.edu.mq.automata._
 *
 * }}}
 *
 *
 *  ==Author==
 * Franck Cassez
 *
 */
package object automat {

    /**
     * Convenient type constructor for partial functions.
     * Imported from Kiama.
     */
    type ==>[ -A, +B ] = PartialFunction[ A, B ]

    /**
     * Every automaton is viewed as a deterministic machine.
     * `DState[A]` provides the internal state of the deternministic
     * version of an automaton.
     * @param A  Is the original state type of an automaton
     */

    type DState[ A ] = Option[ A ]

}
