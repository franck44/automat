/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat
package dpor

/**
 * A Mazurkiewicz trace equipped with dependency between events and thread information.
 *
 * @param   events          The sequence of events in the trace.
 * @param   areDependent    Given a sequence of events,
 *                          wether two events at two indices are dependent or not.
 *                          This function is not specific to the trace `events` and must
 *                          apply to arbitrary traces.
 * @param   proc            The process (thread) an event occurs in.
 */
case class MzTrace[ E ](
    val events : Seq[ E ],
    val proc : E ⇒ Int ) {
    /** Size of the trace */
    lazy val size = events.size

    /**
     *  Whether two indices are dependent in a trace.
     */
    // val areDependent : Seq[ E ] ⇒ ( Int, Int ) ⇒ Boolean = equiv.areDependent

    /**
     * Extract process of an event.
     */
    // val proc : E ⇒ Int = equiv.proc
}

/**
 * A Mazurkiewicz equivalence defined by the depdency relation.
 *
 * @param   areDependent    Given a sequence of events,
 *                          wether two events at two indices are dependent or not.
 *                          This function is not specific to the trace `events` and must
 *                          apply to arbitrary traces.
 * @param   proc            The process (thread) an event occurs in.
 */
case class MzEquiv[ E ](
    areDependent : Seq[ E ] ⇒ ( Int, Int ) ⇒ Boolean )

/**
 * Concurrent trace Analyser.
 */
case class MzAnalyser[ E ]( t : MzTrace[ E ], eqRel : MzEquiv[ E ] ) {

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    //  logger
    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    /**
     *  Happens before ->_t relation on the trace i.e. on `t.events`.
     *
     *  Event e1 happens-before e2 if it preceedes e1 in t.events
     *  and e1 and e2 are dependent.
     *  For each index i, happensBefore(i) is the list of indices
     *  in decreasing order of events
     *  j < i such t.events(j) ->_t t.events(i).
     */
    lazy val happensBefore : Map[ Int, List[ Int ] ] =
        ( ( 0 until t.size ) map {
            i ⇒
                ( i, ( 0 until i )
                    .reverse
                    .filter( eqRel.areDependent( t.events )( i, _ ) ).toList )
        } ).toMap

    /**
     *  Support for the <. relation in POPL paper.
     *  Given an index i, k <. i if k is the closest event before i such
     *  events(k) happens before events(i).
     *
     *  @param  i   The index of the event in `t.events`.
     *  @return     The set of indices k < i such that t.events(k) <. t.events(i) i.e.:
     *              1.  proc(t.events(i)) != proc(t.events(k))
     *              2.  t.events(k) and t.events(i) are dependent
     *              3.  there is no other event k < j < i such that
     *                  t.events(k) t.events(j) are dependent and t.events(j) t.events(i)
     *                  are dependent.
     */
    def <( i : Int ) : List[ Int ] = {
        require( 0 <= i && i < t.size, s"Index out $i of bound for < in ${t.events}" )
        logger.debug( s"Computing <$i for ${t.events}" )
        val r = happensBefore( i )
            //  Remove event with same process as events(i)
            .filter( x ⇒ t.proc( t.events( i ) ) != t.proc( t.events( x ) ) )
            //  Group by processes
            .groupBy( x ⇒ t.proc( t.events( x ) ) )
            .map( {
                //  Collect the largest index for each process. As happensBefore
                //  is in decreasing order, it is the head of xi.
                case ( proc, xi ) ⇒ xi.head
            } ).toList
        logger.debug( s"Result is $r" )
        r
    }

    /**
     *  j is in initial(e) if:
     *      1.  e is before j in t
     *      2.  not (e ->_t j) i.e. not happensBefore(j).contains(e)
     *      3.  there is no k such that e < k with not e ->_t k such that k ->_t j
     */
    def initial( k : Int ) : Seq[ Int ] = {
        require( 0 <= k && k < t.size, s"Index out $k of bound for simpleI in ${t.events}" )

        //  Compute indices of events that are not dependent on e after e (1. and 2.)
        val nonDepAfter = ( k + 1 until t.size )
            .filterNot( happensBefore( _ ).contains( k ) )
        //  Add last event e and keep only if 3.
        nonDepAfter.filter(
            x ⇒ nonDepAfter
                .filter(
                    y ⇒ y < x && happensBefore( x ).contains( y ) ).isEmpty )
    }

    /**
     *  Implements I[E'](v) in DPOR algorithm.
     *
     *  j is in initialForceLast(e) if :
     *      1.  e is before j in t
     *      2.  not (e ->_t j) i.e. not happensBefore(j).contains(e)
     *      3.  there is no k such that e < k with not e ->_t k such that k ->_t j
     */
    def initialForceLast( k : Int ) : Seq[ Int ] = {
        require( t.size > 0, s"Cannot add last element in I+ for the empty trace" )
        require( 0 <= k && k < t.size - 1, s"Index $k out of bound for addLastI in ${t.events}" )
        require(
            <( t.size - 1 ).contains( k ),
            s"Index $k should k <. lastElement in addLastI in ${t.events}" )

        //  Compute indices of events that are not dependent on e after e (1. and 2.)
        val nonDepAfter = ( k + 1 until t.size )
            .filterNot( happensBefore( _ ).contains( k ) )
        //  Add last event e and keep only if 3.
        ( nonDepAfter :+ ( t.size - 1 ) ).filter(
            x ⇒ nonDepAfter
                .filter(
                    y ⇒ y < x && happensBefore( x ).contains( y ) ).isEmpty )
    }

    /**
     * Given a set S of events, the events in S that do not happen after last(t) in t.
     *
     * @param   s       A set of events
     * @return          The subset of s that comprises the events that do not heppen-after
     *                  the last event in t.
     */
    def indepOfLastIn( s : Set[ E ] ) = {
        //  Remove e from s if the last event of t happens before e in t.e
        s filterNot {
            e ⇒
                //  Analyser for extended trace
                val xT = t.copy( events = t.events :+ e )
                val xTraceAnalyser = MzAnalyser( xT, eqRel )
                //  last event in xT is e at index t.size and second last is at index t.size - 1
                xTraceAnalyser.happensBefore( t.size ).contains( t.size - 1 )
        }
    }
}

/**
 * Provide DFS traversal with DPOR with early termination and visitors to collect data.
 */
object Traversal {

    import util.DFSVisitor

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    //  logger
    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    //  Colours to perform DFS
    private sealed trait Colours
    private object White extends Colours {
        override def toString = "White"
    }
    private object White2 extends Colours {
        override def toString = "Black"
    }
    private object Gray extends Colours {
        override def toString = "Gray"
    }
    private object Gray2 extends Colours {
        override def toString = "Gray"
    }
    private object Black extends Colours {
        override def toString = "Black"
    }

    /**
     * Performs Depth-First Search with Dynamic Partial Order Reduction.
     * @param   v       A visitor.
     * @param   next    A function that defines the transition relation. Must be acyclic
     *                  for DPOR to be sound (i.e. not miss traces and generate at least one
     *                  representative per MzTrace).
     * @param   proc    A function that assigns threadId to a label.
     * @param   dep     Whether two instructions at `i` and `j` are dependent in a trace.
     * @return          A function from `S` to `V`. When called performs a
     *                  DFSPOR from `s` and returns a `V`.
     */
    def DFSDPOR[ S, L, V <: DFSVisitor[ S, L, V ] ](
        v : V,
        next : S ⇒ List[ ( L, S ) ],
        proc : L ⇒ Int,
        mzDep : MzEquiv[ L ] ) : S ⇒ V = {

        case s ⇒
            //  local definition of recursive DFS

            /**
             * Performs tail recursive Depth-First Search.
             *
             * @param   stack         Current state of the search.
             *
             * @param   backTrack     Labels of outgoing edges to explore from a state.
             *
             * @param   sleep         Labels of outgoing edges not to be explored.
             *
             * @param   trace         Labels currently explored from initial state.
             *
             * @param   colourMap     Gives the colour of each state on the stack
             *                        If undefined then it is White (not seen yet),
             *                        false then Gray  (on the stack and not fully
             *                        explored) and true then Black (all successors
             *                        explored and thus fully explored).
             *
             * @param   time          Current discovery time (updated every time a
             *                        new state is  encountered).
             *
             * @param   discoveryTime The map that gives the discorery time for each state.
             *
             * @param   parent        The parent Map for the currently explored
             *                        sub spanning tree.
             *
             * @param   v             A visitor [[DFSVisitor]].
             */
            import scala.annotation.tailrec
            @tailrec def DFSPORRec(
                stack : List[ S ],
                backTrack : Map[ S, Set[ L ] ],
                sleep : Map[ S, Set[ L ] ],
                trace : Seq[ L ],
                colourMap : Map[ S, Boolean ],
                time : Int,
                discoveryTime : Map[ S, Int ],
                parent : Map[ S, ( S, L ) ],
                v : V ) : V = {

                //  Helpers

                //  Manage states' colours using Map
                @inline def colour( s : S ) = colourMap.get( s ) match {
                    case Some( false ) ⇒ Gray
                    case Some( true )  ⇒ Black
                    case None          ⇒ White
                }

                /**
                 * Select all transitions enabled in one a process (if any).
                 *
                 * @param   outEdges   The list of all actions.
                 * @return             A (maximal) subset of `l` that comprises all the outgoing edges
                 *                     of one process.
                 */
                def getEnabledInOneProc( outEdges : List[ ( L, S ) ] ) : List[ ( L, S ) ] = {
                    //  Group labels by processes and return the first one if any
                    val k = outEdges.groupBy( { case ( l, s ) ⇒ proc( l ) } ).toList match {
                        case Nil            ⇒ List()
                        case ( _, xl ) :: _ ⇒ xl
                    }

                    //  return l to enable all transitions
                    k
                }

                /**
                 * Successor function in `next`.
                 *
                 * @param   s   The source state.
                 * @param   l   The successor state of s via l in `next`.
                 */
                def succ( s : S, l : L ) : S =
                    next( s )
                        .find( { case ( b, _ ) ⇒ b == l } ) match {
                            case Some( ( _, q ) ) ⇒ q
                            case None             ⇒ sys.error( s"Could not find successor of $s via $l" )
                        }

                /**
                 * Given a trace, provide at one event to backTrack at some states.
                 */
                def actionsToBackTrack( trace : Seq[ L ] ) : Map[ S, L ] = {
                    logger.info( s"Stack is $stack" )
                    logger.info( s"Next for stack is:\n${stack.map( next( _ ) ).mkString( "\n" )}" )

                    //  stack contains states visited in reverse order
                    val stackFromInit = stack.reverse

                    //  Analyser for trace
                    val traceAnalyser = MzAnalyser( MzTrace( trace, proc ), mzDep )
                    //  Indices i of events in trace such trace(i) <~ last(trace)
                    //  i <. last(trace) and last(trace) enabled in pre(trace,i)
                    val indicesToUpdate = traceAnalyser.<( trace.size - 1 )
                        .filter(
                            x ⇒ next( stackFromInit( x ) )
                                .map( { case ( l, _ ) ⇒ l } )
                                .contains( trace( x ) ) )
                    logger.info( s"Indices to update in $trace:  ${indicesToUpdate}" )

                    //  For each e in indicesToUpdate, compute I[pre(trace,e)](notdep(e,E).last(trace))
                    val toExploreAt : List[ ( Int, Set[ L ] ) ] =
                        indicesToUpdate map {
                            i ⇒ ( i, traceAnalyser.initialForceLast( i ).map( trace( _ ) ).toSet )
                        }
                    logger.info( s"ExploreAt ${toExploreAt}" )

                    val addToBackTrackAt = toExploreAt map {
                        case ( i, setAti ) ⇒ (
                            stackFromInit( i ),
                            setAti & backTrack ( stackFromInit( i ) ) match {
                                case s if s.isEmpty ⇒ ( setAti &~ backTrack ( stackFromInit( i ) ) ).toList
                                case _              ⇒ List()
                            } )
                    }
                    logger.info( s"Add to backTrack ${addToBackTrackAt}" )
                    addToBackTrackAt.collect( { case ( s, f :: _ ) ⇒ ( s, f ) } ).toMap
                }

                //  Start DFS with DPOR.

                //  If visitor says search is over then stop and return current result v
                if ( v.abortSearch ) {
                    logger.info( "Aborting DFS." )
                    logger.info( s"Statistics: explored ${time + 1} states and ${parent.size} edges" )
                    v
                } else //  Otherwise continue search
                    stack match {

                        // empty stack. DFS is completed and we return the Visitor v
                        case Nil ⇒
                            logger.info( "Empty stack. DFS completed" )
                            logger.info( s"Statistics: explored ${time + 1} states and ${parent.size} edges" )
                            v

                        //  stack is not empty, process the state x at top of the stack
                        case x :: xs ⇒ {
                            colour( x ) match {

                                //  x is a new state not discovered yet
                                case White ⇒
                                    logger.info( s"Discovered White state $x" )
                                    //
                                    DFSPORRec(
                                        x :: xs,
                                        //  Try to find a process with some enabled transitions not in sleep
                                        backTrack.updated(
                                            x,
                                            getEnabledInOneProc( next( x ).filterNot( a ⇒ sleep.getOrElse( x, Set() ).contains( a._1 ) ) )
                                                .map( _._1 )
                                                .toSet ),
                                        sleep,
                                        trace,
                                        colourMap + ( x → false ), time + 1,
                                        discoveryTime + ( x → time ), parent,
                                        v.discoverState( x, discoveryTime + ( x → time ), parent ) )

                                //  x already discovered. Check if any outgoing edge needs to be explored.
                                //  Check edges that remain to be explored according to the DPOR
                                case Gray ⇒ ( backTrack.getOrElse( x, Set() ) &~ sleep.getOrElse( x, Set() ) ).toList match {
                                    //  no more transitions. x is fully explored, pop it.
                                    case Nil ⇒
                                        logger.info( s"State $x fully explored" )

                                        //  Trace should be empty if and only if xs is empty (DFS is complete)
                                        assert( xs.isEmpty == trace.isEmpty )
                                        DFSPORRec(
                                            xs,
                                            backTrack,
                                            sleep,
                                            //  If trace is empty we have finished the exploration of
                                            //  the initial state and the DFS terminates.
                                            if ( trace.isEmpty ) trace else trace.init,
                                            colourMap + ( x → true ), time,
                                            discoveryTime, parent, v.finishState( x, discoveryTime, parent ) )

                                    //  some transitions left to explore
                                    case l :: xl ⇒
                                        logger.info( s"Trace is $trace" )
                                        logger.info( s"State $x not fully explored: ${l :: xl}" )
                                        logger.info( s"Sleep is $sleep" )
                                        logger.info( s"Exploring $l" )

                                        //  x -- l -> tgt
                                        val tgt = succ( x, l )
                                        //  Analyser for the trace trace.l
                                        val xTrace = MzTrace( trace :+ l, proc )
                                        val xTraceAnalyser = MzAnalyser( xTrace, mzDep )
                                        val sleepAtTgt = xTraceAnalyser.indepOfLastIn( sleep.getOrElse( x, Set() ) )
                                        logger.info( s"sleepAtTgt $tgt is $sleepAtTgt" )

                                        val updatedSleep = sleep
                                            .updated( tgt, sleepAtTgt )
                                            .updated( x, sleep.getOrElse( x, Set() ) + l )
                                        logger.info( s"UpdatedSleep is $updatedSleep" )

                                        //  update backTrack with actionsToBackTrack
                                        val newActionsToBackTrackAt : Map[ S, L ] = actionsToBackTrack( trace :+ l )
                                        logger.info( s"newActionsToBackTrack ${newActionsToBackTrackAt}" )

                                        val updatedBackTrack = backTrack map {
                                            case ( s, xs ) ⇒
                                                if ( newActionsToBackTrackAt.isDefinedAt( s ) ) {
                                                    logger.info( s"Adding backtrack action ${newActionsToBackTrackAt( s )} to state $s" )
                                                    ( s, backTrack( s ) + newActionsToBackTrackAt( s ) )
                                                } else
                                                    ( s, backTrack( s ) )
                                        }

                                        colour( tgt ) match {
                                            //  tgt is a new state
                                            case White ⇒
                                                logger.info( s"New target state $tgt (white)" )

                                                DFSPORRec(
                                                    tgt :: x :: xs,
                                                    updatedBackTrack,
                                                    updatedSleep,
                                                    trace :+ l,
                                                    colourMap,
                                                    time,
                                                    discoveryTime, parent + ( tgt → ( ( x, l ) ) ),
                                                    v.treeEdge( x, l, tgt, discoveryTime, parent ) )

                                            //  tgt has already been discovered and is on the stack (not fully explored)
                                            case Gray ⇒
                                                logger.info( s"Already discovered target state $tgt (gray)" )
                                                DFSPORRec(
                                                    x :: xs,
                                                    updatedBackTrack,
                                                    updatedSleep,
                                                    trace,
                                                    colourMap, time,
                                                    discoveryTime, parent, v.backEdge( x, l, tgt, discoveryTime, parent ) )

                                            //  tgt has already been fully explored. Recheck parent x.
                                            case Black ⇒
                                                logger.info( s"Already fully explored target state $tgt (black)" )
                                                DFSPORRec(
                                                    x :: xs,
                                                    updatedBackTrack,
                                                    updatedSleep,
                                                    trace,
                                                    colourMap, time,
                                                    discoveryTime, parent, v.forwardEdge( x, l, tgt, discoveryTime, parent ) )
                                        }
                                }

                                //  case Black should not happen as every state pushed on the stack is not Black
                                case Black ⇒
                                    logger.error( s"Black node $x on the Stack in DFS." )
                                    throw new RuntimeException( "Black node on the Stack in DFS." )
                            }
                        }
                    }
            }

            //  start the DFS
            DFSPORRec( List( s ), Map(), Map(), Seq(), Map(), 0, Map(), Map(), v )
    }

    /**
     * Performs Depth-First Search with Dynamic Partial Order Reduction.
     *
     * The DFSDPOR is performed by giving priority to the enabled transitions in next2
     * intersected with next1. If empty, then next1 is used in standrad DPOR.
     * The net effect is to process traces that are in next2 first, and remaining traces
     * second. This is useful to check emptiness of differences of MzLang (next1) and
     * Mz closure of regular languages (next2).
     * The DFS stops when a `loop` is encountered, i.e. a state already on the stack is
     * encountered.
     *
     * @param   v       A visitor.
     * @param   next1   A function that defines the transition relation of a concurrent
     *                  process that can generate all the interleavings.
     * @param   next2   A function that defines the transition relation of an automaton
     *                  that contains a regular language (no need for all interleavings).
     *                  next2 should be included in next1.
     * @param   proc    A function that assigns threadId to a label.
     * @param   dep     Whether two instructions at `i` and `j` are dependent in a trace.
     * @return          A function from `S` to `V`. When called performs a
     *                  DFSPOR from `s` and returns a `V`.
     */
    def DFSDPORDiff[ S, L, V <: DFSVisitor[ ( S, Seq[ L ] ), L, V ] ](
        v : V,
        next1 : S ⇒ List[ ( L, S ) ],
        next2 : S ⇒ List[ ( L, S ) ],
        proc : L ⇒ Int,
        mzDep : MzEquiv[ L ] ) : S ⇒ V = {

        case s ⇒
            //  local definition of recursive DFS

            /**
             * Performs tail recursive Depth-First Search.
             *
             * @param   stack         Current state of the search.
             *
             * @param   backTrack     Labels of outgoing edges to explore from a state.
             *
             * @param   sleep         Labels of outgoing edges not to be explored.
             *
             * @param   trace         Labels currently explored from initial state.
             *
             * @param   colourMap     Gives the colour of each state on the stack
             *                        If undefined then it is White (not seen yet),
             *                        false then Gray  (on the stack and not fully
             *                        explored) and true then Black (all successors
             *                        explored and thus fully explored).
             *
             * @param   time          Current discovery time (updated every time a
             *                        new state is  encountered).
             *
             * @param   discoveryTime The map that gives the discorery time for each state.
             *
             * @param   parent        The parent Map for the currently explored
             *                        sub spanning tree.
             *
             * @param   v             A visitor [[DFSVisitor]].
             */
            import scala.annotation.tailrec
            @tailrec def DFSDPORDiffRec(
                stack : List[ S ],
                backTrack : Map[ S, List[ L ] ],
                sleep : Map[ S, Set[ L ] ],
                trace : Seq[ L ],
                colourMap : Map[ S, Colours ],
                time : Int,
                discoveryTime : Map[ ( S, Seq[ L ] ), Int ],
                parent : Map[ ( S, Seq[ L ] ), ( ( S, Seq[ L ] ), L ) ],
                v : V ) : V = {

                //  Helpers

                //  Manage states' colours using Map
                @inline def colour( s : S ) : Colours = colourMap.getOrElse( s, White )

                //  Manage states' colours using Map
                // @inline def colour( s : S ) = colourMap.get( s ) match {
                //     case Some( false ) ⇒ Gray
                //     case Some( true )  ⇒ Black
                //     case None          ⇒ White
                // }

                /**
                 * Select all transitions enabled in one a process (if any).
                 *
                 * @param   outEdges   The list of all actions.
                 * @return             A (maximal) subset of `l` that comprises all the outgoing edges
                 *                     of one process.
                 */
                def getEnabledInOneProc( outEdges : List[ ( L, S ) ] ) : List[ ( L, S ) ] = {
                    //  Group labels by processes and return the first one if any
                    val k = outEdges.groupBy( { case ( l, s ) ⇒ proc( l ) } ).toList match {
                        case Nil            ⇒ List()
                        case ( _, xl ) :: _ ⇒ xl
                    }

                    //  return l to enable all transitions
                    k
                }

                /**
                 * List of transitions enabled in next1 intersected with next2.
                 *
                 * @param   s   A state of the system.
                 * @return      Transitions enabled in next1 and next2 from `s`.
                 */
                def nextForced( s : S ) : List[ ( L, S ) ] = ( next1( s ).toSet & next2( s ).toSet ).toList

                /**
                 * Successor function in `next`.
                 *
                 * @param   s   The source state.
                 * @param   l   The successor state of s via l in `next`.
                 */
                def succ( s : S, l : L ) : S =
                    next1( s )
                        .find( { case ( b, _ ) ⇒ b == l } ) match {
                            case Some( ( _, q ) ) ⇒ q
                            case None             ⇒ sys.error( s"Could not find successor of $s via $l" )
                        }

                /**
                 * Returns a list of actions enabled in s, not in sleep(x), with actions from nextForced(x) first
                 */
                // def getNext( s : S ) : List[ L ] = {
                //     ( nextForced( s ) &~ sleep.getOrElse( s, Set() ) ).toList match {
                //         case l :: _ ⇒ List( l )
                //         case Nil ⇒ ( backTrack( s ).toSet &~ sleep.getOrElse( s, Set() ) ).toList match {
                //             case l :: _ ⇒ List( l )
                //             case _      ⇒ List()
                //         }
                //     }
                // }

                /**
                 * Given a trace, provide at one event to backTrack at some states.
                 */
                def actionsToBackTrack( trace : Seq[ L ] ) : Map[ S, L ] = {
                    logger.info( s"Stack is $stack" )
                    logger.info( s"Next for stack is:\n${stack.map( next1( _ ) ).mkString( "\n" )}" )

                    //  stack contains states visited in reverse order
                    val stackFromInit = stack.reverse

                    //  Analyser for trace
                    val traceAnalyser = MzAnalyser( MzTrace( trace, proc ), mzDep )
                    //  Indices i of events in trace such trace(i) <~ last(trace)
                    //  i <. last(trace) and last(trace) enabled in pre(trace,i).
                    //  enabled in relative to next1.
                    val indicesToUpdate = traceAnalyser.<( trace.size - 1 )
                        .filter(
                            x ⇒ next1( stackFromInit( x ) )
                                .map( { case ( l, _ ) ⇒ l } )
                                .contains( trace( x ) ) )
                    logger.info( s"Indices to update in $trace:  ${indicesToUpdate}" )

                    //  For each e in indicesToUpdate, compute I[pre(trace,e)](notdep(e,E).last(trace))
                    val toExploreAt : List[ ( Int, Set[ L ] ) ] =
                        indicesToUpdate map {
                            i ⇒ ( i, traceAnalyser.initialForceLast( i ).map( trace( _ ) ).toSet )
                        }
                    logger.info( s"ExploreAt ${toExploreAt}" )

                    val addToBackTrackAt = toExploreAt map {
                        case ( i, setAti ) ⇒ (
                            stackFromInit( i ),
                            setAti & backTrack ( stackFromInit( i ) ).toSet match {
                                case s if s.isEmpty ⇒ ( setAti &~ backTrack ( stackFromInit( i ) ).toSet ).toList
                                case _              ⇒ List()
                            } )
                    }
                    logger.info( s"Add to backTrack ${addToBackTrackAt}" )
                    addToBackTrackAt.collect( { case ( s, f :: _ ) ⇒ ( s, f ) } ).toMap
                }

                //  Start DFS with DPOR.

                //  If visitor says search is over then stop and return current result v
                if ( v.abortSearch ) {
                    logger.info( "Aborting DFS [Visitor abortSearch is true]." )
                    logger.info( s"Statistics: explored ${time + 1} states and ${parent.size} edges" )
                    v
                } else
                    //  Otherwise continue search
                    stack match {

                        // empty stack. DFS is completed and we return the Visitor v
                        case Nil ⇒
                            logger.info( "Empty stack. DFS completed" )
                            logger.info( s"Statistics: explored ${time + 1} states and ${parent.size} edges" )
                            v

                        //  stack is not empty, process the state x at top of the stack
                        case x :: xs ⇒ {
                            colour( x ) match {

                                //  x is a new state not discovered yet
                                case White ⇒
                                    logger.info( s"Discovered White state $x" )
                                    //
                                    DFSDPORDiffRec(
                                        x :: xs,
                                        //  Try to find a process with some enabled transitions not in sleep
                                        //  Queue for transitions in next1 & next2 first.
                                        backTrack.updated(
                                            x,
                                            ( nextForced( x ) :::
                                                getEnabledInOneProc(
                                                    next1( x ).filterNot( a ⇒ ( nextForced( x ) ++ sleep.getOrElse( x, Set() ) ).contains( a._1 ) ) ) ).map( _._1 ) ),
                                        sleep,
                                        trace,
                                        colourMap + ( x → Gray ), time + 1,
                                        discoveryTime + ( ( x, trace ) → time ), parent,
                                        v.discoverState( ( x, trace ), discoveryTime + ( ( x, trace ) → time ), parent ) )

                                //  x already discovered and currently processing nextForced transiutions.
                                //  Check if any outgoing edge needs to be explored.
                                case Gray ⇒ ( backTrack( x ).toSet &~ sleep.getOrElse( x, Set() ) ).toList match {
                                    //  no more transitions. x is fully explored, pop it.
                                    case Nil ⇒
                                        logger.info( s"State $x fully explored" )

                                        //  Trace should be empty if and only if xs is empty (DFS is complete)
                                        assert( xs.isEmpty == trace.isEmpty )
                                        DFSDPORDiffRec(
                                            xs,
                                            //  clear backTrack and sleep for state x for the current trace
                                            backTrack - x,
                                            sleep - x,
                                            //  If trace is empty we have finished the exploration of
                                            //  the initial state and the DFS terminates.
                                            if ( trace.isEmpty ) trace else trace.init,
                                            //  Remove x from colourMap (i.e. forget reset to White. if encountered in other branches
                                            //  it will be treated as a fresh white state). Equivalent to setting colourMap(x,trace) to black
                                            colourMap - x,
                                            time,
                                            discoveryTime, parent, v.finishState( ( x, trace ), discoveryTime, parent ) )

                                    //  some transitions left to explore
                                    case l :: xl ⇒
                                        logger.info( s"Trace is $trace" )
                                        logger.info( s"State $x not fully explored, remaining: ${l :: xl}" )
                                        logger.info( s"Sleep is $sleep" )
                                        logger.info( s"Exploring $l" )

                                        //  x -- l -> tgt
                                        val tgt = succ( x, l )
                                        //  Analyser for the trace trace.l
                                        val xTrace = MzTrace( trace :+ l, proc )
                                        val xTraceAnalyser = MzAnalyser( xTrace, mzDep )
                                        //  All the transitions in backTrack are forced so sleep them if indep of l.
                                        val sleepAtTgt = xTraceAnalyser.indepOfLastIn( sleep.getOrElse( x, Set() ) )

                                        // val sleepAtTgt = xTraceAnalyser.indepOfLastIn( backTrack( x ).toSet )
                                        logger.info( s"sleepAtTgt $tgt is $sleepAtTgt" )

                                        val updatedSleep = sleep
                                            .updated( tgt, sleepAtTgt )
                                            .updated( x, sleep.getOrElse( x, Set() ) + l )
                                        logger.info( s"UpdatedSleep is $updatedSleep" )

                                        //  update backTrack with actionsToBackTrack
                                        val newActionsToBackTrackAt : Map[ S, L ] = actionsToBackTrack( trace :+ l )
                                        logger.info( s"newActionsToBackTrack ${newActionsToBackTrackAt}" )

                                        val updatedBackTrack = backTrack map {
                                            case ( s, xs ) ⇒
                                                if ( newActionsToBackTrackAt.isDefinedAt( s ) ) {
                                                    logger.info( s"Adding backtrack action ${newActionsToBackTrackAt( s )} to state $s" )
                                                    ( s, backTrack( s ) :+ newActionsToBackTrackAt( s ) )
                                                } else
                                                    ( s, backTrack( s ) )
                                        }

                                        colour( tgt ) match {
                                            //  tgt is a new state
                                            case White ⇒
                                                logger.info( s"New target state $tgt (white)" )

                                                DFSDPORDiffRec(
                                                    tgt :: x :: xs,
                                                    updatedBackTrack,
                                                    updatedSleep,
                                                    trace :+ l,
                                                    colourMap,
                                                    time,
                                                    discoveryTime,
                                                    parent + ( ( tgt, trace :+ l ) → ( ( ( x, trace ), l ) ) ),
                                                    v.treeEdge( ( x, trace ), l, ( tgt, trace :+ l ), discoveryTime, parent ) )

                                            //  tgt has already been discovered and is on the stack (not fully explored)
                                            case Gray ⇒
                                                logger.info( s"Already discovered target state $tgt (gray)" )
                                                DFSDPORDiffRec(
                                                    x :: xs,
                                                    updatedBackTrack,
                                                    updatedSleep,
                                                    trace,
                                                    colourMap,
                                                    time,
                                                    discoveryTime, parent,
                                                    v.backEdge( ( x, trace ), l, ( tgt, trace :+ l ), discoveryTime, parent ) )

                                            // tgt is Black. Should not happen.
                                            case Black ⇒
                                                logger.error( s"Black node $x discovered while exploring." )
                                                throw new RuntimeException( "Black node discovered while exploring." )
                                        }
                                }

                                //  case Black should not happen as every state pushed on the stack is not Black
                                case Black ⇒
                                    logger.error( s"Black node $x on the Stack in DFS." )
                                    throw new RuntimeException( "Black node on the Stack in DFS." )
                            }
                        }
                    }
            }

            //  start the DFS
            DFSDPORDiffRec( List( s ), Map(), Map(), Seq(), Map(), 0, Map(), Map(), v )
    }

    /**
     * Performs Depth-First Search with Dynamic Partial Order Reduction.
     * @param   v       A visitor.
     * @param   next    A function that defines the transition relation.
     * @param   proc    A function that assigns threadId to a label.
     * @param   dep     Whether two instructions at `i` and `j` are dependent in a trace.
     * @return          A function from `S` to `V`. When called performs a
     *                  DFSPOR from `s` and returns a `V`.
     */
    def DFSDPORDiff2[ S, L, V <: DFSVisitor[ S, L, V ] ](
        v : V,
        next1 : S ⇒ List[ ( L, S ) ], //  original next A
        next2 : S ⇒ List[ ( L, S ) ], //  next of product A times B
        proc : L ⇒ Int,
        mzDep : MzEquiv[ L ] ) : S ⇒ ( V, V ) = {

        case s ⇒
            //  local definition of recursive DFS

            /**
             * Performs tail recursive Depth-First Search.
             *
             * @param   stack         Current state of the search.
             *
             * @param   backTrack     Labels of outgoing edges to explore from a state.
             *
             * @param   sleep         Labels of outgoing edges not to be explored.
             *
             * @param   trace         Labels currently explored from initial state.
             *
             * @param   colourMap     Gives the colour of each state on the stack
             *                        If undefined then it is White (not seen yet),
             *                        false then Gray  (on the stack and not fully
             *                        explored) and true then Black (all successors
             *                        explored and thus fully explored).
             *
             * @param   time          Current discovery time (updated every time a
             *                        new state is  encountered).
             *
             * @param   discoveryTime The map that gives the discorery time for each state.
             *
             * @param   parent        The parent Map for the currently explored
             *                        sub spanning tree.
             *
             * @param   v             A visitor [[DFSVisitor]].
             */
            import scala.annotation.tailrec
            @tailrec def DFSPORRec(
                stack : List[ S ],
                backTrack : Map[ S, Set[ L ] ],
                sleep : Map[ S, Set[ L ] ],
                trace : Seq[ L ],
                colourMap : Map[ S, Colours ],
                time : Int,
                discoveryTime : Map[ S, Int ],
                parent : Map[ S, ( S, L ) ],
                v1 : V,
                v2 : V ) : ( V, V ) = {

                //  Helpers

                //  Manage states' colours using Map
                @inline def colour( s : S ) = colourMap.getOrElse( s, White )

                /**
                 * Select all transitions enabled in one a process (if any).
                 *
                 * @param   outEdges   The list of all actions.
                 * @return             A (maximal) subset of `l` that comprises all the outgoing edges
                 *                     of one process.
                 */
                def getEnabledInOneProc( outEdges : List[ ( L, S ) ] ) : List[ ( L, S ) ] = {
                    //  Group labels by processes and return the first one if any
                    val k = outEdges.groupBy( { case ( l, s ) ⇒ proc( l ) } ).toList match {
                        case Nil            ⇒ List()
                        case ( _, xl ) :: _ ⇒ xl
                    }

                    //  return l to enable all transitions
                    k
                }

                /**
                 * Successor function in `next`.
                 *
                 * @param   s   The source state.
                 * @param   l   The successor state of s via l in `next`.
                 */
                def succ1( s : S, l : L ) : S =
                    next1( s )
                        .find( { case ( b, _ ) ⇒ b == l } ) match {
                            case Some( ( _, q ) ) ⇒ q
                            case None             ⇒ sys.error( s"Could not find successor of $s via $l" )
                        }

                def succ2( s : S, l : L ) : S =
                    next2( s )
                        .find( { case ( b, _ ) ⇒ b == l } ) match {
                            case Some( ( _, q ) ) ⇒ q
                            case None             ⇒ sys.error( s"Could not find successor of $s via $l" )
                        }

                /**
                 * Given a trace, provide at one event to backTrack at some states.
                 */
                def actionsToBackTrack( trace : Seq[ L ], next : S ⇒ List[ ( L, S ) ] ) : Map[ S, L ] = {
                    logger.info( s"Stack is $stack" )
                    logger.info( s"Next for stack is:\n${stack.map( next1( _ ) ).mkString( "\n" )}" )

                    //  stack contains states visited in reverse order
                    val stackFromInit = stack.reverse

                    //  Analyser for trace
                    val traceAnalyser = MzAnalyser( MzTrace( trace, proc ), mzDep )
                    //  Indices i of events in trace such trace(i) <~ last(trace)
                    //  i <. last(trace) and last(trace) enabled in pre(trace,i)
                    val indicesToUpdate = traceAnalyser.<( trace.size - 1 )
                        .filter(
                            x ⇒ next( stackFromInit( x ) )
                                .map( { case ( l, _ ) ⇒ l } )
                                .contains( trace( x ) ) )
                    logger.info( s"Indices to update in $trace:  ${indicesToUpdate}" )

                    //  For each e in indicesToUpdate, compute I[pre(trace,e)](notdep(e,E).last(trace))
                    val toExploreAt : List[ ( Int, Set[ L ] ) ] =
                        indicesToUpdate map {
                            i ⇒ ( i, traceAnalyser.initialForceLast( i ).map( trace( _ ) ).toSet )
                        }
                    logger.info( s"ExploreAt ${toExploreAt}" )

                    val addToBackTrackAt = toExploreAt map {
                        case ( i, setAti ) ⇒ (
                            stackFromInit( i ),
                            setAti & backTrack ( stackFromInit( i ) ) match {
                                case s if s.isEmpty ⇒ ( setAti &~ backTrack ( stackFromInit( i ) ) ).toList
                                case _              ⇒ List()
                            } )
                    }
                    logger.info( s"Add to backTrack ${addToBackTrackAt}" )
                    addToBackTrackAt.collect( { case ( s, f :: _ ) ⇒ ( s, f ) } ).toMap
                }

                //  Start DFS with DPOR.

                //  If visitor says search is over then stop and return current result v
                if ( v1.abortSearch || v2.abortSearch ) {
                    logger.info( "Aborting DFS." )
                    logger.info( s"Statistics: explored ${time + 1} states and ${parent.size} edges" )
                    ( v1, v2 )
                } else //  Otherwise continue search
                    stack match {

                        // empty stack. DFS is completed and we return the Visitor v
                        case Nil ⇒
                            logger.info( "Empty stack. DFS completed" )
                            logger.info( s"Statistics: explored ${time + 1} states and ${parent.size} edges" )
                            ( v1, v2 )

                        //  stack is not empty, process the state x at top of the stack
                        case x :: xs ⇒ {
                            colour( x ) match {

                                //  x is a new state not discovered yet
                                case White ⇒
                                    logger.info( s"Discovered White state $x" )
                                    //
                                    DFSPORRec(
                                        x :: xs,
                                        //  Try to find a process with some enabled transitions not in sleep
                                        backTrack.updated(
                                            x,
                                            //  Add next2 outgoing edges
                                            (
                                                getEnabledInOneProc( next2( x ).filterNot( a ⇒ sleep.getOrElse( x, Set() ).contains( a._1 ) ) ) ).map( _._1 ).toSet ),
                                        sleep,
                                        trace,
                                        colourMap + ( x → Gray2 ), time + 1,
                                        discoveryTime + ( x → time ), parent,
                                        v1,
                                        v2.discoverState( x, discoveryTime + ( x → time ), parent ) )

                                //  start 2nd DFS
                                case White2 ⇒
                                    logger.info( s"Discovered White2 state $x" )
                                    //
                                    logger.info( s"Parent is $parent" )

                                    DFSPORRec(
                                        x :: xs,
                                        //  Try to find a process with some enabled transitions not in sleep
                                        backTrack.updated(
                                            x,
                                            //  Add next2 outgoing edges
                                            (
                                                getEnabledInOneProc( next1( x ).filterNot( a ⇒ sleep.getOrElse( x, Set() ).contains( a._1 ) ) ) ).map( _._1 ).toSet ),
                                        sleep,
                                        trace,
                                        colourMap + ( x → Gray ), time + 1,
                                        discoveryTime + ( x → time ), parent,
                                        v2.discoverState( x, discoveryTime + ( x → time ), parent ),
                                        v2 )

                                case Gray2 ⇒ ( backTrack.getOrElse( x, Set() ).toSet &~ sleep.getOrElse( x, Set() ) ).toList match {
                                    //  no more transitions. x is fully explored, pop it.
                                    case Nil ⇒
                                        logger.info( s"[Gray2] Trace is $trace" )
                                        logger.info( s"[Gray2] BackTrack(x) is $backTrack" )
                                        logger.info( s"[Gray2] State $x fully explored for next2" )

                                        //  Trace should be empty if and only if xs is empty (DFS is complete)
                                        assert( xs.isEmpty == trace.isEmpty )
                                        DFSPORRec(
                                            x :: xs,
                                            backTrack,
                                            sleep,
                                            //  If trace is empty we have finished the exploration of
                                            //  the initial state for next2. Start next 1.
                                            // if ( trace.isEmpty ) trace else trace.init,
                                            trace,
                                            colourMap + ( x → White2 ), time,
                                            discoveryTime, parent,
                                            v1,
                                            v2.finishState( x, discoveryTime, parent ) )

                                    //  some transitions left to explore
                                    case l :: xl ⇒
                                        logger.info( s"[Gray2] Trace is $trace" )
                                        logger.info( s"[Gray2] State $x not fully explored: ${l :: xl}" )
                                        logger.info( s"[Gray2] Sleep is $sleep" )
                                        logger.info( s"[Gray2] Exploring $l" )

                                        //  x -- l -> tgt
                                        val tgt = succ2( x, l )
                                        //  Analyser for the trace trace.l
                                        val xTrace = MzTrace( trace :+ l, proc )
                                        val xTraceAnalyser = MzAnalyser( xTrace, mzDep )
                                        val sleepAtTgt = xTraceAnalyser.indepOfLastIn( sleep.getOrElse( x, Set() ) )
                                        logger.info( s"[Gray2] sleepAtTgt $tgt is $sleepAtTgt" )

                                        val updatedSleep = sleep
                                            .updated( tgt, sleepAtTgt )
                                            .updated( x, sleep.getOrElse( x, Set() ) + l )
                                        logger.info( s"[Gray2] UpdatedSleep is $updatedSleep" )

                                        //  update backTrack with actionsToBackTrack
                                        val newActionsToBackTrackAt : Map[ S, L ] = actionsToBackTrack( trace :+ l, next2 )
                                        logger.info( s"[Gray2] newActionsToBackTrack ${newActionsToBackTrackAt}" )

                                        val updatedBackTrack = backTrack map {
                                            case ( s, xs ) ⇒
                                                if ( newActionsToBackTrackAt.isDefinedAt( s ) ) {
                                                    logger.info( s"[Gray2] Adding backtrack action ${newActionsToBackTrackAt( s )} to state $s" )
                                                    ( s, backTrack( s ) + newActionsToBackTrackAt( s ) )
                                                } else
                                                    ( s, backTrack( s ) )
                                        }

                                        colour( tgt ) match {
                                            //  tgt is a new state
                                            case White ⇒
                                                logger.info( s"[Gray2] New target state $tgt (white)" )

                                                DFSPORRec(
                                                    tgt :: x :: xs,
                                                    updatedBackTrack,
                                                    updatedSleep,
                                                    trace :+ l,
                                                    colourMap,
                                                    time,
                                                    discoveryTime, parent + ( tgt → ( ( x, l ) ) ),
                                                    v1,
                                                    v2.treeEdge( x, l, tgt, discoveryTime, parent ) )

                                            //  tgt has already been discovered and is on the stack (not fully explored)
                                            case Gray2 ⇒
                                                logger.info( s"[Gray2] Already discovered target state $tgt (gray)" )
                                                DFSPORRec(
                                                    x :: xs,
                                                    updatedBackTrack,
                                                    updatedSleep,
                                                    trace,
                                                    colourMap, time,
                                                    discoveryTime, parent,
                                                    v1,
                                                    v2.backEdge( x, l, tgt, discoveryTime, parent ) )

                                            //  tgt has already been fully explored. Recheck parent x.
                                            case Black ⇒
                                                logger.info( s"[Gray2] Already fully explored target state $tgt (black)" )
                                                DFSPORRec(
                                                    x :: xs,
                                                    updatedBackTrack,
                                                    updatedSleep,
                                                    trace,
                                                    colourMap, time,
                                                    discoveryTime, parent,
                                                    v1,
                                                    v2.forwardEdge( x, l, tgt, discoveryTime, parent ) )
                                        }
                                }

                                //  x already discovered. Check if any outgoing edge needs to be explored.
                                //  Check edges that remain to be explored according to the DPOR
                                case Gray ⇒ ( backTrack.getOrElse( x, Set() ).toSet &~ sleep.getOrElse( x, Set() ) ).toList match {
                                    //  no more transitions. x is fully explored, pop it.
                                    case Nil ⇒
                                        logger.info( s"[Gray1] Trace is $trace" )
                                        logger.info( s"[Gray1] State $x fully explored" )

                                        //  Trace should be empty if and only if xs is empty (DFS is complete)
                                        assert( xs.isEmpty == trace.isEmpty )
                                        DFSPORRec(
                                            xs,
                                            backTrack,
                                            sleep,
                                            //  If trace is empty we have finished the exploration of
                                            //  the initial state and the DFS terminates.
                                            if ( trace.isEmpty ) trace else trace.init,
                                            colourMap + ( x → Black ), time,
                                            discoveryTime, parent,
                                            v1.finishState( x, discoveryTime, parent ),
                                            v2 )

                                    //  some transitions left to explore
                                    case l :: xl ⇒
                                        logger.info( s"[Gray1] Trace is $trace" )
                                        logger.info( s"[Gray1] State $x not fully explored: ${l :: xl}" )
                                        logger.info( s"[Gray1] Sleep is $sleep" )
                                        logger.info( s"[Gray1] Exploring $l" )

                                        //  x -- l -> tgt
                                        val tgt = succ1( x, l )
                                        //  Analyser for the trace trace.l
                                        val xTrace = MzTrace( trace :+ l, proc )
                                        val xTraceAnalyser = MzAnalyser( xTrace, mzDep )
                                        val sleepAtTgt = xTraceAnalyser.indepOfLastIn( sleep.getOrElse( x, Set() ) )
                                        logger.info( s"[Gray1] sleepAtTgt $tgt is $sleepAtTgt" )

                                        val updatedSleep = sleep
                                            .updated( tgt, sleepAtTgt )
                                            .updated( x, sleep.getOrElse( x, Set() ) + l )
                                        logger.info( s"[Gray1] UpdatedSleep is $updatedSleep" )

                                        //  update backTrack with actionsToBackTrack
                                        val newActionsToBackTrackAt : Map[ S, L ] = actionsToBackTrack( trace :+ l, next1 )
                                        logger.info( s"[Gray1] newActionsToBackTrack ${newActionsToBackTrackAt}" )

                                        val updatedBackTrack = backTrack map {
                                            case ( s, xs ) ⇒
                                                if ( newActionsToBackTrackAt.isDefinedAt( s ) ) {
                                                    logger.info( s"[Gray1] Adding backtrack action ${newActionsToBackTrackAt( s )} to state $s" )
                                                    ( s, backTrack( s ) + newActionsToBackTrackAt( s ) )
                                                } else
                                                    ( s, backTrack( s ) )
                                        }

                                        colour( tgt ) match {
                                            //  tgt is a new state
                                            case White ⇒
                                                logger.info( s"[Gray1] New target state $tgt (white)" )

                                                DFSPORRec(
                                                    tgt :: x :: xs,
                                                    updatedBackTrack,
                                                    updatedSleep,
                                                    trace :+ l,
                                                    colourMap,
                                                    time,
                                                    discoveryTime, parent + ( tgt → ( ( x, l ) ) ),
                                                    v1.treeEdge( x, l, tgt, discoveryTime, parent ),
                                                    v2 )

                                            //  tgt has already been discovered and is on the stack (not fully explored)
                                            case Gray ⇒
                                                logger.info( s"[Gray1] Already discovered target state $tgt (gray)" )
                                                DFSPORRec(
                                                    x :: xs,
                                                    updatedBackTrack,
                                                    updatedSleep,
                                                    trace,
                                                    colourMap, time,
                                                    discoveryTime, parent,
                                                    v1.backEdge( x, l, tgt, discoveryTime, parent ),
                                                    v2 )

                                            //  tgt has already been fully explored. Recheck parent x.
                                            case Black ⇒
                                                logger.info( s"[Gray1] Already fully explored target state $tgt (black)" )
                                                DFSPORRec(
                                                    x :: xs,
                                                    updatedBackTrack,
                                                    updatedSleep,
                                                    trace,
                                                    colourMap, time,
                                                    discoveryTime, parent,
                                                    v1.forwardEdge( x, l, tgt, discoveryTime, parent ),
                                                    v2 )
                                        }
                                }

                                //  case Black should not happen as every state pushed on the stack is not Black
                                case Black ⇒
                                    logger.error( s"Black node $x on the Stack in DFS." )
                                    throw new RuntimeException( "Black node on the Stack in DFS." )
                            }
                        }
                    }
            }

            //  start the DFS
            DFSPORRec( List( s ), Map(), Map(), Seq (), Map(), 0, Map(), Map(), v, v )
    }

}
