/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat

/**
 * This is the documentation for package [[edge]]
 *
 * ==Overview==
 *
 * This package contains the basic class to create directed edges in directed graph, [[DiEdge]]
 * and labelled directed edges [[LabDiEdge]].
 * An edge of type `LabDiEdge[S,L]` has type `S` for the nodes and `L` for the labels.
 *
 * ==Usage==
 *
 * {{{
 * scala>  import org.bitbucket.franck44.automat.edge.{DiEdge, LabDiEdge}
 * import org.bitbucket.franck44.automat.edge.{DiEdge, LabDiEdge}
 *
 * scala> val e = DiEdge(1,2)
 * e: org.bitbucket.franck44.automat.edge.DiEdge[Int] = DiEdge(1,2)
 *
 * scala> val e1 = LabDiEdge(e, "a")
 * e1: org.bitbucket.franck44.automat.edge.LabDiEdge[Int,String] = LabDiEdge(DiEdge(1,2),a)
 *
 * scala> val e2 = LabDiEdge(DiEdge(3,4), "b")
 * e2: org.bitbucket.franck44.automat.edge.LabDiEdge[Int,String] = LabDiEdge(DiEdge(3,4),b)
 *
 * scala> val e3 = new LabDiEdge(1, "a", 2)             // use the secondary constructor
 * e3: org.bitbucket.franck44.automat.edge.LabDiEdge[Int,String] = LabDiEdge(DiEdge(1,2),a)
 *
 * scala> e1 == e3
 * res1: Boolean = true
 *
 * scala> e1.src
 * res2: Int = 1
 *
 * scala> e1.tgt
 * res3: Int = 2
 *
 * scala> e1.reversed.src
 * res5: Int = 2
 *
 * scala> e1.reversed
 * res6: org.bitbucket.franck44.automat.edge.LabDiEdge[Int,String] = LabDiEdge(DiEdge(2,1),a)
 *
 * scala> e3.lab
 * res7: String = a
 *
 * scala> e3.src
 * res8: Int = 1
 *
 * scala> e3.reversed.lab
 * res9: String = a
 * }}}
 *
 * Implicits are provided to create labelled edges, unlabelled edges and labelled edges
 * fron unlabelled ones:
 *
 * {{{
 * scala> import org.bitbucket.franck44.automat.edge.Implicits._
 * import org.bitbucket.franck44.automat.edge.Implicits._
 *
 * scala> val e4 = 1 ~> 2
 * e4: org.bitbucket.franck44.automat.edge.DiEdge[Int] = DiEdge(1,2)
 *
 * scala> val e6 = e4("a")
 * e6: org.bitbucket.franck44.automat.edge.LabDiEdge[Int,String] = LabDiEdge(DiEdge(1,2),a)
 *
 * scala> val e5 = (1 ~> 2)("a")
 * e5: org.bitbucket.franck44.automat.edge.LabDiEdge[Int,String] = LabDiEdge(DiEdge(1,2),a)
 * }}}
 *
 * The equality operator is also available to compare edges:
 *
 * {{{
 * scala> e3 == e1
 * res1: Boolean = true
 *
 * scala> e6 == e5
 * res2: Boolean = true
 * }}}
 *
 */
package object edge {

}
