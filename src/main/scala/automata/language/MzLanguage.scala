/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat
package lang

import auto.{ DetAuto, NFA }
import dpor.MzEquiv

/**
 * Provide support for Mazurkiewicz languages accepted by [[auto.DetAuto]]
 */
case class MzLang[ S, L ](
    private val a : DetAuto[ S, L ],
    val mzDep : MzEquiv[ L ],
    proc : L ⇒ Int ) {

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    //  logger
    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    /**
     * Union of languages.
     */
    // def \/[ S1 ]( l2 : Lang[ S1, L ] ) = Lang( this.a + l2.a )

    /**
     * Intersection of languages.
     */
    // def /\[ S1 ]( l2 : Lang[ S1, L ] ) = Lang( this.a * l2.a )

    /**
     * Difference of languages.
     */
    def \[ S1 ]( l2 : MzLang[ S1, L ] ) = MzLang( this.a - l2.a, mzDep, proc )

    /**
     * Difference with priority of languages.
     */
    def --[ S1 ]( l2 : MzLang[ S1, L ] ) = MzLang( this.a -- l2.a, mzDep, proc )

    /**
     * Prefix quotient.
     */
    // def /[ S1 ]( xl : Seq[ L ] ) = Lang( this.a, xl )

    /**
     * Language emptiness.
     */
    lazy val isEmpty : Boolean = getAcceptedTrace.isEmpty

    /**
     * Language equality.
     */
    // def ===[ S1 ]( l2 : Lang[ S1, L ] ) : Boolean = ( l2 \ this ).isEmpty && ( this \ l2 ).isEmpty

    /**
     * Language inclusion.
     */
    // def <=[ S1 ]( l2 : Lang[ S1, L ] ) : Boolean = ( this \ l2 ).isEmpty

    /**
     * Membership check.
     *
     * @param   xl  A prefix trace
     * @return      `true` if the trace is in the language accepted by `a`.
     */
    // def accepts( xl : Seq[ L ] ) : Boolean = a.isFinal( a.succ( a.getInit, prefix ++ xl ) )

    /**
     * Compute an accepted trace if any
     */
    lazy val getAcceptedTrace : Option[ Seq[ L ] ] = getAcceptedTraceAfter( List() )

    /**
     * Look for a suffix `s` of a given trace `xl` such that `s.xl`
     * is accepted by `a`.˙
     */
    def getAcceptedTraceAfter( xl : Seq[ L ] ) : Option[ Seq[ L ] ] = {
        import dpor.Traversal.DFSDPOR

        //  perform a DFSDPOR with the RecordTrace visitor from the initial state
        val v = RecordTraceVisitor[ S, L ]( List(), false, a.isFinal )
        val dfsForAcc = DFSDPOR( v, a.outGoingEdges, proc, mzDep )
        val r = dfsForAcc( a.succ( a.getInit, xl ) )

        if ( r.accepted )
            Some( r.xl.reverse )
        else
            None
    }

    /**
     * Look for a suffix `s` of a given trace `xl` such that `s.xl`
     * is accepted by `a`.˙
     */
    def dfsdpor() : NFA[ Int, L ] = {
        import dpor.Traversal.DFSDPOR
        import util.Determiniser.DeterminiserVisitor

        //  perform a DFSDPOR with the RecordTrace visitor from the initial state
        val v = DeterminiserVisitor[ S, L, String ]( Map(), List(), { _ ⇒ "" }, Map() )
        val dfsForAcc = DFSDPOR( v, a.outGoingEdges, proc, mzDep )
        val r = dfsForAcc( a.getInit )

        val detReduced =
            new NFA(
                init = Set( 0 ),
                transitions = r.edges.toSet,
                accepting = Set(),
                name = "MzLang/dfsdpor" )
        detReduced

    }

    /**
     * Look for a suffix `s` of a given trace `xl` such that `s.xl`
     * is accepted by `a`.˙
     */
    def dfs() : NFA[ Int, L ] = {
        import util.Traversal.DFS
        import util.Determiniser.DeterminiserVisitor

        //  perform a DFSDPOR with the RecordTrace visitor from the initial state
        val v = DeterminiserVisitor[ S, L, String ]( Map(), List(), { _ ⇒ "" }, Map() )
        val dfsForAcc = DFS( v, a.outGoingEdges )
        val r = dfsForAcc( a.getInit )

        val detReduced =
            new NFA(
                init = Set( 0 ),
                transitions = r.edges.toSet,
                accepting = Set(),
                name = "MzLang/dfs" )
        detReduced

    }
}
