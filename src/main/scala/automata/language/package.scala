/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat

/**
 *
 * ==Overview==
 *
 * This package provides support for operations on languages accepted
 * by automata.
 *
 * ==Usage==
 *
 * A language is defined by the set of words accepted by an automaton.
 * A word `w` is accepted by an automaton if there is a path from an
 * initial state to a final state the trace of which is `w`.
 *
 * The following example defines the language accepted by an automaton
 * with a single initial and final state (no transitions).
 *
 * {{{
 * scala> import org.bitbucket.franck44.automat.auto.NFA
 * import org.bitbucket.franck44.automat.auto.NFA
 *
 * scala> import org.bitbucket.franck44.automat.lang.Lang
 * import org.bitbucket.franck44.automat.lang.Lang
 *
 * scala> val aut1 = NFA[Int, String]( Set( 0 ), Set(), Set( 0 ) )
 * aut1: org.bitbucket.franck44.automat.auto.NFA[Int,String] = NFA(Set(0),Set(),Set(0))
 *
 * scala> val l1 = Lang(aut1)
 * l1: org.bitbucket.franck44.automat.lang.Lang[Set[Int],String] = Lang(NFA(Set(0),Set(),Set(0)))
 *
 * scala> Lang(aut1).isEmpty
 * res1: Boolean = false
 *
 * scala> Lang(aut1).getAcceptedTrace
 * res2: Option[Seq[String]] = Some(List())
 * }}}
 * `res2` contains an accepted word that consists of the empty string (`List()`).
 *
 * ==Union, Intersection, Difference==
 *
 * Languages define sets of words and the Standard set operations are defined
 * on languages.
 *
 * {{{
 * scala> val aut2 = NFA[Int, String]( Set( 0 ), Set(), Set(  ) )
 * aut2: org.bitbucket.franck44.automat.auto.NFA[Int,String] = NFA(Set(0),Set(),Set())
 *
 * scala> val l1 = Lang( aut1 ) /\ Lang( aut2 )
 * l1: org.bitbucket.franck44.automat.lang.Lang[(org.bitbucket.franck44.automat.DState[Set[Int]],
 *   org.bitbucket.franck44.automat.DState[Set[Int]]),String] =
 *   Lang(Inter(NFA(Set(0),Set(),Set(0)),NFA(Set(0),Set(),Set())))
 *
 * scala> l1.isEmpty
 * res3: Boolean = true
 *
 * scala> val l2 = Lang( aut1 ) \/ Lang( aut2 )
 * l2: org.bitbucket.franck44.automat.lang.Lang[(org.bitbucket.franck44.automat.DState[Set[Int]],
 *   org.bitbucket.franck44.automat.DState[Set[Int]]),String] =
 *   Lang(Union(NFA(Set(0),Set(),Set(0)),NFA(Set(0),Set(),Set())))
 *
 * scala> l2.isEmpty
 * res4: Boolean = false
 *
 * scala> l2.getAcceptedTrace
 * res5: Option[Seq[String]] = Some(List())
 * }}}
 *
 * Other non trivial examples include:
 * {{{
 * scala> import org.bitbucket.franck44.automat.edge.Edge
 * import org.bitbucket.franck44.automat.edge.Edge
 *
 * scala> val aut3 = NFA(Set(0),Set(Edge(0,"a",1), Edge(1,"b",0), Edge(1,"a",1)),Set(1))
 * aut3: org.bitbucket.franck44.automat.auto.NFA[Int,String] = NFA(Set(0),Set(Edge(0,a,1), Edge(1,b,0), Edge(1,a,1)),Set(1))
 *
 * scala> val aut4 = NFA( Set( 0 ), Set( Edge( 1, "a", 2 ), Edge( 0, "b", 1 ) ), Set( 2 ) )
 * aut4: org.bitbucket.franck44.automat.auto.NFA[Int,String] = NFA(Set(0),Set(Edge(1,a,2), Edge(0,b,1)),Set(2))
 *
 * scala> (Lang(aut3) \/ Lang(aut4) ).getAcceptedTrace
 * res6: Option[Seq[String]] = Some(List(a))
 *
 * scala> (Lang(aut3) \/ Lang(aut4) ) accepts List("b","a")
 * res7: Boolean = true
 *
 * scala> (Lang(aut3) \ Lang(aut4)).isEmpty
 * res8: Boolean = false
 *
 * scala> (Lang(aut3) \ Lang(aut4)).getAcceptedTrace
 * res9: Option[Seq[String]] = Some(List(a))
 * }}}
 *
 */
package object lang
