/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat
package auto

/**
 * An automaton that accepts the concatenation of the languages
 * accepted by two deterministic and complete automata.
 *
 * @param   a The first automaton
 * @param   b The second
 * @tparam  S1 Type of the states of first automaton
 * @tparam  S2 Type of the states of the second automaton
 * @tparam  L  Type of the labels
 */
final case class Concatenation[ S1, S2, L ]( private val a : DetAuto[ S1, L ], private val b : DetAuto[ S2, L ] ) extends DetAuto[ ( S1, Set[ S2 ] ), L ] {
    private type S = ( S1, Set[ S2 ] )

    override val name : String = s"Concatenation of ${a.name} and ${b.name}"

    override val getInit : S = {
        if ( a.isFinal( a.getInit ) )
            ( a.getInit, Set( b.getInit ) )
        else
            ( a.getInit, Set.empty )
    }

    override def isFinal( s : S ) : Boolean = s._2.exists( b.isFinal )

    override def acceptsAll( s : S ) : Boolean = s._2.exists( b.acceptsAll )

    override def acceptsNone( s : S ) : Boolean = a.acceptsNone( s._1 ) && s._2.forall( b.acceptsNone )

    import util.Cache

    //  the cache for successors
    private val cachedSucc = Cache[ ( S, L ), S ]()

    override def succ( s : S, l : L ) : S = {
        cachedSucc.putIfAbsent(
            ( s, l ),
            {
                val ( q1, p1 ) = s
                val q2 = a.succ( q1, l )
                val p2 = p1.map( b.succ( _, l ) )
                if ( a.isFinal( q2 ) )
                    ( q2, p2 + b.getInit )
                else
                    ( q2, p2 )
            } )
        cachedSucc.get( ( s, l ) ).get
    }

    override def enabledIn( s : S ) : Set[ L ] = a.enabledIn( s._1 ) ++ s._2.flatMap( b.enabledIn )
}
