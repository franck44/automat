/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat
package auto

import edge.{ LabDiEdge, DiEdge }
import graph.DiGraph

/**
 * A Non-Deterministic Finite Automaton (NFA),
 *
 * @param   init        The set of initial states
 * @param   transitions The set of edges as instances of [[automat.edge.DiEdge]]
 * @param   accepting   The set of accepting states
 * @param   sinkAccept  A set of states from which every finite word is
 *                      accepted (must be included in `accepting`)
 * @param   sinkReject  A set of states from which no finite word is
 *                      accepted (must be disjoint from `accepting`)
 * @tparam  S           Type of the states
 * @tparam  L           Type of the labels
 */
case class NFA[ S, L ](
    private[ automat ] val init : Set[ S ],
    val transitions : Set[ LabDiEdge[ S, L ] ],
    val accepting : Set[ S ],
    val sinkAccept : Set[ S ] = Set[ S ](),
    val sinkReject : Set[ S ] = Set[ S ](),
    val name : String = "NoName" ) extends DiGraph[ S ]( transitions.map( _.unLabelled ) ) with DetAuto[ Set[ S ], L ] {

    require(
        ( sinkAccept &~ accepting ).isEmpty,
        "Set of sink accept states should be a subset of accepting states" )
    require(
        ( sinkReject & accepting ).isEmpty,
        "Set of sink reject states should not intersect accepting states" )
    require(
        ( transitions.map( _.src ) & sinkAccept ).isEmpty,
        "Outgoing edge from an AcceptAll state" )
    require(
        ( transitions.map( _.src ) & sinkReject ).isEmpty,
        "Outgoing edge from an RejectAll state" )

    import util.Cache
    //  cache for isFinal
    private val cachedIsFinal = Cache[ Set[ S ], Boolean ]()

    /**
     * Redefine == to ignore the name.
     */
    override def equals( that : Any ) = that match {
        case that : NFA[ S, L ] ⇒ init == that.init &&
            transitions.toSet == that.transitions.toSet &&
            accepting == that.accepting &&
            sinkAccept == that.sinkAccept &&
            sinkReject == that.sinkReject

        case _ ⇒ false
    }

    /**
     * The predicate `isFinal` on states of the deterministic automaton
     */
    def isFinal( s : Set[ S ] ) : Boolean = {
        cachedIsFinal.putIfAbsent ( s, ( s & accepting ).nonEmpty )
        cachedIsFinal.get( s ).get
    }

    //  cache for acceptsAll
    private val cachedAcceptsAll = Cache[ Set[ S ], Boolean ]()

    /**
     * The predicate acceptsAll on states of the deterministic automaton
     */
    def acceptsAll( s : Set[ S ] ) : Boolean = {
        cachedAcceptsAll.putIfAbsent ( s, ( s & sinkAccept ).nonEmpty )
        cachedAcceptsAll.get( s ).get
    }

    //  cache for acceptsNone
    private val cachedAcceptsNone = Cache[ Set[ S ], Boolean ]()

    /**
     * The predicate acceptsAll on states of the deterministic automaton
     */
    def acceptsNone( s : Set[ S ] ) : Boolean = {
        cachedAcceptsNone.putIfAbsent ( s, ( s &~ sinkReject ).isEmpty )
        cachedAcceptsNone.get( s ).get
    }

    //  initial set of states
    def getInit : Set[ S ] = init

    //  set of edges in the form s - l -> {s1,s2, ..., sn}
    private lazy val succMap : Map[ ( S, L ), Set[ S ] ] = {
        transitions.groupBy( e ⇒ ( e.src, e.lab ) ) map {
            case ( k, xv ) ⇒ ( k, xv.map( _.tgt ).toSet )
        }
    }

    //  Outgoing transitions from a state.
    private lazy val outMap : Map[ S, Set[ ( L, S ) ] ] =
        transitions.groupBy( _.src )
            .mapValues(
                _.map { e ⇒ ( e.lab, e.tgt ) } )

    //  the cache for successors
    private val cachedSucc = Cache[ ( Set[ S ], L ), Set[ S ] ]()

    /**
     * Total successor function for the deterministic automaton.
     *
     * @param   s   A state of the deterministic and complete automaton
     * @param   l   A label of type `L`
     * @return      The unique successor state of `s` via `l`
     */
    def succ( s : Set[ S ], l : L ) : Set[ S ] = {
        cachedSucc.putIfAbsent(
            ( s, l ),
            {
                if ( acceptsAll( s ) || acceptsNone( s ) )
                    s
                else
                    s.flatMap( q ⇒ succMap.get( ( q, l ) ) ).flatten
            } )
        cachedSucc.get( ( s, l ) ).get
    }

    //  set of edges in the form s -> {l1,l2,...,ln}
    private val enabledInMap : Map[ S, Set[ L ] ] = {
        transitions.groupBy( e ⇒ e.src ) map {
            case ( k, xv ) ⇒ ( k, xv.map( _.lab ).toSet )
        }
    }

    //  cache for EnabledIn
    private val cachedEnabledIn = Cache[ Set[ S ], Set[ L ] ]()

    /**
     * Labels enabled in a state of the deterministic automaton
     */
    def enabledIn( xs : Set[ S ] ) : Set[ L ] = {
        cachedEnabledIn.putIfAbsent(
            xs,
            //  collect the labels enabled in each state in xs
            xs.foldLeft( Set[ L ]() ) {
                ( y, x ) ⇒ y ++ enabledInMap.getOrElse( x, Set() )
            } )
        cachedEnabledIn.get( xs ).get
    }

    /**
     * The set of states of this NFA.
     */
    lazy val states : Set[ S ] = init ++ accepting ++
        sinkAccept ++ sinkReject ++
        transitions.flatMap( e ⇒ Set( e.src, e.tgt ) )

    /**
     * A fast (cached) outgoing edges function.
     */
    override def outGoingEdges( s : Set[ S ] ) : List[ ( L, Set[ S ] ) ] = {
        if ( acceptsAll( s ) || acceptsNone( s ) )
            //  sinkAccept and sinkReject states have by convention empty set of outgoing edges.
            List()
        else
            s.flatMap( outMap.get ).flatten
                .groupBy( _._1 )
                .mapValues( _.map( _._2 ) )
                .toList
    }
}
