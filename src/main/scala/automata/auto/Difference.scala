/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat
package auto

/**
 * An automaton that accepts the <strong>difference</strong> of
 * the languages accepted by two automata `a` and `b`.
 * `a - b` accepts L(a) \setminus L(b).
 *
 * As `a` and `b` come with a deterministic interface of type [[DetAuto]]
 * the difference isimplemented as a product with a definition of `isFinal`
 * that returns `true` iff `a` is in a final state and `b` is not.
 *
 * @param   a   The first automaton to subtract from.
 * @param   b   The second automaton to be subtracted.
 * @tparam  S1  Type of the states of first automaton
 * @tparam  S2  Type of the states of the second automaton
 * @tparam  L   Type of the labels
 */
case class Diff[ S1, S2, L ](
    private val a : DetAuto[ S1, L ],
    private val b : DetAuto[ S2, L ] )
    extends ProdAuto( a, b ) {

    val name = s"Difference ${a.name} minus ${b.name}"

    /**
     * `true` for  `(s1,s2)` iff  `s1` is final and `s2` is not final.
     */
    def isFinal( s : ( S1, S2 ) ) : Boolean = {
        a.isFinal( s._1 ) & !b.isFinal( s._2 )
    }

    /**
     * Accepts all if a accepts all and b accepts none
     */
    def acceptsAll( s : ( S1, S2 ) ) : Boolean = {
        a.acceptsAll( s._1 ) && b.acceptsNone( s._2 )
    }

    /**
     *  Accepts none if a accepts none or b accepts all
     */
    def acceptsNone( s : ( S1, S2 ) ) : Boolean = {
        a.acceptsNone( s._1 ) || b.acceptsAll( s._2 )
    }

    /**
     *  Enabled labels for `a` and `b` (both are deterministic).
     */
    def enabledIn( s : ( S1, S2 ) ) : Set[ L ] =
        a.enabledIn( s._1 )
}
