/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat
package auto

/**
 *  Free Product of deterministic automata.
 *
 * @param   autos       The automata in the product.
 * @tparam  S           Type of the states.
 * @tparam  L           Type of the labels.
 */
case class FreeProduct[ S, L ](
    autos : Seq[ DetAuto[ S, L ] ] ) extends DetAuto[ Seq[ S ], ( Int, L ) ] {

    /**
     * Name of the automaton
     */
    override val name : String = "prod " + autos.map( _.name ).mkString( " x " )

    /**
     * Unique initial state of the automaton.
     */
    def getInit : Seq[ S ] = autos.map( _.getInit )

    /**
     * `true` iff `s` is an accepting state of the DCA.
     */
    def isFinal( s : Seq[ S ] ) : Boolean =
        Range( 0, s.size ).forall( a ⇒ autos( a ).isFinal( s( a ) ) )

    /**
     * `true` iff every finite word is accepted from `s`.
     *
     * This can be used to defined suffix-closed languages of the
     * form L.A*. IF acceptsAll(`s`) then `isFinal(s)` must also hold.
     */
    def acceptsAll( s : Seq[ S ] ) : Boolean = false

    /**
     * `true` only if no finite word is accepted from `s`.
     *
     *  This is mainly used in building `Difference`: when  the `product`
     *  `A` - `B` is a  state (s1,s2) with acceptsAll(s2) true,
     *  no works can be accepted from (s1,s2). This enables to prune the
     *  search tree when looking for an accepted word in `A`-`B`.
     *
     */
    def acceptsNone( s : Seq[ S ] ) : Boolean = false

    /**
     * Unique successor state `s'` of `s` after `l` in the deterministic view
     */
    def succ( s : Seq[ S ], l : ( Int, L ) ) : Seq[ S ] = {
        val i = l._1
        s.updated( i, autos( i ).succ( s( i ), l._2 ) )
    }

    /**
     * Set of labels in `L` enabled in `s`
     */
    def enabledIn( s : Seq[ S ] ) : Set[ ( Int, L ) ] = {
        Range( 0, s.size )
            .flatMap(
                a ⇒ autos( a ).enabledIn( s( a ) ).map( ( a, _ ) ) )
            .toSet
    }
}
