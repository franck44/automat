/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat
package auto

/**
 *  Product of two deterministic automata. It does not define
 *  any final/acceptsAll/accepstNone states but only the type
 *  of states in the product and the successor relation
 *
 * @param   init        The set of initial state
 * @param   edges       The set of edges
 * @param   accepting   The set of accepting states
 * @tparam  S           Type of the states
 * @tparam  L           Type of the labels
 */

abstract class ProdAuto[ S1, S2, L ](
    a : DetAuto[ S1, L ],
    b : DetAuto[ S2, L ] )
    extends DetAuto[ ( S1, S2 ), L ] {

    // initial state in the product
    def getInit : ( S1, S2 ) = ( a.getInit, b.getInit )

    import util.Cache
    //  the cache for successors
    private val cachedSucc = Cache[ ( ( S1, S2 ), L ), ( S1, S2 ) ] ()

    def succ( s : ( S1, S2 ), l : L ) : ( S1, S2 ) =
        {
            cachedSucc.putIfAbsent(
                ( s, l ),
                {
                    if ( acceptsAll( s ) || acceptsNone( s ) ) s
                    else ( a.succ( s._1, l ), b.succ( s._2, l ) )
                } )
            cachedSucc.get( ( s, l ) ).get
        }
}
