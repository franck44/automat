/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat

/**
 * This is the documentation for package [[auto]]
 *
 * ==Overview==
 *
 * This package provides support for finite automata operations. The simplest type
 * is a non-deterministic finite automaton `NFA`.
 * Compositions of automata can be built using the operators `+` (union),
 * `*` (intersection) and `-` (difference).
 *
 * ==Usage==
 *
 * ====Creating NFA====
 * The `NFA` class is provided by the subpackage `auto` import:
 *
 * {{{
 * scala> import org.bitbucket.franck44.automat.auto._
 * import org.bitbucket.franck44.automat.auto._
 *
 * scala> val aut1 = NFA[Int, String](Set(), Set(), Set())
 * aut1: org.bitbucket.franck44.automat.auto.NFA[Int,String] = NFA(Set(),Set(),Set(),Set(),Set(),NoName)
 * }}}
 * creates a simple emtpy `NFA`. The states or nodes are of type `Int` and the labels of type `String`. By default the NFA has the name `NoName`, and the
 * set of states `sinkAccept` and `sinkReject` are empty (see below for the usage of
 * these sets).
 *
 * NFAs can be defined using the implicit Edge constructors. The edge constructor si `~>` and can
 * be used to define labeled edges (the first one is an edge from state 0 to state 1, labeled `a`):
 * {{{
 * scala> import org.bitbucket.franck44.automat.edge.Implicits._
 * import org.bitbucket.franck44.automat.edge.Implicits._
 *
 * scala> val edges1 = Set( (0 ~> 1)("a"), (0 ~> 2)("a"), (1 ~> 0)("b"), (1 ~> 2)("a") )
 * edges1: scala.collection.immutable.Set[org.bitbucket.franck44.automat.edge.Edge[Int,String]] =
 *         Set(Edge(0,a,1), Edge(0,a,2), Edge(1,b,0), Edge(1,a,2))
 * }}}
 * By creating a set of intial, acepting states we can define an automaton:
 * {{{
 * scala> val init = Set(0)           // initial states
 * init: scala.collection.immutable.Set[Int] = Set(0)
 *
 * scala> val acc = Set(2)            // accepting/final states
 * acc: scala.collection.immutable.Set[Int] = Set(2)
 *
 * scala> val aut2 = NFA( init, edges1, acc)
 * aut2: org.bitbucket.franck44.automat.auto.NFA[Int,String] =
 *       NFA(Set(0),Set(Edge(0,a,1), Edge(0,a,2), Edge(1,b,0), Edge(1,a,2)),Set(2),Set(),Set(),NoName)
 * }}}
 *
 * The two empty sets are respectively the set of `sinkAccept` and `sinkReject`.
 * From a `sinkAccept` (resp. `sinkReject`) states, every word is accepted (resp. rejected).
 * This allows for compact representations for automata accepting suffix closed languages
 * of the form
 * L.\Sigma^*
 *
 * ====Deterministic view====
 *
 * All the automata inherit the [[DetAuto]] trait. The [[DetAuto]] trait
 * provides and interface to view an automaton `Aut` as
 * a complete and deterministic automaton that accepts the same
 * language as `Aut`.
 *
 * The deterministic view interface consists of: `getInit` to retrieve the initial
 * state of the deterministic view, `succ(s,l)` to compute the (unique)
 * successor of a state `s` of the deterministic view, the  predicates `isFinal`,
 * `acceptsAll` and `acceptsNone`.
 *
 * {{{
 * scala> val aut4 = NFA (Set(0), Set ( (0 ~> 1)("a"), (1 ~> 2)("b")), Set(2))
 * aut4: org.bitbucket.franck44.automat.auto.NFA[Int,String] =
 * NFA(Set(0),Set(Edge(0,a,1), Edge(1,b,2)),Set(2),Set(),Set(),NoName)
 * scala> import aut4._
 * import aut4._
 *
 * scala> val s0 = getInit
 * s0: Set[Int] = Set(0)
 *
 * scala> val s1 = succ(s0, "a")
 * s1: Set[Int] = Set(1)
 *
 * scala> isFinal(s1)
 * isFinal(s1)
 * res10: Boolean = false
 *
 * scala> isFinal ( succ(s1, "b"))
 * res11: Boolean = true
 *
 * scala> isFinal (succ(s1, Seq("b", "a")))
 * res12: Boolean = false
 *
 * }}}
 *
 * If a state `s` of the deterministic view does not have a successor for
 * a given label, the deterministic view enters the empty set  `Set()`.
 * {{{
 *
 * scala> succ(s0, "b")
 * res13: Set[Int] = Set()
 *
 * }}}
 *
 *
 * ====Using sinkAccept====
 * To define an automaton that accepts all the words with a prefix in a regular language,
 * we can use the `sinkAccept` set:
 * {{{
 * scala> val aAndMore = NFA(Set(0), edges = Set((0 ~> 1)("a")), Set(1), sinkAccept = Set(1), Set())
 * aAndMore: org.bitbucket.franck44.automat.auto.NFA[Int,String] = NFA(Set(0),Set(Edge(0,a,1)),Set(1),Set(1),Set(),NoName)
 *
 * }}}
 * `aAndMore` accepts all the words that start with letter `a`.
 * A `sinkAccept` state `s` is equivalent to a state with a loop `s - l -> s` for any
 * letter in alphabet of the automaton. The `sinkAccept` set provides a compact way to
 * represent a potentially infinite
 * number of loop transitions. It can be used to define suffix-closed languages. A
 * suffix-closed language `K` has the following property: if `w` is in `K` the any
 * extension `w.x` with `x` in `L*` is in `K`.
 *
 * Computing the successor state of the initial state after
 * a sequence that starts with an `a` yields:
 *
 * {{{
 * scala> import aAndMore._         // import methods of aAndMore
 * import aAndMore._
 *
 * scala> val s0 = getinit          // get the initial state of aAndMore
 * s0: Set[Int] = Set(0)
 *
 * scala> val s1 = succ(s0,"a")    //  successor state of initial state
 * s1: Set[Int] = Set(1)
 *
 * scala> val s2 = succ(s0, Seq("a", "b", "c", "d", "a"))    // successor after sequence of letters
 * s2: Set[Int] = Set(1)
 *
 * scala> s1 == s2
 * s1 == s2
 * res8: Boolean = true
 * }}}
 *
 * As can be seen in the previous example, a `sinkAccept` acts as a sink: every letter
 * is accepted and the successor state is the same as the previous (sink) state.
 *
 * `sinkReject` can be used in a similar way. Theye are used internally to define the complement
 * of a automaton: if `s` is a `sinkAccept` state in `A`, `B` is another automaton,  then `B - A`
 * can be defined as a product of `B` and `A` where the `sinkAccept` are now `sinkReject` states.
 *
 * ===Product automata===
 *
 * Product (composition of) automata  i.e. intersection, union, complement, can be defined using
 * the operators `+` (union), `*` (intersection) and `-` (difference).
 *
 * {{{
 * scala> val aut3 = NFA (Set(0), Set ( (0 ~> 1)("a"), (1 ~> 2)("b")), Set(2))
 * aut3: org.bitbucket.franck44.automat.auto.NFA[Int,String] =
 *     NFA(Set(0),Set(Edge(0,a,1), Edge(1,b,2)),Set(2))
 *
 * scala> val p1 = aut2 + aut1
 * p1: org.bitbucket.franck44.automat.auto.Union[Set[Int],Set[Int],String] =
 *     Union(
 *       NFA(Set(0),Set(Edge(0,a,1), Edge(0,a,2), Edge(1,b,0), Edge(1,a,2)),Set(2)),
 *       NFA(Set(),Set(),Set())
 *     )
 *
 *
 * scala> val p2 = aut2 * aut3
 * p2: org.bitbucket.franck44.automat.auto.Inter[Set[Int],Set[Int],String] =
 *     Inter(
 *       NFA(Set(0), Set(Edge(0,a,1), Edge(0,a,2), Edge(1,b,0), Edge(1,a,2)),Set(2)),
 *       NFA(Set(0),Set(Edge(0,a,1), Edge(1,b,2)),Set(2))
 *     )
 *
 * scala> val p3 = p1 - aut1
 * p3: org.bitbucket.franck44.automat.auto.Diff[(org.bitbucket.franck44.automat.DState[Set[Int]],
 *    org.bitbucket.franck44.automat.DState[Set[Int]]),Set[Int],String] =
 *    Diff(
 *      Union(
 *        NFA(Set(0),Set(Edge(0,a,1), Edge(0,a,2), Edge(1,b,0), Edge(1,a,2)),Set(2)),
 *        NFA(Set(),Set(),Set())
 *      ),
 *      NFA(Set(),Set(),Set())
 *    )
 * }}}
 *
 *
 *
 */

package object auto
