/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat
package auto

/**
 *  An automaton that accepts the <strong>intersection</strong>
 *  of the languages of two automata.
 *
 * A word is accepted by `Inter(a,b)` iff it is accepted
 * by `a` and by `b`.
 *
 * @param   a   The first automaton
 * @param   b   The second
 * @tparam  S1  Type of the states of first automaton
 * @tparam  S2  Type of the states of the second automaton
 * @tparam  L   Type of the labels
 */
case class Inter[ S1, S2, L ](
    private val a : DetAuto[ S1, L ],
    private val b : DetAuto[ S2, L ] ) extends ProdAuto( a, b ) {

    val name = s"Intersection of ${a.name} and ${b.name}"

    /**
     * `(s1,s2)` is final in the intersection iff both `s1` and `s2` are final.
     */
    def isFinal( s : ( S1, S2 ) ) : Boolean =
        a.isFinal( s._1 ) && b.isFinal( s._2 )

    /**
     * Accepts all if both automata are in a state that accepts all.
     */
    def acceptsAll( s : ( S1, S2 ) ) : Boolean =
        a.acceptsAll( s._1 ) && b.acceptsAll( s._2 )

    /**
     * Accepts none iff one automaton is in a state that accepts none.
     */
    def acceptsNone( s : ( S1, S2 ) ) : Boolean =
        a.acceptsNone( s._1 ) || b.acceptsNone( s._2 )

    /**
     * In the intersection, the set of enabled labels is the
     * intersection of the set of the enabled labels in each automaton.
     */
    def enabledIn( s : ( S1, S2 ) ) : Set[ L ] =
        a.enabledIn( s._1 ) & b.enabledIn( s._2 )
}
