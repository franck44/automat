/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat
package util

/**
 * Provide a function to compute an NFA[Int,L]
 * representation of an DetAuto[S,L]
 */
object Determiniser {

    import Traversal._
    import edge.{ LabDiEdge }
    import edge.Implicits._
    import auto.{ NFA, DetAuto }
    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    //  logger
    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    def toDetNFA[ S, L ]( a : DetAuto[ S, L ] ) : ( NFA[ Int, L ], Map[ Int, S ] ) = toDetNFA( a, identity[ S ] )

    /**
     * Determinise an automaton.
     *
     * @param a The automaton DetAuto[S,L] to be determinised
     * @return Return an automaton of type NFA[Int,L] which is deterministic
     */
    def toDetNFA[ S, L, R ](
        a : DetAuto[ S, L ],
        recorder : S ⇒ R ) : ( NFA[ Int, L ], Map[ Int, R ] ) = {

        logger.info( "Starting Determinisation of automaton {}", a.name )

        //  compute the determinised version with a visitor and collect states and edges
        val DFSdeterminiser = DFS( DeterminiserVisitor[ S, L, R ]( Map(), List(), recorder, Map() ), a.outGoingEdges )
        val v = DFSdeterminiser( a.getInit )

        logger.info( "Determinisation completed for automaton {}", a.name )

        //  compute accepting, sinkAccept and sinkReject
        val accept = ( for ( ( s, i ) ← v.index if a.isFinal( s ) ) yield i ).toSet
        val acceptsAll = ( for ( ( s, i ) ← v.index if a.acceptsAll( s ) ) yield i ).toSet
        val rejectsAll = ( for ( ( s, i ) ← v.index if a.acceptsNone( s ) ) yield i ).toSet

        //  filter transitions to remove outgoing edges from sinkAccept/sinkReject
        val edges = v.edges.toSet.filterNot( x ⇒ ( acceptsAll ++ rejectsAll ).contains( x.src ) )

        //  create an NFA
        (
            new NFA(
                init = Set(
                    v.index( a.getInit ) ),
                transitions = edges,
                accepting = accept,
                acceptsAll,
                rejectsAll,
                name = s"Determinised(${a.name})" ),
            v.nodeInfo )
    }

    /**
     * Visitor to collect the reachable states and edges of a [[DetAuto[S,L]]] and build a [[NFA[Int,L]]]
     *
     * @param   index       Each new state discovered during the DFS is assigned
     *                      a fresh integer index
     * @param   edges       Current list of edges
     * @param   recorder    A function that is applied to each newly discovered state
     * @param   nodeInfo    The result of recorder on the currenlty discovered states
     *
     * @return edges The edges of the dterminisation of the automaton
     */
    private[ automat ] case class DeterminiserVisitor[ S, L, R ](
        val index : Map[ S, Int ] = Map[ S, Int ](),
        val edges : List[ LabDiEdge[ Int, L ] ] = List[ LabDiEdge[ Int, L ] ](),
        val recorder : S ⇒ R,
        val nodeInfo : Map[ Int, R ] = Map.empty ) extends DFSVisitor[ S, L, DeterminiserVisitor[ S, L, R ] ] {

        //  When discovering a new state s, if it does not have a parent
        //  we do not create a new edge as it should be the initial state.
        //  otherwise, if p(s) = (t, l) add edge (s, t, l) but mapped to indices.
        override def discoverState(
            s : S,
            discoveryTime : Map[ S, Int ],
            parent : Map[ S, ( S, L ) ] ) = {
            val newIndex = index + ( s → discoveryTime( s ) )
            val newEdges =
                if ( parent.isDefinedAt( s ) )
                    (
                        ( newIndex( parent( s )._1 ) ~> newIndex( s ) )( parent( s )._2 ) ) :: edges
                else edges
            DeterminiserVisitor( newIndex, newEdges, recorder, nodeInfo + ( discoveryTime( s ) → recorder( s ) ) )
        }

        //  Nothing to be done on treeEdges as the new edge will be created only when we visit
        //  the new state
        override def treeEdge(
            s : S, l : L, t : S,
            discoveryTime : Map[ S, Int ],
            parent : Map[ S, ( S, L ) ] ) = this

        //  For backEdges and ForwardEdges, no new node is created, only  add an edge.
        override def backEdge(
            s : S, l : L, t : S,
            discoveryTime : Map[ S, Int ],
            parent : Map[ S, ( S, L ) ] ) = DeterminiserVisitor( index, ( index( s ) ~> index( t ) )( l ) :: edges, recorder, nodeInfo )

        //  See comment for `backEdge`
        override def forwardEdge(
            s : S, l : L, t : S,
            discoveryTime : Map[ S, Int ],
            parent : Map[ S, ( S, L ) ] ) = DeterminiserVisitor( index, ( index( s ) ~> index( t ) )( l ) :: edges, recorder, nodeInfo )

    }
}
