/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat
package util

/**
 * Partitions of a (finite) set of elements.
 *
 * @param   p   A map that defines the class of each element.
 * @tparam  S   The type of the elements.
 * @tparam  I   The type of the class index.
 */
case class Partition[ S, I ]( p : Map[ S, I ] ) {

    /**
     * The equivalece class of a state.
     *
     * @param   s   A state.
     * @return      The Id of the equivalence class of a state.
     */
    def equivClassOf( s : S ) : Option[ I ] = p.get( s )

    /**
     * Build the list of classes of the partition.
     */
    def allClasses : Set[ Set[ S ] ] =
        /* example: p = Map("a" -> 0, "b" -> 1, "c" -> 0) */
        p
            /* Group states by values i.e. plit the map p according to values. */
            .groupBy { case ( _, v ) ⇒ v }
            /*
             * Map(0 -> Map("a" -> 0, "c" -> 0), 1 -> Map("b" -> 1)).
             * For each value of groupBy collect the set of keys
             */
            .mapValues( _.keys.toSet )
            /*
             * Map(1 -> Set(b), 0 -> Set(a, c)).
             * collect the values
             */
            .values
            /* MapLike.DefaultValuesIterable(Set(b), Set(a, c)) */
            .toSet

    /**
     * Partition Equalilty.
     *
     * @param  that    The partition to compare with.
     * @return         Whether `this` and `that` define the same partition i.e. same
     *                  equivalence classes.
     */
    def equiv[ J ]( that : Partition[ S, J ] ) = allClasses == that.allClasses

    /**
     * Number of equivalence classes in the partition.
     */
    lazy val size = p.values.toSet.size

}