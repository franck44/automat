/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat
package util

/**
 * Provide a function to compute a minimal and deterministic NFA[Int,L].
 */
object Minimiser {

    import edge.{ LabDiEdge }
    import edge.Implicits._
    import auto.{ NFA, DetAuto }
    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    //  logger
    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    /**
     * Split an equivalence class of states according to target states'
     * class for each outgoing label.
     *
     * @param clazz     The class (set of states) to be split.
     * @param p         The equivalence classes defined by `p`, `s equiv s' iff p(s) == p(s')`.
     * @return          The class `clazz` split into new equivalence (sub-)classes.
     *                  The elements E of the result satisfy:
     *                      s and s' are in E iff: for each label `a`
     *                          1. s - a -> s1 iff s' - a -> s2 and
     *                          2. p(s1) = p(s2).
     *                  Result is the coarset parition that satifies 1 and 2.
     */
    private def splitClass[ S, L, I ](
        clazz : Set[ S ],
        transitions : Map[ S, Set[ ( L, S ) ] ],
        p : Partition[ S, I ] ) : Set[ Set[ S ] ] =
        clazz
            /* for each state s in class c, collect outgoing transitions  */
            .map {
                s ⇒
                    ( s, transitions.getOrElse( s, Set.empty )
                        /* and map them to pairs (label, class of tgt) */
                        .map{ case ( l, tgt ) ⇒ ( l, p.equivClassOf( tgt ) ) } )
            }
            /* Group according to same set of target pairs (label, class of tgt) */
            .groupBy { case ( _, k ) ⇒ k }
            /* Map the values (List of pairs (S, Set[L,Int]) of the groupBy map to the list of S */
            .mapValues { l ⇒ l.map( { case ( x, _ ) ⇒ x } ).toSet }
            /* Collect the values of the map which are the new equivalence classes */
            .values
            .toSet

    /**
     * Refine a list of classes by splitting each class in the list.
     *
     * @param part    The partition to refine (implicitly defines the equivalence classes).
     * @return        A refinement of each class of the partition according to [[splitClass]].
     */
    private def splitPartition[ S, L, I ](
        transitions : Map[ S, Set[ ( L, S ) ] ],
        part : Partition[ S, I ] ) : Partition[ S, Int ] = {

        /* Split each class and put together the concatenated list of new classes */
        Partition(
            part.allClasses
                /* Split each class */
                .flatMap { c ⇒ splitClass( c, transitions, part ) }
                /* Build the new partition */
                .zipWithIndex
                .flatMap( { case ( xl, k ) ⇒ xl.toList map { case a ⇒ ( a, k ) } } )
                .toMap )
    }

    /**
     * Compute greatest Fix point of splitPartition.
     *
     * @param   transitions The transition relation.
     * @param   part        A partition of the states of `transitions` into equivalence classes.
     * @return              The greatest fix point splitPartition.
     */
    private def gfpSplitPartition[ S, L, I ](
        transitions : Map[ S, Set[ ( L, S ) ] ],
        part : Partition[ S, I ] ) : Partition[ S, Int ] = {
        splitPartition( transitions, part ) match {
            case s if s.size == part.size ⇒ s
            case s                        ⇒ gfpSplitPartition( transitions, s )
        }
    }

    /**
     * The default initial partition, which groups states by their acceptance / rejection properties.
     *
     * @param det The automaton.
     */
    private def defaultPartition[ S, L ]( det : NFA[ S, L ] ) : Partition[ S, Int ] = {
        Partition(
            det.states
                .map( {
                    case s if det.sinkAccept.contains( s ) ⇒ ( s, 0 )
                    case s if det.accepting.contains( s )  ⇒ ( s, 1 )
                    case s if det.sinkReject.contains( s ) ⇒ ( s, 2 )
                    case s                                 ⇒ ( s, 3 )
                } )
                .toMap )
    }

    /**
     * Computes the state equivalence classes for a given deterministic automaton.
     * @param det The automaton.
     * @param initialPartition An optional initial partition. Defaults to [[defaultPartition()]].
     */
    def getLanguageEquivStates[ S, L, I ]( det : NFA[ S, L ] )( initialPartition : Partition[ S, I ] = defaultPartition( det ) ) : Partition[ S, Int ] = {
        /* Check that each state of det has a class defined in initialPartition */
        require( ( det.states &~ initialPartition.p.keys.toSet ).isEmpty, "Initial Partition does not map all states" )

        /* Group transitions by source state once, for faster lookup later */
        val groupedTransitions = det.transitions.groupBy( _.src ).mapValues( _.map { e ⇒ ( e.lab, e.tgt ) } )

        /* minimise using partition refinement */
        gfpSplitPartition( groupedTransitions, initialPartition )
    }

    /**
     *
     * @param det               The automaton to quotient.
     * @param initialPartition  A state partition.
     *
     * @return                  The automaton det' such that (part(s), l, part(s')) iff (s, l, s')  and
     *                          init(det') = part(init(det)) and
     *                          accepting(det') = part(accepting(det)) (same for sinkReject and sinkAccept).
     */
    def quotient[ S, L, I ]( det : NFA[ S, L ], part : Partition[ S, I ] ) : NFA[ I, L ] = {
        NFA(
            det.init flatMap part.equivClassOf, //  the equivalence class of the initial state
            // As each state must be mapped in the initial partition, get cannot fail
            det.transitions.map( { case l ⇒ ( part.equivClassOf( l.src ).get ~> part.equivClassOf( l.tgt ).get )( l.lab ) } ),
            det.accepting flatMap part.equivClassOf,
            det.sinkAccept flatMap part.equivClassOf,
            det.sinkReject flatMap part.equivClassOf,
            name = s"Minimised ${det.name}" )
    }

    /**
     * Minimisation operator for DFA.
     * Can be applied only if the NFA is deterministic.
     *
     * @param det               The deterninistic automaton to minimize.
     * @param initialPartition  An optional initial state partition. Defaults to [[defaultPartition(det)]] if omitted.
     */
    def minimiseDet[ S, L, I ]( det : NFA[ S, L ] )(
        initialPartition : Partition[ S, I ] = defaultPartition ( det ) ) : NFA[ Int, L ] =
        quotient( det, getLanguageEquivStates( det )( initialPartition ) )

    /**
     * Minimisation operator for NFA.
     *
     * @param   nondet  The NFA to minimise.
     */
    def minimiseNonDet[ S, L ]( nondet : NFA[ S, L ] ) : NFA[ Int, L ] = {
        val ( det, _ ) = util.Determiniser.toDetNFA( nondet )
        minimiseDet( det )()
    }

}
