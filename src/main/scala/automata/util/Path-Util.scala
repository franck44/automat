/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.automat

package util

/**
 * Utilities for managing paths that are used to load and store dot files for testing.
 */
object PathUtil {
    import java.nio.file.{ Files, Paths }

    //  store current directory into root
    val root : String = new java.io.File( "." ).getCanonicalPath

    /** Path to dot tests files directory which is used by dot parser tests */
    val dotTestPath = Paths.get( root, "src", "test", "resources", "dot-test-files" ).toString()

    /** Join a filename to a path stored in a string */
    def joinPath( path : String, fileName : String ) = Paths.get( path, fileName ).toString()

}
