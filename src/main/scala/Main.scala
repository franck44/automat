/*
 *  This file is part of Automat.
 *
 *  Copyright (C) 2015-2018 Franck Cassez.
 *
 *  Automat is free software: you can redistribute it and/or modify it under
 *  the terms  of the  GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of  the License,  or (at your
 *  option) any later version.
 *
 *  Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
 *  WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU Lesser General Public License for  more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
 *  <http://www.gnu.org/licenses/>.
 */

/**
 * Main object that contains the function to run first
 */
object Main {

    import org.bitbucket.franck44.automat.auto.NFA
    import org.bitbucket.franck44.automat.util.DFSVisitor
    import org.bitbucket.franck44.automat.util.Determiniser.toDetNFA
    import org.bitbucket.franck44.automat.util.DotConverter._
    import org.bitbucket.franck44.automat.util.Traversal.DFS
    import org.bitbucket.franck44.dot.DOTPrettyPrinter.show
    import org.bitbucket.franck44.dot.DOTSyntax.DotSpec
    import org.bitbucket.franck44.dot.util.DotASTAttr._
    import org.bitbucket.franck44.dot.util.FileParser.parseFile
    import scala.reflect.io._
    import scala.util.Try

    /**
     * The main method.
     */
    def main( args : Array[ String ] ) : Unit = {
        println( s"Hello Franck - Parsing and pretty printing file ${args( 1 )}" )
        val path = "src/test/scala/au/edu/mq/comp/automata/tests/"
        //  read an NFA from a dot file

        //  build an NFA. As we read strings we have to convert
        val nfa1 = {
            val tryNfa : Try[ DotSpec ] = parseFile( path + "auto4.dot" )
            assert( tryNfa.isSuccess )
            new NFA(
                getInitStates( tryNfa.get ),
                getEdges( tryNfa.get ), getAcceptStates( tryNfa.get ) )
        }

        //  build an NFA. As we read strings we have to convert
        // val nfa2 = {
        //     val tryNfa : Try[ DotSpec ] = parseFile( path + "auto5.dot" )
        //     assert( tryNfa.isSuccess )
        //     val r = new NFA[ String, String ](
        //         getInitStates( tryNfa.get ),
        //         getEdges( tryNfa.get ), getAcceptStates( tryNfa.get )
        //     )
        //     // with Determinised[ String, String ]
        //     // DFA( r )
        //     r
        // }

        // val d = nfa1 + nfa2
        // File( "/tmp/latestRes.dot" ).writeAll( show( toDot( toDetNFA( d )._1 ) ) )
        // println( "Done - result in /tmp/latestRes.dot" )

    }
}
