[![License: LGPL v3](https://img.shields.io/badge/License-LGPL%20v3-blue.svg)](http://www.gnu.org/licenses/lgpl-3.0)

[![Sonatype Nexus (Releases)](https://img.shields.io/nexus/r/https/oss.sonatype.org/org.bitbucket.franck44.automat/automat_2.12.svg?label=Sonatype%20Nexus%20Latest)]()
[![Sonatype Nexus (Snapshots)](https://img.shields.io/nexus/s/https/oss.sonatype.org/org.bitbucket.franck44.automat/automat_2.12.svg?label=Sonatype%20Nexus%20Current)]()

[![Run Status](https://api.shippable.com/projects/57497cfa2a8192902e21b58a/badge?branch=master)](https://app.shippable.com/projects/57497cfa2a8192902e21b58a)
[![Coverage Badge](https://api.shippable.com/projects/57497cfa2a8192902e21b58a/coverageBadge?branch=master)](https://app.shippable.com/projects/57497cfa2a8192902e21b58a)

# Automata library #

This library provides classes and methods to create finite automata and compositions thereof.
To use it, you may add it to your dependencies (see installation section below) or clone the project, run sbt test and set publishLocal.

### Automat ###

* The main package is automat. It provides classes and methods to create/compose finite automata.
* Versions: see badges with latest stable and current snapshot (nexus)

### Requirements
To use Automat you need:

*  a recent version (&geq; 0.13) of [scala sbt](http://www.scala-sbt.org/).

### Installation

Add to your `build.sbt` file the following dependencies:

~~~
libraryDependencies ++=
    Seq (
        ...
        "org.bitbucket.franck44.automat" %% "automata" % "1.1.0-SNAPSHOT"
    )
~~~

The `1.1.0-SNAPSHOT` uses the SNAPSHOT version of the Sonatype Nexus repository.
Change this field to the version you want to use (The badges at the top of this page lists the latest stable and snapshot versions).

For more versions, you can browse the  arhives [snapshots versions](https://oss.sonatype.org/content/repositories/snapshots/org/bitbucket/franck44/automat/automat_2.12/) and the [released versions](https://repo.maven.apache.org/maven2/org/bitbucket/franck44/automat/).

### Change logs (recent)
* 1.2.0: Add support for minimisation for finite automata (NFA).

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Contact ###

* franck.cassez@mq.edu.au
* Macquarie University, Sydney, Australia
