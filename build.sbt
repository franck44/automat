
initialCommands in console := 
     """|
        | import ch.qos.logback.classic.Logger
        | import org.slf4j.LoggerFactory
        | val root = LoggerFactory.getLogger("root").asInstanceOf[Logger]
        | import ch.qos.logback.classic.Level
        | root.setLevel(Level.OFF)
        | import org.bitbucket.franck44.automat.auto.NFA
        | import org.bitbucket.franck44.automat.edge.LabDiEdge
        | import org.bitbucket.franck44.automat.edge.Implicits._
        | import org.bitbucket.franck44.automat.util.Determiniser.toDetNFA
        |
        | println("Init done")
        |
        | """.stripMargin


//  define project common settings for current directory
lazy val commonSettings = Seq(
    organization := "org.bitbucket.franck44.automat",
    scalaVersion := "2.12.8"  
)

ensimeScalaVersion in ThisBuild := "2.12.8"

// Dependencies

libraryDependencies ++=
    Seq (
        "org.bitbucket.inkytonik.kiama" %% "kiama" % "2.3.0-SNAPSHOT",
        "org.bitbucket.inkytonik.kiama" %% "kiama" % "2.3.0-SNAPSHOT" % "test" classifier ("tests"),
        "org.bitbucket.inkytonik.kiama" %% "kiama-extras" % "2.3.0-SNAPSHOT",
        "org.bitbucket.inkytonik.kiama" %% "kiama-extras" % "2.3.0-SNAPSHOT" % "test" classifier ("tests"),
        "org.scalatest" %% "scalatest" % "3.0.5" % "test",
        "org.scalacheck" %% "scalacheck" % "1.14.0" % "test",
        "com.typesafe.scala-logging" %% "scala-logging" % "3.7.2",
        "ch.qos.logback" % "logback-classic" % "1.2.3"
    )

//  create a configuration for coverage test
lazy val ShippableCITest = config("shippableci") extend(Test)

// test options for generating formatted test results in IntegrationTest config for shippable
ShippableCITest / testOptions += {
    (target in Test) map {
        t => Tests.Argument(TestFrameworks.ScalaTest, "-u", "%s" format (t / "../shippable/testresults"))
        }
    }.value

ShippableCITest / fork := true

// scoverage package
ShippableCITest / coverageEnabled := true
ShippableCITest / coverageMinimum := 80
ShippableCITest / coverageFailOnMinimum := false   //  false is safer as otherwise the build breaks
ShippableCITest / coverageHighlighting := true     //  enable highlighting of covered/non-covered
 // exclude some package from coverage
ShippableCITest / coverageExcludedPackages := ".*sbtrats.*;.*resources.*"
coverageExcludedPackages := ".*sbtrats.*;.*resources.*"
ShippableCITest / logBuffered := false
ShippableCITest / logLevel:= Level.Info

ShippableCITest / scalaSource:= baseDirectory.value / "src"
ShippableCITest / resourceDirectory := baseDirectory.value / "src/test/resources"

//  parallel execution
Test / parallelExecution := true
ShippableCITest / parallelExecution := true

ThisBuild / logLevel := Level.Info

//  sbt shell prompt
ThisBuild / shellPrompt := {
    state =>
        Project.extract(state).currentRef.project + " [" + name.value + "] " + version.value +
            " " + scalaVersion.value + "> "
}

//      project settings
lazy val root = (project in file(".")).
    configs(ShippableCITest).
    settings(commonSettings: _*).
    settings(inConfig(ShippableCITest)(Defaults.testTasks): _*).
    settings(
      name := "automat",
      scalacOptions :=
          Seq (
              "-deprecation",
              "-feature",
              "-sourcepath", baseDirectory.value.getAbsolutePath,
              "-unchecked",
              "-Xlint:-stars-align,-unused,_",
              "-Xcheckinit",
              "-Yrangepos"
          ),
          // Publishing
          publishTo := {
              // val nexus = "https://oss.sonatype.org/"
              Some(
                  if (version.value.trim.endsWith("SNAPSHOT"))
                      Opts.resolver.sonatypeSnapshots
                  else
                      Opts.resolver.sonatypeStaging
              )
          },
          publishConfiguration := publishConfiguration.value.withOverwrite(true),
          publishLocalConfiguration := publishLocalConfiguration.value.withOverwrite(true),
          publishMavenStyle := true,
          publishArtifact in Test := true,
          pomIncludeRepository := { x => false },
          pomExtra := (
              <url>https://bitbucket.org/franck44/automat/</url>
              <licenses>
                  <license>
                      <name>LGPL, v. 3.0</name>
                      <url>https://www.gnu.org/licenses/lgpl-3.0.en.html</url>
                      <distribution>repo</distribution>
                  </license>
              </licenses>
              <scm>
                  <url>https://bitbucket.org/franck44/automat</url>
                  <connection>scm:git:https://bitbucket.org/franck44/automat</connection>
              </scm>
              <developers>
                  <developer>
                     <id>franck44</id>
                     <name>Franck Cassez</name>
                     <url>https://bitbucket.org/franck44</url>
                  </developer>
              </developers>
          )
      )

//  set profile for publishing
sonatypeProfileName  := "org.bitbucket.franck44"
// Add the default sonatype repository setting
publishTo := sonatypePublishTo.value

Compile / doc / scalacOptions ++= Seq(
    "-groups",
    "-implicits",
    "-diagrams",
    "-diagrams-dot-restart",
    "50",
    "-no-link-warnings")

//  create a tmp directory before running the tests if it does not exist
lazy val makeTmpDirTask = taskKey[Unit]("Create a tmp directory before running tests.")

makeTmpDirTask := {
  import java.nio.file.{ Files, Paths }
  println("Checking whether tmp directory exists ...")
  if ( !Files.exists( Paths.get( "tmp" ) ) ) {
          println("... tmp does not exist. Creating tmp")
          Files.createDirectory( Paths.get( "tmp" ) )
      }
  else
    println("... tmp directory exists.")
}

test in Test := {
  makeTmpDirTask.value
  (test in Test).value
}

test in ShippableCITest := {
  makeTmpDirTask.value
  (test in ShippableCITest).value
}

//  scoverage
coverageMinimum := 80
coverageFailOnMinimum := false  //  false is safer as otherwise the build breaks
coverageHighlighting := true    //  enable highlighting of covered/non-covered
//  exclude some package from coverage
coverageExcludedPackages := ".*sbtrats.*"

// Interactive settings

parallelExecution in Test := true

resolvers ++= Seq (
    Resolver.sonatypeRepo ("releases"),
    Resolver.sonatypeRepo ("snapshots")
)

// Rats! setup

ratsScalaRepetitionType := Some (ListType)

ratsUseScalaOptions := true

ratsUseScalaPositions := true

ratsDefineASTClasses := true

ratsDefinePrettyPrinter := true

ratsUseKiama := 2

// ScalariForm

import scalariform.formatter.preferences._
import SbtScalariform.ScalariformKeys

ScalariformKeys.preferences := ScalariformKeys.preferences.value
    .setPreference (AlignSingleLineCaseStatements, true)
    .setPreference (IndentSpaces, 4)
    .setPreference (SpaceBeforeColon, true)
    .setPreference (SpaceInsideBrackets, true)
    .setPreference (SpacesAroundMultiImports, true)
    .setPreference (PreserveSpaceBeforeArguments, true)
    .setPreference (SpacesWithinPatternBinders, true)
    .setPreference (RewriteArrowSymbols, true)
    .setPreference (SpaceInsideParentheses, true)
//    .setPreference (PreserveSpaceAfterArguments, true)

//  use
headerLicense := Some(
    HeaderLicense.Custom(
    """| This file is part of Automat.
       |
       | Copyright (C) 2015-2018 Franck Cassez.
       |
       | Automat is free software: you can redistribute it and/or modify it under
       | the terms  of the  GNU Lesser General Public License as published by the
       | Free Software Foundation, either version 3 of  the License,  or (at your
       | option) any later version.
       |
       | Automat  is  distributed in  the  hope  that  it  will  be  useful,  but
       | WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
       | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
       |
       | See the GNU Lesser General Public License for  more details.
       |
       | You should have received a copy of the GNU Lesser General Public License
       | along with Automat. (See files COPYING and COPYING.LESSER.)  If not  see
       | <http://www.gnu.org/licenses/>.
       |""".stripMargin
        )
    )

//  exclude headers management in the generated sources
excludeFilter.in(headerSources) := HiddenFileFilter || "src/generated/**"

//  note: use test:headerCreate in sbt to generate the headers
//  checkHeaders to check which files need new headers
//  headers generations can also be automated at compile time
